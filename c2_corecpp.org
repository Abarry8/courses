# -*- mode: org -*-

#+TITLE: Core C++ (Spring 2024 version)
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="style/rworgmode.css" />

-------------------------------------------------------------
* About this book

- Contains all course content for the semester, including:
  - Reading material for topics
  - Review questions & answers
  - Programming assignment documentation
  - Reference material

-------------------------------------------------------------
* Course information

** Course information

** Policies

** Schedule
(Tentative)

| Week | Monday date  | Unit(s)                          |
|------+--------------+----------------------------------|
|    1 | Jan 15, 2024 | Welcome / Setup                  |
|    2 | Jan 22, 2024 | Exploring software; main()       |
|    3 | Jan 29, 2024 | Variables; input/output          |
|    4 | Feb 5, 2024  | If statements; switch statements |
|    5 | Feb 12, 2024 | While loops                      |
|    6 | Feb 19, 2024 | Pointers and memory              |
|    7 | Feb 26, 2024 | Functions                        |
|    8 | Mar 4, 2024  | Structs; classes                 |
|    9 | Mar 11, 2024 |                                  |
|   10 | Mar 18, 2024 | For loops; arrays and vectors    |
|   11 | Mar 25, 2024 | Strings; file input/output       |
|   12 | Apr 1, 2024  | Inheritance                      |
|   13 | Apr 8, 2024  | Searching and sorting; recursion |
|   14 | Apr 15, 2024 |                                  |
|   15 | Apr 22, 2024 |                                  |
|   16 | Apr 29, 2024 |                                  |
|   17 | May 6, 2024  |                                  |

Campus schedule notes:
- Jan 15: Campus closed
- Jan 16: first day of semester
- Mar 11 - 17: Spring break
- May 5: Last day of spring classes
- May 7 - 13: Final exams

-------------------------------------------------------------
* Units


(Each unit would be imported from another file)
# #+INCLUDE: "wip_notes/c2_u04_cincout.org"

** Unit XYZ: Example

*** Reading

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

*** Summary notes

Even more concise reference page for this topic

*** Lab program

*Goals:* asdfasdf

*Starter code:*  (Also link on repo)

#+BEGIN_SRC cpp :class cpp
  /* STUDENT NAME:
   * SEMESTER/YEAR:
   */

  #include <iostream>
  using namespace std;

  int main()
  {
    // -- PROGRAM CONTENTS GO HERE -----


    return 0;
  }
#+END_SRC

*Test scripts:*

Test scripts included ... (Link on repo)

*How to build and run:*

#+BEGIN_SRC terminal :class terminal
cd FOLDER
g++ FILENAME.cpp -o PROGRAM.out
#+END_SRC

Varius morbi enim nunc faucibus a pellentesque sit amet porttitor. Purus semper eget duis at tellus at urna. Malesuada nunc vel risus commodo viverra maecenas accumsan lacus. Adipiscing vitae proin sagittis nisl rhoncus mattis. Aliquet bibendum enim facilisis gravida neque convallis a cras. Aliquet bibendum enim facilisis gravida neque. Nam libero justo laoreet sit amet cursus sit. Et malesuada fames ac turpis egestas sed tempus urna. Dignissim diam quis enim lobortis scelerisque fermentum. Morbi tempus iaculis urna id volutpat lacus laoreet non curabitur. Cras fermentum odio eu feugiat. Nisl tincidunt eget nullam non. Bibendum est ultricies integer quis auctor elit sed vulputate. Sapien eget mi proin sed libero enim. Vitae congue eu consequat ac.

Tincidunt ornare massa eget egestas. Tristique senectus et netus et. Imperdiet proin fermentum leo vel orci porta non. Tortor at auctor urna nunc. Lectus arcu bibendum at varius vel pharetra. Sed faucibus turpis in eu mi bibendum. Metus vulputate eu scelerisque felis imperdiet proin. Proin nibh nisl condimentum id venenatis a condimentum. Enim neque volutpat ac tincidunt vitae semper quis lectus. Proin sed libero enim sed faucibus turpis in. Vel elit scelerisque mauris pellentesque. Fringilla urna porttitor rhoncus dolor purus non enim praesent.

*** Review Q&A

1. Quis auctor elit sed vulputate?
2. Sed id semper risus in hendrerit gravida rutrum quisque non?
3. Ullamcorper dignissim cras tincidunt lobortis feugiat?

-------------------------------------------------------------
* Semester project

-------------------------------------------------------------
* Problem solving practice

Sudokus here? IDK lol

-------------------------------------------------------------
* Reference

** Basic computer skills

** Tools setup and usage

** Style guide for code and UI

** C++ quick reference

** Common bugs and issues

-----
