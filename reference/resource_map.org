# -*- mode: org -*-

#+TITLE: Work In Progress
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="..//style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

-----

Template file

-----


#+ATTR_HTML: :class console
#+BEGIN_SRC bash
bash
#+END_SRC

- Python :: 
#+BEGIN_SRC python :class python
#+END_SRC

- Lua :: 
#+BEGIN_SRC lua :class lua
#+END_SRC

- C++ :: 
#+BEGIN_SRC cpp :class cpp
#+END_SRC

#+ATTR_HTML: :class action-item
#+NAME: content
#+BEGIN_HTML
*Task:* asdfasdf
#+END_HTML

#+ATTR_HTML: :class hint
#+NAME: content
#+BEGIN_HTML
*Context:* asdfasdf
#+END_HTML


#+BEGIN_QUOTE
Quote thing
#+END_QUOTE
