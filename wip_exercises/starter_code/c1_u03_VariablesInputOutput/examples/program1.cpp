#include <iostream>
#include <string>
using namespace std;

int main()
{
  string name;
  cout << "What is your name? ";
  getline( cin, name );
  cout << "Hello, " << name << endl << endl;

  float x, y;
  cout << "Enter a first number: ";
  cin >> x;
  cout << "Enter a second number: ";
  cin >> y;
  
  float sum = x + y;
  float difference = x - y;
  float product = x * y;
  float quotient = x / y;
  
  cout << x << " + " << y << " = " << sum << endl;
  cout << x << " - " << y << " = " << difference << endl;
  cout << x << " * " << y << " = " << product << endl;
  cout << x << " / " << y << " = " << quotient << endl;

  return 0;
}
