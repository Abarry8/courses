--Copy/paste your recipe from the previous exercise and make the modifications outlined in the instructions.
--If you don't have a recipe program done from last week, you can use this as your starter:
print( "Butter recipe" )
print( "" )
print( "INGREDIENTS" )
print( "* 1 stick - Butter" )
print( "* 5 tbsp - Salt" )
print( "" )
print( "STEPS" )
print( "1. Put butter and salt in microwave safe bowl" )
print( "2. Microwave butter" )
print( "3. Enjoy!!" )