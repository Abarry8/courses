#include <iostream>
using namespace std;

int main()
{
  // 1. Create a float for `points_possible` and for `earned_points`.


  
  // 2. Ask the user "How many points possible?". Get their input, store in `points_possible`.


  
  // 3. Ask the user "How many points earned?". Get their input, store in `earned_points`.


  
  // 4. Calculate their score percent with: earned_points / points_possible * 100. Store this in a new float variable, `score`.


  
  // 5. Display `score` to the screen.


  
  // 6. If the `score` is greater than or equal to 100, then also display "PERFECT!".



  
  // 7. Display "Goodbye" at the end of the program.
  cout << endl << "Goodbye." << endl;
  
  return 0; // program end
}