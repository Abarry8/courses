
print( "----------------------------" )
print( "       MOVIE THEATER        " )
print( "----------------------------" )
print( "\n" )

age = int( input( "Enter customer age: " ) )
price = 0.0

if ( age < 13 ):
  # Child
  price = 6.50
  
else if ( age >= 13 and age <= 18 ):
  # Teenager
  price = 7.75

else if ( age > 18 and age < 60 ):
  # Adult
  price = 9.95
  
else: 
  #  Senior citizen
  price = 5.50

print( "Ticket price is: ${price:.2f}" )

print( "\n" )
print( "Goodbye." )

  