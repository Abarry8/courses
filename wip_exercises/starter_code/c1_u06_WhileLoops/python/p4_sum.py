# 1. Create a variable called `sum` and store a float value 0.0 in it.


# 2. Create a variable called `user_number` and store a float value 0.0 in it.


# 3. Create a while loop that loops while `user_number` is greater than or equal to 0. Within the loop do the following:


#  3a. Display "Sum: " and then the value from the `sum` variable.


#  3b. Tell the user "Enter a non-negative number", and store it in the `user_number` variable.


#  3c. Add `user_number` onto the `sum`: `sum = sum + user_number`.




# (The loop will end once the user enters a negative number)


# Say Goodbye at the end of the program!
print( "\n GOODBYE!" )