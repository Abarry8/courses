#include <iostream>
#include <vector>
using namespace std;

int main()
{
  // 1. Create a VECTOR of FLOATS named "prices", and store several prices (FLOATS) within.

  
  // 2. Create a FLOAT VARIABLE named "total_price" and set it equal to 0.
  
  // 3. Use the FOR LOOP WITHOUT INDEX for (auto& item : prices). Within the loop do the following:

  //    3a. DISPLAY the price (item) to the screen.
  
  
  //    3b. ADD the price (item) onto the total_price.
  
  
  // 4. After the for loop is over, DISPLAY the total price to the screen.
    
  
  
  // Say Goodbye at the end of the program!
  cout << "\n GOODBYE!" << endl;
  
  return 0;
}