#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main()
{
  // 1. Create a VECTOR of strings named ingredient_names. Make it empty.
  
  // 2. Create a VECTOR of floats named ingredient_amounts. Make it empty.
  
  // 3. Create an int VARIABLE called ingredient_amount. Ask the user how many ingredients there are and store it in this variable.
  
  
  // 4. Create a for loop that runs `ingredient_amount` times. for ( int i = 0; i < ... ; i++ ) Do the following within the loop:
  
  //   4a. Create a variable "new_name" and ask the user to enter an ingredient name. (This should be a string.)
  
  //   4b. Create a variable "new_amount" and ask the user to enter an ingredient amount. (This should be a float.)
  
  //   4c. Add `new_name` to the `ingredient_names` list.
  
  //   4d. Add `new_amount` to the `ingredient_amounts` list.
  
  
  // 5. Once the loop is done, use another for loop (for i in range...) to display all ingredient_names[i] and ingredient_amounts[i].


  
  // Say Goodbye at the end of the program!
  cout << "\n GOODBYE!" << endl;
  
  return 0;
}