classes = [ "CS134", "CS200", "CS210" ]

print( "The size of the classes list is", len(classes) )

print()
print( "LOOP STYLE 1:" )
for i in range( len( classes ) ):
  print( "Item #", i, " is:", classes[i] )

print()
print( "LOOP STYLE 2:" )
for item in classes:
  print( "Item:", item )