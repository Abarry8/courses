# 1. Create a LIST named "prices", and store several prices (FLOATS) within.


# 2. Create a VARIABLE named "total_price" and set it equal 0.


# 3. Use the FOR LOOP WITHOUT INDEX (for item in prices). Within the loop do the following:

#    3a. DISPLAY the price (item) to the screen.


#    3b. ADD the price (item) onto the total_price.


# 4. After the for loop is over, DISPLAY the total price to the screen.