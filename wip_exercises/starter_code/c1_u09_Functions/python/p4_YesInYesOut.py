# Function definition
def GetAverage( num1, num2, num3 ):
  return (num1+num2+num3)/3
# End of function


# PROGRAM CODE
number1 = 0
number2 = 0
number3 = 0
average = 0

# Ask the user to enter a first number, store in `number1`.

# Ask the user to enter a second number, store in `number2`.

# Ask the user to enter a third number, store in `number3`.

# Call the GetAverage function, passing in `number1`, `number2`, and `number3`. Store the returned result in the `average` variable.

# Display the "The average is:" and then the `average` variable.