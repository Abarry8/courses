/* STUDENT NAME: 
 * SEMESTER/YEAR: 
*/

#include <iostream>
using namespace std;

/* Create two float variables, num1 and num2.
 * Convert argument1 and argument2 to floats and store in those variables.
 * Create a third float variable, product. Store the result of num1 * num2 in product.
 * Display the result like this:
 * A*B=C
 * Where A is num1, B is num2, and C is the product.
 * */

int main( int argCount, char* args[] )
{
  if ( argCount < 3 ) { cout << "Not enough arguments!" << endl; return 1; }
  
  float num1 = stof( args[1] );
  float num2 = stof( args[2] );
  float product = num1*num2;
  
  cout << num1 << "*" << num2 << "=" << product << endl;
  
  return 0;
}
