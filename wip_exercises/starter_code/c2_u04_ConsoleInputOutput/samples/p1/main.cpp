/* STUDENT NAME: 
 * SEMESTER/YEAR: 
*/

#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int main()
{
  string name;
  cout << "What is your name? ";
  cin >> name;

  float myFloat;
  cout << "Enter a float: ";
  cin >> myFloat;

  cout << "Hello " << name << ", why " << myFloat << "?" << endl;

  
  return 0;
}
