/* STUDENT NAME: 
 * SEMESTER/YEAR: 
*/

#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int main()
{
  string name;
  cout << "Product name: ";
  //cin >> name;
  getline( cin, name );

  float price;
  cout << "Price: ";
  cin >> price;

  cin.ignore();

  string color;
  cout << "Color: ";
  getline( cin, color );


  cout << name << ", " << color << ", $" << price << endl;
  
  return 0;
}
