--------------------------------------------------------
INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------
For this program we are going to be using the `cin >>` statement
to get input from the keyboard and store it in a variable.
"cin" stands for "console in", and >> is known as the "input stream operator".

-- BUILDING YOUR PROGRAM --
cd studentA/p1_cinstream
g++ pizza.cpp -o pizza.out

-- EXAMPLE PROGRAM OUTPUT --
./pizza.out
There are 8 slices of pizza
How many people are there? 3
You entered: 3
Servings per 1 person: 2.66667

./pizza.out
There are 8 slices of pizza
How many people are there? 4
You entered: 4
Servings per 1 person: 2

--------------------------------------------------------
PROGRAM INSTRUCTIONS
--------------------------------------------------------

Create three variables: servings (float), name (string), people (int).

Initialize servings to 8 and name to "pizza".

Display the servings and the name to the user.

Ask the user how many people there are.

Get their input with a cin statement, store it in the people variable.

Display what the user entered.

Calculate the servings per person and store it in a new servingsPerPerson variable (float).
   Calculation is servings / people.
   
Display the servingsPerPerson to the user.

