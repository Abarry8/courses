--------------------------------------------------------
INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------

-- BUILDING YOUR PROGRAM --
g++ raise.cpp -o raise.out


-- EXAMPLE PROGRAM OUTPUT --
./raise.out 10000 5 3
year: 1, pay: $10000.00, increase: $500.00
year: 2, pay: $10500.00, increase: $525.00
year: 3, pay: $11025.00, increase: $551.25
year: 4, pay: $11576.25

./raise.out 60000 10 4
year: 1, pay: $60000.00, increase: $6000.00
year: 2, pay: $66000.00, increase: $6600.00
year: 3, pay: $72600.00, increase: $7260.00
year: 4, pay: $79860.00, increase: $7986.00
year: 5, pay: $87846.00


--------------------------------------------------------
PROGRAM INSTRUCTIONS
--------------------------------------------------------

ARGUMENTS: This program expects additional arguments:
  `basepay` (float), from args[1],
  `raise` (float), from args[2],
  `years` (int), from args[3]
                    
1. Load the arguments into their variables.

2. Create a pay float variable, initialize it to the basepay.

3. Create a counter integer variable, initialize it to 1.

4. While the counter is less than or equal to the years, do the following:
    a) Create a increase float variable. To calculate how much the pay will increase by, multiply pay by (raise/100). Store it in this variable.
    b) Display “year: ” counter “, pay: $” pay “, increase: $” increase.
    c) Add the increase to the pay, storing it in the pay variable.
    d) Increment counter by 1.
    
5. After the while loop, display “year: ” counter “, pay: $” pay.
