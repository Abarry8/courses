--------------------------------------------------------
INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------

-- CREATING FUNCTIONS --
When creating functions in C++ we need to have a DECLARATION and a DEFINITION.

A DECLARATION contains the function header, with a semi-colon at the end:
RETURNTYPE FUNCTIONAME( TYPE1 VAR1, TYPE2 VAR2 );

A DEFINITION contains the function header, plus a code block { } and the contents of the function:
RETURNTYPE FUNCTIONNAME( TYPE1 VAR1, TYPE2 VAR2 )
{
  // Stuff the function does
}

Function DECLARATIONS must go in .h header files.
Function DEFINITIONS must go in .cpp source files.

Why are there two types of source files in C++? C++ is old and utilizes a compiler and a linker. The .h file is to be included in any other source files that will be including functions from that file set. It is a way to tell the program, "hey, look out for this thing later, which will be defined elseware". Header files tell you about functions' and classes' structures, but not HOW they work - that's for the definitions, within the .cpp files.


-- CALLING FUNCTIONS --
Once a function has been declared, such as:
int WhichIsBigger( int a, int b );

We can call that function elseware within our program. A function call tells the program to stop where it is currently and jump to the code within that function. Once the code in the function has finished, then it returns back to where it was before and continues on with the program execution.
To call a function, we use its NAME and pass in ARGUMENTS. Note that we DO NOT specify data types during the function call.

```
int num1 = 4, num2 = 3;
int bigger = WhichIsBigger( num1, num2 );
```


-- FILE GUARDS --
When the compiler is working through all the code in a project, it basically takes any #include statements and copy/pastes the contents of that file into whatever file is including it. Because of this, if multiple files contain the same .h file, without FILE GUARDS the compiler will copy/paste it multiple times, and then think you're reusing the same function names over and over - which will result in a build error.
Within HEADER files we need to make sure to include file guards.

The platform agnostic file guards are:
```
#ifndef _LABEL
#define _LABEL

// Declarations go here

#endif
```

Note that each .h file should have its own unique LABEL. Usually, you'll find the file guard label to mirror the name of the file itself. DON'T REUSE FILE GUARD LABELS, otherwise you're basically telling the compiler to not include other .h files that have the same label.

In Visual Studio it will add `#pragma once` to do the same job, but you should be using CROSS-PLATFORM STANDARD C++ in your programs.


-- INCLUDE OTHER FILES --
When you've declared functions in a .h file and want to use those functions elseware in the program (usually .cpp files), you will need to #include it in the file(s) that will be using it:
```
#include "Functions.h"
```

NOTE: ONLY EVER INLCUDE .h FILES! NEVER INCLUDE .cpp FILES! This will result in build errors.


-- FUNCTIONS: NO INPUTS, NO OUTPUTS -- 
Functions like these usually just display information to the user, such as a menu.

They do not receive information and they do not return any information.


-- BUILDING YOUR PROGRAM --
You will need to make sure to build ALL header and source files now.

cd studentA/p1_NoInNoOut
g++ *.h *.cpp -o program.out


-- EXAMPLE PROGRAM OUTPUT --
./program.out 
Recommended movie: The Lost Skeleton of Cadavra

--------------------------------------------------------
PROGRAM INSTRUCTIONS
--------------------------------------------------------

Within the Functions.h file, create a function DECLARATION that meets the following criteria:
* Name: RecommendMovie
* Input Parameters: NONE
* Output Return: NONE (void)

Within the Functions.cpp file, you will add your function DEFINITION. The function header needs to match the header from the .h file exactly, except with no `;` at the end. Instead, you'll have a code block { } and put program logic within.

Within the function, use a `cout` statement to display a recommended movie to the user.

Within main(), call the function. Since this function takes no input parameters, its parentheses will be empty. Since it does not return any data, we don't need to create a variable to store its result:
```
RecommendMovie();
```







