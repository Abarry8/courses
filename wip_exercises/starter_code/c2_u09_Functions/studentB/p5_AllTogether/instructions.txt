--------------------------------------------------------
INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------

-- BUILDING YOUR PROGRAM --
You will need to make sure to build ALL header and source files now.

cd studentB/p5_AllTogether
g++ *.h *.cpp -o program.out


-- EXAMPLE PROGRAM OUTPUT --
./program.out 
What is the price of the item? $25.50
What is the city's tax percent? %9.1
Price after tax: $27.82

--------------------------------------------------------
PROGRAM INSTRUCTIONS
--------------------------------------------------------

FUNCTIONS - Declare in Functions.h, define in Functions.cpp

1. GetInput function
    Input parameters: message - string
    Output return: float
    Functionality: Use `cout` to display the `message`, then use a `cin` statement and have the user enter a float value. `return` that float.

2. IsValid function
    Input parameters: value - float
    Output return: bool
    Functionality: If the `value` passed in is less than or equal to 0, then return `false`. Otherwise, return `true`.

3. GetPricePlusTax function
    Input parameters: price - float, taxPercent - float
    Output return: float
    Functionality: Calculate the price of the item after the tax amount is added to it. Return the calculated result.
    ADDITIONAL TAX: price * taxPercent/100
    PRICE AFTER SALE: price + ADDITIONAL TAX
  

MAIN PROGRAM - Do the following:
1. Create a float `price` variable and call the `GetInput` function to initialize its value - ask the user "What is the price of the item? $"
2. Create a float `tax` variable and call the `GetInput` function to initialize its value - ask the user "What is the city's tax percent? %"
3. Do an error check - if `price` is INVALID, or if `tax` is INVALID, then display "INVALID VALUE!" and `return 1;` to quit the program, symbolizing an error has occurred.
4. Create a float `pricePlusTax` variable. Call the `GetPricePlusTax` function, passing in the `price` and `tax`. Store its result in your `pricePlusTax` variable.
5. Use a `cout` statement to display the resulting price after tax.
