--------------------------------------------------------
INTRODUCTION (Scroll down further for program instructions)
--------------------------------------------------------

-- FUNCTIONS: YES INPUTS, YES OUTPUTS -- 
Functions like these often take in some data and use it in a formula or algorithm and return some sort of result.

These are most similar to the functions you learned about in algebra, like f(x)=x^2.

-- BUILDING YOUR PROGRAM --
You will need to make sure to build ALL header and source files now.

cd studentC/p4_YesInYesOut
g++ *.h *.cpp -o program.out


-- EXAMPLE PROGRAM OUTPUT --
./program.out 
Enter numerator: 1
Enter denominator: 2
As a decimal, it's: 0.5

./program.out
Enter numerator: 3
Enter denominator: 4
As a decimal, it's: 0.75

--------------------------------------------------------
PROGRAM INSTRUCTIONS
--------------------------------------------------------

Within the Functions.h file, create a function DECLARATION that meets the following criteria:
* Name: FractionToDecimal
* Input Parameters: numerator - float, denominator - float
* Output Return: float

Within the Functions.cpp file, you will add your function DEFINITION. The function header needs to match the header from the .h file exactly, except with no `;` at the end. Instead, you'll have a code block { } and put program logic within.
Within the function, calculate the fraction as a decimal, simply by dividing `numerator/denominator`. Make sure to return the result.


Within main() do the following:
1. Create float variables for `numerator` and `denominator`.
2. Ask the user to enter values for both of the variables.
3. Create an float variable named `result`. Call the `FractionToDecimal` function, passing in your inputs. Store the result in the `result` variable.
4. Display "As a decimal, it's: " and then the value of the `result` variable.






