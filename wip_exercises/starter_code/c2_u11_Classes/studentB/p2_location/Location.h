/* STUDENT NAME:
 * SEMESTER/YEAR:
 * */
#ifndef _LOCATION
#define _LOCATION

#include <string>
using namespace std;

// -- ADD STRUCT DECLARATION HERE --------------------------------------------------

class Location
{
  public:
  void Setup( int newLocId, string newAddress, float newTax );  
  void PrintTableRow() const;
  
  private:
  int locationId;
  string streetAddress;
  float salesTaxPercent;
};

// -- ADD FUNCTION DECLARATIONS HERE --------------------------------------------------

void SetupLocations( Location& l1, Location& l2 );
void PrintLocationTable( const Location& l1, const Location& l2 );

#endif
