/*
YOU DO NOT NEED TO MODIFY THIS FILE.
*/

#ifndef _CARD
#define _CARD

#include <string>
using namespace std;

// -- CLASS DECLARATION GOES HERE --------------------------------------------------

class Card
{
    private:
    string m_rank;    // Rank (A, 2-10, J, Q, K)
    char m_suit;      // Suit (Diamonds, Spades, Hearts, Clubs)

    public:
    void Setup( string newRank, char newSuit ); // Set up a card's values
    void Display();                             // Show a card's info

    friend void CardTest();                     // Helps the tester (ignore)
};

#endif
