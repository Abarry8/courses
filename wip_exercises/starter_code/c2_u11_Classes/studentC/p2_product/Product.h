/* STUDENT NAME:
 * SEMESTER/YEAR:
 * */
#ifndef _PRODUCT
#define _PRODUCT

#include <string>
using namespace std;

// -- ADD STRUCT DECLARATION HERE --------------------------------------------------

class Product
{
  public:
  void Setup( int newProdId, string newName, float newPrice, int newCalories );
  void PrintTableRow() const;
  
  private:
  int productId;
  string name;
  float price;
  int calories;
};

// -- ADD FUNCTION DECLARATIONS HERE --------------------------------------------------

void SetupProducts( Product& p1, Product& p2, Product& p3 );
void PrintProductTable( const Product& p1, const Product& p2, const Product& p3 );

#endif
