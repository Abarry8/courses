/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ b_program2.cpp -o studentBprogram2.exe
 * RUN PROGRAM: ./studentBprogram2.exe low high
 *
 * 1. Load args[1] into a `low` int
 * 2. Load args[2] into a `high` int
 * 3. Use a for loop that uses an iterator `i`:
 *    START: at `high`
 *    CONDITION: while `i` is greater than or equal to `low`.
 *    UPDATE: Decrement `i` by 3 each time.
 *    3a. Within the for loop, display the value of `i` and then a space.
 * 
 * EXAMPLE OUTPUT:
 * ./studentAprogram1.exe 1 10
 * 1 3 5 7 9
 * */

#include <iostream>
using namespace std;

int main( int argCount, char *args[] ) 
{
  if (argCount < 2) { cout << "NOT ENOUGH ARGUMENTS! Expected form: " << args[0] << " low high" << endl; return 1; }
  
  int low = stoi(args[1]);
  int high = stoi(args[2]);

  // TODO: Create for loop


  return 0;
}
