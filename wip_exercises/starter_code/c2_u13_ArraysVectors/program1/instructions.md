# Traditional Array

  ## Creating a traditional array
  
    With a traditional array we need to create a *named constant* to
    store the size of our array. The traditional array's size cannot be changed,
    so that's why we made it a `const`. Also, the size of an array cannot
    be negative, so by using `unsigned` with our int, we ensure the `SIZE`
    will be 0 or greater.
    
    When declaring the array we need to specify the data type of the elements
    (`string`) and the size of the array (`SIZE`).
  
```
const unsigned int SIZE = 5;
string arr[SIZE];
```

  ## Adding items to a traditional array
  
    We can add items to the array by specifying an *index* within the
    subscript operator `[ ]`, and giving that element a value, like:
    `ARRAYNAME[ INDEX ] = VALUE;`
    So we can initialize our elements like this:

```
arr[0] = "Kansas";
arr[1] = "Missouri";
arr[2] = "Nebraska";
arr[3] = "Iowa";
arr[4] = "Oklahoma";
```

  ## Iterating over the traditional array to display its elements
    
    With our for loop, we set `i` as an `unsigned int` because, again,
    the size cannot be negative. We want to keep looping while `i < SIZE`,
    and go up by 1 each time so we use `i++` to increment our counter variable (`i`).
    
    Within the loop, we can display each element's *index number* by displaying `i`,
    and display each element's *value* by displaying `arr[i]`.
  
```
for ( unsigned int i = 0; i < SIZE; i++ )
{
  cout << "arr[" << i << "] = " << arr[i] << endl;
}
```


-----

# Dynamic Array

  ## Creating a dynamic array
  
    Dynamic arrays must be created via pointers. We use the `new` keyword
    to allocate space, and specify a `size`. The size is a variable,
    and can be entered by the user or given a number value.

```
unsigned int size = 5;  
string* arr = new string[size];
```

  ## Adding items to a dynamic array
  
    We can access each element of the array via its index as normal:

```
arr[0] = "Kansas";
arr[1] = "Missouri";
arr[2] = "Nebraska";
arr[3] = "Iowa";
arr[4] = "Oklahoma";
```

  ## Iterating over the dynamic array to display its elements
  
    We can utilize the `size` variable in our for loop now:
  
```
for ( unsigned int i = 0; i < size; i++ )
{
  cout << "arr[" << i << "] = " << arr[i] << endl;
}
```

  ## Freeing memory from the dynamic array
  
    Since we used the `new` keyword to allocate space for the array
    originally, we need to make sure to use the `delete` keyword to
    free that memory before the end of the scope.

```
delete [] arr;
```

-----

# STL Array

  ## Creating a STL array
  
    The STL Array is basically a traditional array. We need to declare
    it by setting the data type of the array (string) as the first part
    and the size (5) as the second part. This array cannot be resized.

```
array<string, 5> arr;
```

  ## Adding items to a STL array
  
    We can assign values to elements normally via the index:

```
arr[0] = "Kansas";
arr[1] = "Missouri";
arr[2] = "Nebraska";
arr[3] = "Iowa";
arr[4] = "Oklahoma";
```

  ## Iterating over the STL array to display its elements

    The STL Array gives us a `size()` function so we don't have
    to keep track of the array size ourselves with our own variable.

```
for ( unsigned int i = 0; i < arr.size(); i++ )
{
  cout << "arr[" << i << "] = " << arr[i] << endl;
}
```

-----

# STL Vector

  ## Creating a STL vector
  
    A vector is basically a dynamic array except we don't need to
    deal with the pointers. We just declare the vector and what
    type of data it is storing:

```
vector<string> arr;
```

  ## Adding items to a STL vector

    Since the vector needs to be able to adjust its size,
    we cannot access items with the subscript operator `[ ]`
    until we've actually added them. To add items to the vector,
    we need to use its `push_back` function.

```
arr.push_back( "Kansas" );
arr.push_back( "Missouri" );
arr.push_back( "Nebraska" );
arr.push_back( "Iowa" );
arr.push_back( "Oklahoma" );
```

  ## Iterating over the STL vector to display its elements

    We can use the vector's `size()` function to get its size,
    and then access already-stored elements of the vector
    using the subscript operator `[ ]`

```
for ( unsigned int i = 0; i < arr.size(); i++ )
{
  cout << "arr[" << i << "] = " << arr[i] << endl;
}
```
