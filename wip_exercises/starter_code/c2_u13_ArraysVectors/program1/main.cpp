#include <iostream>
#include <array>
#include <vector>
#include <string>
using namespace std;

void TraditionalArray()
{
  cout << endl << string( 10, '-' ) << " Traditional Array " << string( 10, '-' ) << endl;
  
  // Create array
 
  // Set items
  
  // Display elements
}

void DynamicArray()
{
  cout << endl << string( 10, '-' ) << " Dynamic Array " << string( 10, '-' ) << endl;
  
  // Create array
  
  // Set items
  
  // Display elements
  
  // Free memory  
}

void STLArray()
{
  cout << endl << string( 10, '-' ) << " STL Array " << string( 10, '-' ) << endl;
  
  // Create array
  
  // Set items
  
  // Display elements
}

void STLVector()
{
  cout << endl << string( 10, '-' ) << " STL Vector " << string( 10, '-' ) << endl;
  
  // Create vector
  
  // Set items
  
  // Display elements
}

int main()
{
  TraditionalArray();
  DynamicArray();
  STLArray();
  STLVector();
  
  return 0;
}
