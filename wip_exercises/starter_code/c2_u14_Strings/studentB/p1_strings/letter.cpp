/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ letter.cpp -o letter.exe
 * RUN PROGRAM: ./letter.exe string
 *
 * 
 * */
 
 #include <iostream>
 #include <string>
 using namespace std;
 
 int main( int argCount, char* args[] )
 {
   if ( argCount < 3 )
   {
     cout << "EXPECTED FORM: " << args[0] << " string index" << endl;
     return 1;
   }
   
   string str = string( args[1] );
   int index = stoi( args[2] );
   cout << "Letter #" << index << " is: ";
   // TODO: Use the subscript operator

   
   return 0;
 }
