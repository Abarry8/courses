/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ insert.cpp -o insert.exe
 * RUN PROGRAM: ./remove.exe string index length
 *
 * 
 * */
 
 #include <iostream>
 #include <string>
 using namespace std;
 
 int main( int argCount, char* args[] )
 {
   if ( argCount < 4 )
   {
     cout << "EXPECTED FORM: " << args[0] << " string index length" << endl;
     return 1;
   }
   
   string string1 = string( args[1] );
   int index = stoi( args[2] );
   int length = stoi( args[3] );
   
   // TODO: Use the erase functions

   cout << "string1 is now " << string1 << endl;
   
   return 0;
 }
