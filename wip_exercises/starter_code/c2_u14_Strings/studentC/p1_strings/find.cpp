/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ find.cpp -o find.exe
 * RUN PROGRAM: ./find.exe string1 string2
 *
 * 
 * */
 
 #include <iostream>
 #include <string>
 using namespace std;
 
 int main( int argCount, char* args[] )
 {
   if ( argCount < 3 )
   {
     cout << "EXPECTED FORM: " << args[0] << " string1 string2" << endl;
     return 1;
   }
   
   string string1 = string( args[1] );
   string string2 = string( args[2] );
   int pos;
   
   // TODO: Use find function

   cout << "Position of string2 is " << pos << endl;
   
   return 0;
 }
