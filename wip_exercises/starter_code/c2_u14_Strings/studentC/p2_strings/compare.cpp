/* STUDENT NAME: 
 * SEMESTER/YEAR: 
 * 
 * BUILD PROGRAM: g++ compare.cpp -o compare.exe
 * RUN PROGRAM: ./compare.exe string1 string2
 *
 * 
 * */
 
 #include <iostream>
 #include <string>
 using namespace std;
 
 int main( int argCount, char* args[] )
 {
   if ( argCount < 3 )
   {
     cout << "EXPECTED FORM: " << args[0] << " string1 string2" << endl;
     return 1;
   }
   
   string string1 = string( args[1] );
   string string2 = string( args[2] );
   
   // TODO: Compare the two strings with <, >, and ==   

   
   return 0;
 }
