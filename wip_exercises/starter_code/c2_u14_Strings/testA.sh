#! /bin/bash
# $? = Specifies the exit status of the last command or the most recent execution process.
RED='\033[0;31m'; GREEN='\033[0;32m'; NC='\033[0m' # No Color
ACOL='\033[0;34m'; BCOL='\033[0;35m'; CCOL='\033[0;36m'
STUDENTCOL=ACOL
student='studentA'

programs=("${student}/p1_strings/" "${student}/p2_strings/" "${student}/p3_strings/")
executables=()

declare -A TESTS
# TEST 0 - PROGRAM 0 ----------------------------------------------
i=0
TESTS[$i,0]=0                                      # programs index
TESTS[$i,1]="hello"                                # arguments
TESTS[$i,2]="Length is: 5"                         # expected output
TESTS[$i,3]=""                                     # runtime values
TESTS[$i,4]="automated"                            # automated or manual test?
# TEST 1 - PROGRAM 0 ----------------------------------------------
i=$((i+1))
TESTS[$i,0]=0                                      # programs index
TESTS[$i,1]="hamburger"                            # arguments
TESTS[$i,2]="Length is: 9"                         # expected output
TESTS[$i,3]=""                                     # runtime values
TESTS[$i,4]="automated"                            # automated or manual test?
# TEST 2 - PROGRAM 1 ----------------------------------------------
i=$((i+1))
TESTS[$i,0]=1                                      # programs index
TESTS[$i,1]="cheese burger"                        # arguments
TESTS[$i,2]="string3 is cheeseburger"              # expected output
TESTS[$i,3]=""                                     # runtime values
TESTS[$i,4]="automated"                            # automated or manual test?
# TEST 2 - PROGRAM 2 ----------------------------------------------
i=$((i+1))
TESTS[$i,0]=1                                      # programs index
TESTS[$i,1]="cat dog"                              # arguments
TESTS[$i,2]="string3 is catdog"                    # expected output
TESTS[$i,3]=""                                     # runtime values
TESTS[$i,4]="automated"                            # automated or manual test?
# TEST 2 - PROGRAM 1 ----------------------------------------------
i=$((i+1))
TESTS[$i,0]=2                                      # programs index
TESTS[$i,1]="wolf erew 1"                          # arguments
TESTS[$i,2]="string1 is now werewolf"              # expected output
TESTS[$i,3]=""                                     # runtime values
TESTS[$i,4]="automated"                            # automated or manual test?
# TEST 2 - PROGRAM 2 ----------------------------------------------
i=$((i+1))
TESTS[$i,0]=2                                      # programs index
TESTS[$i,1]="homerunner star 4"                    # arguments
TESTS[$i,2]="string1 is now homestarrunner"        # expected output
TESTS[$i,3]=""                                     # runtime values
TESTS[$i,4]="automated"                            # automated or manual test?

i=$((i+1))
TOTALTESTS=$i


# ---------------------------------------------------------------------- WHICH STUDENT?
echo -e "\n TESTS FOR ${student}"; head -1 "${programs[0]}size.cpp"
# ---------------------------------------------------------------------- BUILD PROGRAMS
echo -e "\n === BUILD PROGRAMS ==="
for i in ${!programs[@]}; do
  niceIndex=$((i+1))
  outname="${programs[i]}${student}_program$niceIndex.out"
  g++ ${programs[i]}*.cpp -o ${outname}
  if [ "$?" -eq 0 ]; then
    echo -e "${GREEN}Successfully built [${outname}] ${NC}"
    executables+=(${outname})
  else
    echo -e "${RED}FAILED to build [${outname}]${NC}"
  fi  
done
# ---------------------------------------------------------------------- TEST PROGRAMS
echo -e "\n === TEST PROGRAMS ==="
for ((i=0; i<${TOTALTESTS}; i++))
do  
  PROGRAMI=${TESTS[$i,0]}
  PROGARGS=${TESTS[$i,1]}
  EXPECTED=${TESTS[$i,2]}
  RUNTIMEV=${TESTS[$i,3]}
  TESTTYPE=${TESTS[$i,4]}
  EXECUTBL=${executables[${PROGRAMI}]}

  RUNNER="./$EXECUTBL"
  if [ "$PROGARGS" != "" ]; then
    RUNNER="./$EXECUTBL $PROGARGS"
  elif [ "$RUNTIMEV" != "" ]; then
    RUNNER="echo $RUNTIMEV | ./$EXECUTBL"
  fi
  
    
  if [ "$TESTTYPE" == "manual" ]; then
    echo -e "TEST ${i}: (MANUAL-CHECK TEST)"
    eval "$RUNNER"
    echo -e "* EXPECTED OUTPUT: [${EXPECTED}]"
    
  else

    ACTUAL=$(eval "$RUNNER")
    if [ "${ACTUAL}" == "${EXPECTED}" ]; then
      echo -e "* ${GREEN}TEST ${i} SUCCESS! PROGRAM:[$RUNNER] OUTPUT: [${ACTUAL}]${NC}"
    else
      echo -e "* ${RED}TEST #${i} FAIL! PROGRAM:[$RUNNER] \n\t* EXPECTED OUTPUT: [${EXPECTED}] \n\t* ACTUAL OUTPUT:   [$ACTUAL]${NC}"
    fi
  fi
    
done
