#ifndef _PRODUCT
#define _PRODUCT

#include <string>

class Product
{
public:
	// Overloading constructors
	Product();
	Product(std::string newName, int newQuantity, float newPrice);
	Product(const Product& copyMe);

	// Default parameters 
	void Setup(std::string newName = "unset", int newQuantity = 0, float newPrice = 0);
	// void Setup(std::string newName);

	std::string GetName() const;
	int GetQuantityInStock() const;
	float GetPrice() const;

protected:
	std::string m_name;
	int m_quantityInStock;
	float m_price;
};

#endif