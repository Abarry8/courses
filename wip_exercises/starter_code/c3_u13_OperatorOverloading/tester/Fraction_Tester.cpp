#include "Fraction_Tester.hpp"
#include "Helper.hpp"

#include <string>
#include <iostream>
#include <iomanip>
using namespace std;

int Tester::Test_GetDecimal()
{
  string fnName = "GetDecimal";
  StartTestSet( fnName, { fnName } );

  /* Test Begin */ { StartTest( "Check if 1/2 is converted to the proper decimal" );
  int input_num = 1; int input_denom = 2; float out_expected = 0.5;
  Fraction fraction; fraction.m_num = input_num; fraction.m_denom = input_denom;
  float out_actual = fraction.GetDecimal();
  Set_ExpectedOutput  ( "fraction.GetDecimal()", out_expected );
  Set_ActualOutput    ( "fraction.GetDecimal()", out_actual );
  Set_Comments( "Numerator:   " + Helper::ToString( input_num ) );
  Set_Comments( "Denominator: " + Helper::ToString( input_denom ) );
  if ( out_actual == out_expected ) { TestPass(); }
  else                              { TestFail(); }
  /* Test End   */ FinishTest(); }


  /* Test Begin */ { StartTest( "Check if 5/1 is converted to the proper decimal" );
  int input_num = 5; int input_denom = 1; float out_expected = 5;
  Fraction fraction; fraction.m_num = input_num; fraction.m_denom = input_denom;
  float out_actual = fraction.GetDecimal();
  Set_ExpectedOutput  ( "fraction.GetDecimal()", out_expected );
  Set_ActualOutput    ( "fraction.GetDecimal()", out_actual );
  Set_Comments( "Numerator:   " + Helper::ToString( input_num ) );
  Set_Comments( "Denominator: " + Helper::ToString( input_denom ) );
  if ( out_actual == out_expected ) { TestPass(); }
  else                              { TestFail(); }
  /* Test End   */ FinishTest(); }

  FinishTestSet();
  return TestResult();
}

int Tester::Test_CommonDenominatorize()
{
  string fnName = "CommonDenominatorize";
  StartTestSet( fnName, { fnName } );

  /* Test Begin */ { StartTest( "Check common denominator for 1/2 and 3/4" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 1; fraction1.m_denom = 2;
  fraction2.m_num = 3; fraction2.m_denom = 4;

  Fraction out_expected; out_expected.m_num = 4; out_expected.m_denom = 8;
  Fraction out_actual = fraction1.CommonDenominatorize( fraction2 );

  Set_ExpectedOutput  ( "fraction1.CommonDenominatorize( fraction2 )", Helper::ToString( out_expected.m_num ) + "/" + Helper::ToString( out_expected.m_denom ) );
  Set_ActualOutput    ( "fraction1.CommonDenominatorize( fraction2 )", Helper::ToString( out_actual.m_num ) + "/" + Helper::ToString( out_actual.m_denom ) );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual.m_num   != out_expected.m_num )   { TestFail(); }
  else if ( out_actual.m_denom != out_expected.m_denom ) { TestFail(); }
  else                                                   { TestPass(); }
  /* Test End   */ FinishTest(); }

  /* Test Begin */ { StartTest( "Check common denominator for 2/3 and 1/5" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 2; fraction1.m_denom = 3;
  fraction2.m_num = 1; fraction2.m_denom = 5;

  Fraction out_expected; out_expected.m_num = 10; out_expected.m_denom = 15;
  Fraction out_actual = fraction1.CommonDenominatorize( fraction2 );

  Set_ExpectedOutput  ( "fraction1.CommonDenominatorize( fraction2 )", Helper::ToString( out_expected.m_num ) + "/" + Helper::ToString( out_expected.m_denom ) );
  Set_ActualOutput    ( "fraction1.CommonDenominatorize( fraction2 )", Helper::ToString( out_actual.m_num ) + "/" + Helper::ToString( out_actual.m_denom ) );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual.m_num   != out_expected.m_num )   { TestFail(); }
  else if ( out_actual.m_denom != out_expected.m_denom ) { TestFail(); }
  else                                                   { TestPass(); }
  /* Test End   */ FinishTest(); }

  /* Test Begin */ { StartTest( "Check that if both fractions have same denominator, it remains the same" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 2; fraction1.m_denom = 3;
  fraction2.m_num = 1; fraction2.m_denom = 3;

  Fraction out_expected; out_expected.m_num = 2; out_expected.m_denom = 3;
  Fraction out_actual = fraction1.CommonDenominatorize( fraction2 );

  Set_ExpectedOutput  ( "fraction1.CommonDenominatorize( fraction2 )", Helper::ToString( out_expected.m_num ) + "/" + Helper::ToString( out_expected.m_denom ) );
  Set_ActualOutput    ( "fraction1.CommonDenominatorize( fraction2 )", Helper::ToString( out_actual.m_num ) + "/" + Helper::ToString( out_actual.m_denom ) );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual.m_num   != out_expected.m_num )   { TestFail(); }
  else if ( out_actual.m_denom != out_expected.m_denom ) { TestFail(); }
  else                                                   { TestPass(); }
  /* Test End   */ FinishTest(); }

  FinishTestSet();
  return TestResult();
}

int Tester::Test_AssignmentOperator()
{
  string fnName = "AssignmentOperator";
  StartTestSet( fnName, { fnName } );

  /* Test Begin */ { StartTest( "Check that assignment operators copies fraction (Test 1)" );
  Fraction copyMe;
  copyMe.m_num = 50;
  copyMe.m_denom = 100;

  Fraction out_expected; out_expected.m_num = 50; out_expected.m_denom = 100;
  Fraction out_actual;
  out_actual = copyMe;

  Set_ExpectedOutput  ( "Copied fraction", Helper::ToString( out_expected.m_num ) + "/" + Helper::ToString( out_expected.m_denom ) );
  Set_ActualOutput    ( "Copied fraction", Helper::ToString( out_actual.m_num ) + "/" + Helper::ToString( out_actual.m_denom ) );
  Set_Comments( "Original fraction:   " + Helper::ToString( copyMe.m_num ) + "/" + Helper::ToString( copyMe.m_denom ) );
  if      ( out_actual.m_num   != out_expected.m_num )   { TestFail(); }
  else if ( out_actual.m_denom != out_expected.m_denom ) { TestFail(); }
  else                                                   { TestPass(); }
  /* Test End   */ FinishTest(); }

  /* Test Begin */ { StartTest( "Check that assignment operators copies fraction (Test 2)" );
  Fraction copyMe;
  copyMe.m_num = 99;
  copyMe.m_denom = 99;

  Fraction out_expected; out_expected.m_num = 99; out_expected.m_denom = 99;
  Fraction out_actual;
  out_actual = copyMe;

  Set_ExpectedOutput  ( "Copied fraction", Helper::ToString( out_expected.m_num ) + "/" + Helper::ToString( out_expected.m_denom ) );
  Set_ActualOutput    ( "Copied fraction", Helper::ToString( out_actual.m_num ) + "/" + Helper::ToString( out_actual.m_denom ) );
  Set_Comments( "Original fraction:   " + Helper::ToString( copyMe.m_num ) + "/" + Helper::ToString( copyMe.m_denom ) );
  if      ( out_actual.m_num   != out_expected.m_num )   { TestFail(); }
  else if ( out_actual.m_denom != out_expected.m_denom ) { TestFail(); }
  else                                                   { TestPass(); }
  /* Test End   */ FinishTest(); }

  FinishTestSet();
  return TestResult();
}

int Tester::Test_AdditionOperator()
{
  string fnName = "AdditionOperator";
  StartTestSet( fnName, { fnName } );

  /* Test Begin */ { StartTest( "Check that 3/4 + 1/4 is added correctly" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 3; fraction1.m_denom = 4;
  fraction2.m_num = 1; fraction2.m_denom = 4;

  Fraction out_expected; out_expected.m_num = 4; out_expected.m_denom = 4;
  Fraction out_actual = fraction1 + fraction2;

  Set_ExpectedOutput  ( "fraction1 + fraction2", Helper::ToString( out_expected.m_num ) + "/" + Helper::ToString( out_expected.m_denom ) );
  Set_ActualOutput    ( "fraction1 + fraction2", Helper::ToString( out_actual.m_num ) + "/" + Helper::ToString( out_actual.m_denom ) );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual.m_num   != out_expected.m_num )   { TestFail(); }
  else if ( out_actual.m_denom != out_expected.m_denom ) { TestFail(); }
  else                                                   { TestPass(); }
  /* Test End   */ FinishTest(); }

  /* Test Begin */ { StartTest( "Check that 1/2 + 1/4 is added correctly" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 1; fraction1.m_denom = 2;
  fraction2.m_num = 1; fraction2.m_denom = 4;

  Fraction out_expected; out_expected.m_num = 6; out_expected.m_denom = 8;
  Fraction out_actual = fraction1 + fraction2;

  Set_ExpectedOutput  ( "fraction1 + fraction2", Helper::ToString( out_expected.m_num ) + "/" + Helper::ToString( out_expected.m_denom ) );
  Set_ActualOutput    ( "fraction1 + fraction2", Helper::ToString( out_actual.m_num ) + "/" + Helper::ToString( out_actual.m_denom ) );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual.m_num   != out_expected.m_num )   { TestFail(); }
  else if ( out_actual.m_denom != out_expected.m_denom ) { TestFail(); }
  else                                                   { TestPass(); }
  /* Test End   */ FinishTest(); }

  FinishTestSet();
  return TestResult();
}

int Tester::Test_SubtractionOperator()
{
  string fnName = "SubtractionOperator";
  StartTestSet( fnName, { fnName } );

  /* Test Begin */ { StartTest( "Check that 3/4 - 1/4 is subtracted correctly" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 3; fraction1.m_denom = 4;
  fraction2.m_num = 1; fraction2.m_denom = 4;

  Fraction out_expected; out_expected.m_num = 2; out_expected.m_denom = 4;
  Fraction out_actual = fraction1 - fraction2;

  Set_ExpectedOutput  ( "fraction1 - fraction2", Helper::ToString( out_expected.m_num ) + "/" + Helper::ToString( out_expected.m_denom ) );
  Set_ActualOutput    ( "fraction1 - fraction2", Helper::ToString( out_actual.m_num ) + "/" + Helper::ToString( out_actual.m_denom ) );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual.m_num   != out_expected.m_num )   { TestFail(); }
  else if ( out_actual.m_denom != out_expected.m_denom ) { TestFail(); }
  else                                                   { TestPass(); }
  /* Test End   */ FinishTest(); }

  /* Test Begin */ { StartTest( "Check that 1/2 - 1/4 is subtracted correctly" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 1; fraction1.m_denom = 2;
  fraction2.m_num = 1; fraction2.m_denom = 4;

  Fraction out_expected; out_expected.m_num = 2; out_expected.m_denom = 8;
  Fraction out_actual = fraction1 - fraction2;

  Set_ExpectedOutput  ( "fraction1 - fraction2", Helper::ToString( out_expected.m_num ) + "/" + Helper::ToString( out_expected.m_denom ) );
  Set_ActualOutput    ( "fraction1 - fraction2", Helper::ToString( out_actual.m_num ) + "/" + Helper::ToString( out_actual.m_denom ) );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual.m_num   != out_expected.m_num )   { TestFail(); }
  else if ( out_actual.m_denom != out_expected.m_denom ) { TestFail(); }
  else                                                   { TestPass(); }
  /* Test End   */ FinishTest(); }

  FinishTestSet();
  return TestResult();
}

int Tester::Test_MultiplicationOperator()
{
  string fnName = "MultiplicationOperator";
  StartTestSet( fnName, { fnName } );

  /* Test Begin */ { StartTest( "Check that 3/4 * 1/4 is multiplied correctly" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 3; fraction1.m_denom = 4;
  fraction2.m_num = 1; fraction2.m_denom = 4;

  Fraction out_expected; out_expected.m_num = 3; out_expected.m_denom = 16;
  Fraction out_actual = fraction1 * fraction2;

  Set_ExpectedOutput  ( "fraction1 * fraction2", Helper::ToString( out_expected.m_num ) + "/" + Helper::ToString( out_expected.m_denom ) );
  Set_ActualOutput    ( "fraction1 * fraction2", Helper::ToString( out_actual.m_num ) + "/" + Helper::ToString( out_actual.m_denom ) );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual.m_num   != out_expected.m_num )   { TestFail(); }
  else if ( out_actual.m_denom != out_expected.m_denom ) { TestFail(); }
  else                                                   { TestPass(); }
  /* Test End   */ FinishTest(); }

  /* Test Begin */ { StartTest( "Check that 1/2 * 1/4 is multiplied correctly" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 1; fraction1.m_denom = 2;
  fraction2.m_num = 1; fraction2.m_denom = 4;

  Fraction out_expected; out_expected.m_num = 1; out_expected.m_denom = 8;
  Fraction out_actual = fraction1 * fraction2;

  Set_ExpectedOutput  ( "fraction1 * fraction2", Helper::ToString( out_expected.m_num ) + "/" + Helper::ToString( out_expected.m_denom ) );
  Set_ActualOutput    ( "fraction1 * fraction2", Helper::ToString( out_actual.m_num ) + "/" + Helper::ToString( out_actual.m_denom ) );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual.m_num   != out_expected.m_num )   { TestFail(); }
  else if ( out_actual.m_denom != out_expected.m_denom ) { TestFail(); }
  else                                                   { TestPass(); }
  /* Test End   */ FinishTest(); }

  FinishTestSet();
  return TestResult();
}

int Tester::Test_DivisionOperator()
{
  string fnName = "DivisionOperator";
  StartTestSet( fnName, { fnName } );

  /* Test Begin */ { StartTest( "Check that 3/4 / 1/4 is divided correctly" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 3; fraction1.m_denom = 4;
  fraction2.m_num = 1; fraction2.m_denom = 4;

  Fraction out_expected; out_expected.m_num = 12; out_expected.m_denom = 4;
  Fraction out_actual = fraction1 / fraction2;

  Set_ExpectedOutput  ( "fraction1 / fraction2", Helper::ToString( out_expected.m_num ) + "/" + Helper::ToString( out_expected.m_denom ) );
  Set_ActualOutput    ( "fraction1 / fraction2", Helper::ToString( out_actual.m_num ) + "/" + Helper::ToString( out_actual.m_denom ) );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual.m_num   != out_expected.m_num )   { TestFail(); }
  else if ( out_actual.m_denom != out_expected.m_denom ) { TestFail(); }
  else                                                   { TestPass(); }
  /* Test End   */ FinishTest(); }

  /* Test Begin */ { StartTest( "Check that 1/2 / 1/4 is divided correctly" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 1; fraction1.m_denom = 2;
  fraction2.m_num = 1; fraction2.m_denom = 4;

  Fraction out_expected; out_expected.m_num = 4; out_expected.m_denom = 2;
  Fraction out_actual = fraction1 / fraction2;

  Set_ExpectedOutput  ( "fraction1 + fraction2", Helper::ToString( out_expected.m_num ) + "/" + Helper::ToString( out_expected.m_denom ) );
  Set_ActualOutput    ( "fraction1 + fraction2", Helper::ToString( out_actual.m_num ) + "/" + Helper::ToString( out_actual.m_denom ) );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual.m_num   != out_expected.m_num )   { TestFail(); }
  else if ( out_actual.m_denom != out_expected.m_denom ) { TestFail(); }
  else                                                   { TestPass(); }
  /* Test End   */ FinishTest(); }

  FinishTestSet();
  return TestResult();
}

int Tester::Test_EqualOperator()
{
  string fnName = "EqualOperator";
  StartTestSet( fnName, { fnName } );

  /* Test Begin */ { StartTest( "Check that 1/2 and 2/4 are identified as equivalent" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 1; fraction1.m_denom = 2;
  fraction2.m_num = 2; fraction2.m_denom = 4;

  bool out_expected = true;
  bool out_actual = fraction1 == fraction2;

  Set_ExpectedOutput  ( "fraction1 == fraction2", (out_expected) ? "TRUE" : "FALSE" );
  Set_ActualOutput    ( "fraction1 == fraction2", (out_actual) ? "TRUE" : "FALSE" );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual != out_expected )   { TestFail(); }
  else                                     { TestPass(); }
  /* Test End   */ FinishTest(); }


  /* Test Begin */ { StartTest( "Check that 3/4 and 3/4 are identified as equivalent" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 3; fraction1.m_denom = 4;
  fraction2.m_num = 3; fraction2.m_denom = 4;

  bool out_expected = true;
  bool out_actual = fraction1 == fraction2;

  Set_ExpectedOutput  ( "fraction1 == fraction2", (out_expected) ? "TRUE" : "FALSE" );
  Set_ActualOutput    ( "fraction1 == fraction2", (out_actual) ? "TRUE" : "FALSE" );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual != out_expected )   { TestFail(); }
  else                                     { TestPass(); }
  /* Test End   */ FinishTest(); }

  /* Test Begin */ { StartTest( "Check that 1/2 and 3/4 are identified as NOT equivalent" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 1; fraction1.m_denom = 2;
  fraction2.m_num = 3; fraction2.m_denom = 4;

  bool out_expected = false;
  bool out_actual = fraction1 == fraction2;

  Set_ExpectedOutput  ( "fraction1 == fraction2", (out_expected) ? "TRUE" : "FALSE" );
  Set_ActualOutput    ( "fraction1 == fraction2", (out_actual) ? "TRUE" : "FALSE" );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual != out_expected )   { TestFail(); }
  else                                     { TestPass(); }
  /* Test End   */ FinishTest(); }

  FinishTestSet();
  return TestResult();
}

int Tester::Test_NotEqualOperator()
{
  string fnName = "NotEqualOperator";
  StartTestSet( fnName, { fnName } );

  /* Test Begin */ { StartTest( "Check that 1/2 and 2/4 are identified as equivalent" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 1; fraction1.m_denom = 2;
  fraction2.m_num = 2; fraction2.m_denom = 4;

  bool out_expected = false;
  bool out_actual = fraction1 != fraction2;

  Set_ExpectedOutput  ( "fraction1 != fraction2", (out_expected) ? "TRUE" : "FALSE" );
  Set_ActualOutput    ( "fraction1 != fraction2", (out_actual) ? "TRUE" : "FALSE" );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual != out_expected )   { TestFail(); }
  else                                     { TestPass(); }
  /* Test End   */ FinishTest(); }


  /* Test Begin */ { StartTest( "Check that 3/4 and 3/4 are identified as equivalent" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 3; fraction1.m_denom = 4;
  fraction2.m_num = 3; fraction2.m_denom = 4;

  bool out_expected = false;
  bool out_actual = fraction1 != fraction2;

  Set_ExpectedOutput  ( "fraction1 != fraction2", (out_expected) ? "TRUE" : "FALSE" );
  Set_ActualOutput    ( "fraction1 != fraction2", (out_actual) ? "TRUE" : "FALSE" );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual != out_expected )   { TestFail(); }
  else                                     { TestPass(); }
  /* Test End   */ FinishTest(); }

  /* Test Begin */ { StartTest( "Check that 1/2 and 3/4 are identified as NOT equivalent" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 1; fraction1.m_denom = 2;
  fraction2.m_num = 3; fraction2.m_denom = 4;

  bool out_expected = true;
  bool out_actual = fraction1 != fraction2;

  Set_ExpectedOutput  ( "fraction1 != fraction2", (out_expected) ? "TRUE" : "FALSE" );
  Set_ActualOutput    ( "fraction1 != fraction2", (out_actual) ? "TRUE" : "FALSE" );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual != out_expected )   { TestFail(); }
  else                                     { TestPass(); }
  /* Test End   */ FinishTest(); }

  FinishTestSet();
  return TestResult();
}

int Tester::Test_LessThanOrEqualToOperator()
{
  string fnName = "LessThanOrEqualToOperator";
  StartTestSet( fnName, { fnName } );

  /* Test Begin */ { StartTest( "Check that 1/4 is identified as less than or equal to 1/2" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 1; fraction1.m_denom = 4;
  fraction2.m_num = 1; fraction2.m_denom = 2;

  bool out_expected = true;
  bool out_actual = fraction1 <= fraction2;

  Set_ExpectedOutput  ( "fraction1 <= fraction2", (out_expected) ? "TRUE" : "FALSE" );
  Set_ActualOutput    ( "fraction1 <= fraction2", (out_actual) ? "TRUE" : "FALSE" );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual != out_expected )   { TestFail(); }
  else                                     { TestPass(); }
  /* Test End   */ FinishTest(); }

  /* Test Begin */ { StartTest( "Check that 3/4 is identified as NOT less than or equal to 1/2" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 3; fraction1.m_denom = 4;
  fraction2.m_num = 1; fraction2.m_denom = 2;

  bool out_expected = false;
  bool out_actual = fraction1 <= fraction2;

  Set_ExpectedOutput  ( "fraction1 <= fraction2", (out_expected) ? "TRUE" : "FALSE" );
  Set_ActualOutput    ( "fraction1 <= fraction2", (out_actual) ? "TRUE" : "FALSE" );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual != out_expected )   { TestFail(); }
  else                                     { TestPass(); }
  /* Test End   */ FinishTest(); }

  /* Test Begin */ { StartTest( "Check that 2/4 is identified as less than or equal to 1/2" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 2; fraction1.m_denom = 4;
  fraction2.m_num = 1; fraction2.m_denom = 2;

  bool out_expected = true;
  bool out_actual = fraction1 <= fraction2;

  Set_ExpectedOutput  ( "fraction1 <= fraction2", (out_expected) ? "TRUE" : "FALSE" );
  Set_ActualOutput    ( "fraction1 <= fraction2", (out_actual) ? "TRUE" : "FALSE" );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual != out_expected )   { TestFail(); }
  else                                     { TestPass(); }
  /* Test End   */ FinishTest(); }

  FinishTestSet();
  return TestResult();
}

int Tester::Test_GreaterThanOrEqualToOperator()
{
  string fnName = "GreaterThanOrEqualToOperator";
  StartTestSet( fnName, { fnName } );

  /* Test Begin */ { StartTest( "Check that 1/4 is identified as NOT greater than or equal to 1/2" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 1; fraction1.m_denom = 4;
  fraction2.m_num = 1; fraction2.m_denom = 2;

  bool out_expected = false;
  bool out_actual = fraction1 >= fraction2;

  Set_ExpectedOutput  ( "fraction1 >= fraction2", (out_expected) ? "TRUE" : "FALSE" );
  Set_ActualOutput    ( "fraction1 >= fraction2", (out_actual) ? "TRUE" : "FALSE" );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual != out_expected )   { TestFail(); }
  else                                     { TestPass(); }
  /* Test End   */ FinishTest(); }

  /* Test Begin */ { StartTest( "Check that 3/4 is identified as greater than or equal to 1/2" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 3; fraction1.m_denom = 4;
  fraction2.m_num = 1; fraction2.m_denom = 2;

  bool out_expected = true;
  bool out_actual = fraction1 >= fraction2;

  Set_ExpectedOutput  ( "fraction1 >= fraction2", (out_expected) ? "TRUE" : "FALSE" );
  Set_ActualOutput    ( "fraction1 >= fraction2", (out_actual) ? "TRUE" : "FALSE" );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual != out_expected )   { TestFail(); }
  else                                     { TestPass(); }
  /* Test End   */ FinishTest(); }

  /* Test Begin */ { StartTest( "Check that 2/4 is identified as greater than or equal to 1/2" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 2; fraction1.m_denom = 4;
  fraction2.m_num = 1; fraction2.m_denom = 2;

  bool out_expected = true;
  bool out_actual = fraction1 >= fraction2;

  Set_ExpectedOutput  ( "fraction1 >= fraction2", (out_expected) ? "TRUE" : "FALSE" );
  Set_ActualOutput    ( "fraction1 >= fraction2", (out_actual) ? "TRUE" : "FALSE" );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual != out_expected )   { TestFail(); }
  else                                     { TestPass(); }
  /* Test End   */ FinishTest(); }

  FinishTestSet();
  return TestResult();
}

int Tester::Test_LessThanOperator()
{
  string fnName = "LessThanOperator";
  StartTestSet( fnName, { fnName } );

  /* Test Begin */ { StartTest( "Check that 1/4 is identified as less than 1/2" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 1; fraction1.m_denom = 4;
  fraction2.m_num = 1; fraction2.m_denom = 2;

  bool out_expected = true;
  bool out_actual = fraction1 < fraction2;

  Set_ExpectedOutput  ( "fraction1 < fraction2", (out_expected) ? "TRUE" : "FALSE" );
  Set_ActualOutput    ( "fraction1 < fraction2", (out_actual) ? "TRUE" : "FALSE" );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual != out_expected )   { TestFail(); }
  else                                     { TestPass(); }
  /* Test End   */ FinishTest(); }

  /* Test Begin */ { StartTest( "Check that 3/4 is identified as NOT less than 1/2" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 3; fraction1.m_denom = 4;
  fraction2.m_num = 1; fraction2.m_denom = 2;

  bool out_expected = false;
  bool out_actual = fraction1 < fraction2;

  Set_ExpectedOutput  ( "fraction1 < fraction2", (out_expected) ? "TRUE" : "FALSE" );
  Set_ActualOutput    ( "fraction1 < fraction2", (out_actual) ? "TRUE" : "FALSE" );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual != out_expected )   { TestFail(); }
  else                                     { TestPass(); }
  /* Test End   */ FinishTest(); }

  /* Test Begin */ { StartTest( "Check that 2/4 is identified as NOT less than 1/2" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 2; fraction1.m_denom = 4;
  fraction2.m_num = 1; fraction2.m_denom = 2;

  bool out_expected = false;
  bool out_actual = fraction1 < fraction2;

  Set_ExpectedOutput  ( "fraction1 < fraction2", (out_expected) ? "TRUE" : "FALSE" );
  Set_ActualOutput    ( "fraction1 < fraction2", (out_actual) ? "TRUE" : "FALSE" );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual != out_expected )   { TestFail(); }
  else                                     { TestPass(); }
  /* Test End   */ FinishTest(); }

  FinishTestSet();
  return TestResult();
}

int Tester::Test_GreaterOperator()
{
  string fnName = "GreaterOperator";
  StartTestSet( fnName, { fnName } );

  /* Test Begin */ { StartTest( "Check that 1/4 is identified as NOT greater than 1/2" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 1; fraction1.m_denom = 4;
  fraction2.m_num = 1; fraction2.m_denom = 2;

  bool out_expected = false;
  bool out_actual = fraction1 > fraction2;

  Set_ExpectedOutput  ( "fraction1 > fraction2", (out_expected) ? "TRUE" : "FALSE" );
  Set_ActualOutput    ( "fraction1 > fraction2", (out_actual) ? "TRUE" : "FALSE" );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual != out_expected )   { TestFail(); }
  else                                     { TestPass(); }
  /* Test End   */ FinishTest(); }

  /* Test Begin */ { StartTest( "Check that 3/4 is identified as greater than 1/2" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 3; fraction1.m_denom = 4;
  fraction2.m_num = 1; fraction2.m_denom = 2;

  bool out_expected = true;
  bool out_actual = fraction1 > fraction2;

  Set_ExpectedOutput  ( "fraction1 > fraction2", (out_expected) ? "TRUE" : "FALSE" );
  Set_ActualOutput    ( "fraction1 > fraction2", (out_actual) ? "TRUE" : "FALSE" );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual != out_expected )   { TestFail(); }
  else                                     { TestPass(); }
  /* Test End   */ FinishTest(); }

  /* Test Begin */ { StartTest( "Check that 2/4 is identified as NOT greater than 1/2" );
  Fraction fraction1, fraction2;
  fraction1.m_num = 2; fraction1.m_denom = 4;
  fraction2.m_num = 1; fraction2.m_denom = 2;

  bool out_expected = false;
  bool out_actual = fraction1 > fraction2;

  Set_ExpectedOutput  ( "fraction1 > fraction2", (out_expected) ? "TRUE" : "FALSE" );
  Set_ActualOutput    ( "fraction1 > fraction2", (out_actual) ? "TRUE" : "FALSE" );
  Set_Comments( "Fraction 1:   " + Helper::ToString( fraction1.m_num ) + "/" + Helper::ToString( fraction1.m_denom ) );
  Set_Comments( "Fraction 2:   " + Helper::ToString( fraction2.m_num ) + "/" + Helper::ToString( fraction2.m_denom ) );
  if      ( out_actual != out_expected )   { TestFail(); }
  else                                     { TestPass(); }
  /* Test End   */ FinishTest(); }

  FinishTestSet();
  return TestResult();
}

int Tester::Test_OutputStreamOperator()
{
  string fnName = "OutputStreamOperator";
  StartTestSet( fnName, { fnName } );

  /* Test Begin */ { StartTest( "Check that 2/4 is streamed out properly" );
  Fraction fraction; fraction.m_num = 2; fraction.m_denom = 4;

  string out_expected = "2/4";
  ostringstream out_actual;
  out_actual << fraction;

  Set_ExpectedOutput  ( "outstream result", out_expected );
  Set_ActualOutput    ( "outstream result", out_actual.str() );
  Set_Comments( "Fraction:   " + Helper::ToString( fraction.m_num ) + "/" + Helper::ToString( fraction.m_denom ) );
  if      ( out_actual.str() != out_expected )   { TestFail(); }
  else                                     { TestPass(); }
  /* Test End   */ FinishTest(); }

  /* Test Begin */ { StartTest( "Check that 1/5 is streamed out properly" );
  Fraction fraction; fraction.m_num = 1; fraction.m_denom = 5;

  string out_expected = "1/5";
  ostringstream out_actual;
  out_actual << fraction;

  Set_ExpectedOutput  ( "outstream result", out_expected );
  Set_ActualOutput    ( "outstream result", out_actual.str() );
  Set_Comments( "Fraction:   " + Helper::ToString( fraction.m_num ) + "/" + Helper::ToString( fraction.m_denom ) );
  if      ( out_actual.str() != out_expected )   { TestFail(); }
  else                                     { TestPass(); }
  /* Test End   */ FinishTest(); }

  FinishTestSet();
  return TestResult();
}

int Tester::Test_InputStreamOperator()
{
  string fnName = "InputStreamOperator";
  StartTestSet( fnName, { fnName } );

  /* Test Begin */ { StartTest( "Check that 2/4 can be streamed into a fraction properly" );
  Fraction out_expected; out_expected.m_num = 2; out_expected.m_denom = 4;
  Fraction out_actual;

  istringstream instream("2 4");
  instream >> out_actual;

  Set_ExpectedOutput  ( "streamed in fraction", Helper::ToString( out_expected.m_num ) + "/" + Helper::ToString( out_expected.m_denom ) );
  Set_ActualOutput    ( "streamed in fraction", Helper::ToString( out_actual.m_num ) + "/" + Helper::ToString( out_actual.m_denom ) );
  Set_Comments( "Input stream: \"2 4\"" );
  if      ( out_actual.m_num   != out_expected.m_num )   { TestFail(); }
  else if ( out_actual.m_denom != out_expected.m_denom ) { TestFail(); }
  else                                                   { TestPass(); }
  /* Test End   */ FinishTest(); }

  /* Test Begin */ { StartTest( "Check that 3/5 can be streamed into a fraction properly" );
  Fraction out_expected; out_expected.m_num = 3; out_expected.m_denom = 5;
  Fraction out_actual;

  istringstream instream("3 5");
  instream >> out_actual;

  Set_ExpectedOutput  ( "streamed in fraction", Helper::ToString( out_expected.m_num ) + "/" + Helper::ToString( out_expected.m_denom ) );
  Set_ActualOutput    ( "streamed in fraction", Helper::ToString( out_actual.m_num ) + "/" + Helper::ToString( out_actual.m_denom ) );
  Set_Comments( "Input stream: \"3 5\"" );
  if      ( out_actual.m_num   != out_expected.m_num )   { TestFail(); }
  else if ( out_actual.m_denom != out_expected.m_denom ) { TestFail(); }
  else                                                   { TestPass(); }
  /* Test End   */ FinishTest(); }

  FinishTestSet();
  return TestResult();
}

