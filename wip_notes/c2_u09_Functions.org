# -*- mode: org -*-

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

#+TITLE: Q&A: Functions
#+AUTHOR: Rachel Wil Sha Singh

*What are the 3 parts of a function header?*
- Return type, function name, and parameter list
- =RETURN_TYPE FUNCTION_NAME( PARAM1TYPE PARAM1NAME, PARAM2TYPE PARAM2NAME )=

*What is a return type? Which return type is used if the function returns no data?*
- The return type is used to specify /what kind of data/ the function will be returning.
- If the function does not return any data, we use a =void= return type.

*What is an input parameter?*
- These are variables that are part of the function header and they are places where we store incoming data,
  passed in during the function call.

*What is a function name?*
- The function name is a way we can refer back to the function to call it at any time.

*How do you declare a function?*
- The function header, ending with a semicolon.
#+BEGIN_SRC cpp :class cpp
  void DisplayMenu();                          // NO input    NO output
  void Format( string fname, string lname );   // YES input   NO output
  float GetTax();                              // NO input    YES output
  float Sum( float a, float b );               // YES input   YES output
#+END_SRC

*How do you define a function?*
- The function header, followed by a codeblock, which begins at ={= and ends at =}=.
#+BEGIN_SRC cpp :class cpp
  void DisplayMenu()                           // NO input    NO output
  {
    cout << "0. QUIT" << endl;
    cout << "1. Withdraw" << endl;
    cout << "2. Deposit" << endl;
    cout << "3. View balance" << endl;
  }

  void Format( string fname, string lname )    // YES input   NO output
  {
    cout << lname << ", " << fname << endl;
  }

  float GetTax()                               // NO input    YES output
  {
    return 0.091;
  }

  float Sum( float a, float b )                // YES input   YES output
  {
    return a + b;
  }
#+END_SRC

*How do you call a function?*
- Use the function's name and pass in any required input arguments.

#+BEGIN_SRC cpp :class cpp
  int main()
  {
    DisplayMenu();                             // NO input    NO output
    Format( "RW", "Singh" );                   // YES input   NO output
    float tax = GetTax();                      // NO input    YES output

    float num1 = 5, num2 = 10;
    float result = Sum( num1, num2 );          // YES input   YES output
  }
#+END_SRC

*What is an argument? How is it different from a parameter?*
- The argument is the *input data* being passed into the function when called.
- This data can be a literal, or a variable's value, or the result from another function call.
- =result = Sum( 2, 5 );= Call function, passing in 2 and 3 as arguments, and storing the result in the result variable.
- =result = Sum( num1, num2 );= Call function, passing in num1 and num2 as arguments, storing the result in the result variable.

*What is scope? How do you tell what the scope of a variable is?*
- The scope is where a variable "lives". It includes the *codeblock* where it is declared within.
- A variable declared within one function does not exist in another function.
- A variable is "out of scope" if it does not exist in the current context.

*Are variables pass-by-value or pass-by-reference as a default in C++?*
- By default, primitive variables (int, float, char) and object variables (string, custom types) are passed *by value* by default.

*How do you tell if a parameter is pass-by-value or pass-by-reference?*
- If a parameter has =&= after the data type, then it is *pass-by-reference*.
- If a parameter has =*= after the data type, then it is a *pointer*.
- If a variable is a *traditional array*, then it is technically a *pointer*.
- If a parameter has no special symtols after the data type, then it is *pass-by-value*.

#+BEGIN_SRC cpp :class cpp
  void WorkWithCopies( int a, string b );       // pass-by-value
  void WorkWithReferences( int& a, string& b ); // pass-by-reference
  void WorkWithPointers( int* a, string* b );   // pointers
#+END_SRC

*What does pass-by-value mean?*
- The /input argument/ is copied into the parameter variable; the parameter is only a copy and does not provide direct access to the original.

#+BEGIN_SRC cpp :class cpp
  void WorkWithCopies( int a, string b );       // pass-by-value

  int main()
  {
    int my_number = 50;
    string my_string = "Hello";

    // my_number is an argument, and its value is copied to WorkWithCopies' "a" variable.
    // my_string is an argument, and its value is copied to WorkWithCopies' "b" variable.
    WorkWithCopies( my_number, my_string );

    return 0;
  }
#+END_SRC

*What does pass-by-reference mean?*
- The /input argument/ variable is given to the function call and the parameter is a reference to that variable. Changes made to it within the function are then reflected back out in the original argument variable.

#+BEGIN_SRC cpp :class cpp
  void WorkWithReferences( int& a, string& b ); // pass-by-reference

  int main()
  {
    int my_number = 50;
    string my_string = "Hello";

    // my_number is an argument, this variable is directly accessed via the WorkWithReferences' "a" variable.
    // my_string is an argument, this variable is directly accessed via the WorkWithReferences' "b" variable.
    WorkWithReferences( my_number, my_string );

    return 0;
  }
#+END_SRC

*What does passing a pointer look like?*

#+BEGIN_SRC cpp :class cpp
      void WorkWithPointers( int* a, string* b );   // pointers

      int main()
      {
        int my_number = 50;
        string my_string = "Hello";

        // my_number is an argument, its address is passed to the WorkWithPointers function,
        // where the address is stored in the "a" variable.
        // my_string is an argument, its address is passed to the WorkWithPointers function,
        // where the address is stored in the "b" variable.
        WorkWithPointers( &my_number, &my_string );

        return 0;
      }
#+END_SRC

-----
