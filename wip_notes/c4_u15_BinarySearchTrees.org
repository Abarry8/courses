# -*- mode: org -*-

Write down the function implementation for each of the following. Add diagrams as well
to help you remember these better.

- =void BinarySearchTree::Push( const TK& newKey, const TD& newData )=
- =void BinarySearchTree::RecursivePush( const TK& newKey, const TD& newData, BinarySearchTreeNode<TK, TD>* ptrCurrent )=
- =bool BinarySearchTree::Contains( const TK& key )=
- =bool BinarySearchTree::RecursiveContains( const TK& key, BinarySearchTreeNode<TK, TD>* ptrCurrent )=
- =BinarySearchTreeNode<TK, TD>* BinarySearchTree::RecursiveFindNode( const TK& key, BinarySearchTreeNode<TK, TD>* ptrCurrent )=
- =int BinarySearchTree::RecursiveGetHeight( BinarySearchTreeNode<TK, TD>* ptrCurrent )=
