#ifndef _PROGRAM
#define _PROGRAM

#include "Product.h"

#include "../DataStructure/SmartDynamicArray/SmartDynamicArray.h"
//#include <vector> // temporary
#include <string>

class Program
{
public:
	void Run();
	void SetDataPath(std::string path);

private:
	void Setup();
	void Cleanup();

	void Menu_Main();
	void Menu_AddProduct();
	void Menu_EditProduct();
	void Menu_DeleteProduct();

	void DisplayProducts(const std::vector<Product>& productList); // TODO: Replace with void DisplayProducts(const DataStructure::SmartDynamicArray<Product>& productList);

	std::vector<Product> m_products; // TODO: Replace with DataStructure::SmartDynamicArray<Product>
	std::string m_dataPath;
};

#endif
