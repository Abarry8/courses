#include "Utilities/Logger.h"
#include "Program/Program.h"
#include "DataStructure/SmartDynamicArray/SmartDynamicArrayTester.h"

#include <iostream>
using namespace std;

int main()
{
	// Initialize the debug logger
	Utility::Logger::Setup();

	cout << "STARTUP" << endl;
	cout << "1. Run unit tests" << endl;
	cout << "2. Run program" << endl;
	cout << endl << ">> ";
	int choice;
	cin >> choice;

	switch (choice)
	{
		case 1: {
			DataStructure::SmartDynamicArrayTester tester;
			tester.Start();
			break;
		}
		case 2: {
			Program program;
			program.SetDataPath("../Data/products.txt");
			program.Run();
			break;
		}
	}

	// Clean up the debug logger
	Utility::Logger::Cleanup();

	return 0;
}







