#ifndef _QUEUE_TESTER_HPP
#define _QUEUE_TESTER_HPP

// C++ Library includes
#include <iostream>
#include <string>

// Project includes
#include "ArrayQueue.h"
#include "LinkedQueue.h"
#include "../../CuTest/TesterBase.h"
#include "../../Utilities/Menu.h"
#include "../../Utilities/StringUtil.h"
#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/InvalidIndexException.h"
#include "../../Exceptions/NullptrException.h"

namespace DataStructure
{

//! TESTER for the Queue
class QueueTester : public cuTest::TesterBase
{
public:
    QueueTester()
        : TesterBase( "test_result_queue.html" )
    {
        AddTest(cuTest::TestListItem("Test_Push",                 std::bind(&QueueTester::Test_Push, this)));
        AddTest(cuTest::TestListItem("Test_Pop",                  std::bind(&QueueTester::Test_Pop, this)));
        AddTest(cuTest::TestListItem("Test_Get",                  std::bind(&QueueTester::Test_Front, this)));
        AddTest(cuTest::TestListItem("Test_Size",                 std::bind(&QueueTester::Test_Size, this)));
        AddTest(cuTest::TestListItem("Test_IsEmpty",              std::bind(&QueueTester::Test_IsEmpty, this)));
    }

    virtual ~QueueTester() { }

private:
    int Test_Push();
    int Test_Pop();
    int Test_Front();
    int Test_Size();
    int Test_IsEmpty();
};

int QueueTester::Test_Push()
{
    std::string functionName = "Queue Push";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", functionName, 3 );
    StartTestSet( "Queue Test_" + functionName, { functionName } );

    StartTest( "0a. Check if function " + functionName + " is implemented (Array Version)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try  { ArrayQueue<int> arr; arr.Push( 123 ); }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "0b. Check if function " + functionName + " is implemented (Linked Version)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try  { LinkedQueue<int> arr; arr.Push( 123 ); }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Push one item (Array and Linked)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        Set_Comments( "Pushing 12 to each queue" );

        int input = 12;
        int expectedOutput = 12;

        ArrayQueue<int> arrayQueue;
        LinkedQueue<int> linkedQueue;

        arrayQueue.Push( input );
        linkedQueue.Push( input );

        /*
        MAKE SURE THIS IS ADDED TO SMART DYNAMIC ARRAY AND LINKED LIST:
        friend class QueueTester;
        friend class StackTester;
        */

        int actualOutputArray = arrayQueue.m_vector.m_array[0];
        int actualOutputLinked = linkedQueue.m_list.m_ptrFirst->m_data;

        if      ( !Set_Outputs( "Array Queue item 0", expectedOutput, actualOutputArray ) )                 { TestFail(); }
        else if ( !Set_Outputs( "Linked Queue item 0", expectedOutput, actualOutputLinked ) )               { TestFail(); }
        else if ( !Set_Outputs( "arrayQueue.m_vector.m_itemCount", 1, arrayQueue.m_vector.m_itemCount ) )   { TestFail(); }
        else if ( !Set_Outputs( "linkedQueue.m_list.m_itemCount", 1, linkedQueue.m_list.m_itemCount ) )     { TestFail(); }
        else                                                                                                { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. Push two items (Array and Linked)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        Set_Comments( "Pushing 12, 20 to each queue" );

        int input1 = 12;
        int input2 = 20;

        ArrayQueue<int> arrayQueue;
        LinkedQueue<int> linkedQueue;

        arrayQueue.Push( input1 );
        arrayQueue.Push( input2 );
        linkedQueue.Push( input1 );
        linkedQueue.Push( input2 );

        int arrayFirstItem = arrayQueue.m_vector.m_array[0];
        int arraySecondItem = arrayQueue.m_vector.m_array[1];
        int linkedFirstItem = linkedQueue.m_list.m_ptrFirst->m_data;
        int linkedSecondItem = linkedQueue.m_list.m_ptrFirst->m_ptrNext->m_data;

        if      ( !Set_Outputs( "Array Queue item 0",  input1, arrayFirstItem ) )                           { TestFail(); }
        else if ( !Set_Outputs( "Array Queue item 1",  input2, arraySecondItem ) )                          { TestFail(); }
        else if ( !Set_Outputs( "Linked Queue item 0", input1, linkedFirstItem ) )                          { TestFail(); }
        else if ( !Set_Outputs( "Linked Queue item 1", input2, linkedSecondItem ) )                         { TestFail(); }
        else if ( !Set_Outputs( "arrayQueue.m_vector.m_itemCount", 2, arrayQueue.m_vector.m_itemCount ) )   { TestFail(); }
        else if ( !Set_Outputs( "linkedQueue.m_list.m_itemCount", 2, linkedQueue.m_list.m_itemCount ) )     { TestFail(); }
        else                                                                                                { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int QueueTester::Test_Pop()
{
    std::string functionName = "Queue Pop";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", functionName, 3 );
    StartTestSet( "Queue Test_" + functionName, { functionName } );

    StartTest( "0a. Check if function " + functionName + " is implemented (Array Version)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try  { ArrayQueue<int> arr; arr.Pop(); }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "0b. Check if function " + functionName + " is implemented (Linked Version)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try  { LinkedQueue<int> arr; arr.Pop(); }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Add item to queue and call Pop" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        Set_Comments( "Pushing 50 to the queue" );

        int input = 50;
        bool exceptionThrown = false;

        ArrayQueue<int> arrayVersion;
        LinkedQueue<int> linkedVersion;

        arrayVersion.m_vector.m_array = new int[3];
        arrayVersion.m_vector.m_arraySize = 3;
        arrayVersion.m_vector.m_array[0] = input;
        arrayVersion.m_vector.m_itemCount = 1;

        DoublyLinkedListNode<int>* node = new DoublyLinkedListNode<int>;
        node->m_data = input;
        linkedVersion.m_list.m_ptrFirst = node;
        linkedVersion.m_list.m_ptrLast = node;
        linkedVersion.m_list.m_itemCount = 1;

        try
        {
          arrayVersion.Pop();
          linkedVersion.Pop();
        }
        catch( ... )
        {
          exceptionThrown = true;
        }

        if      ( !Set_Outputs( "Exception thrown?",  false, exceptionThrown ) )                            { TestFail(); }
        else if ( !Set_Outputs( "arrayQueue.m_vector.m_itemCount", 0, arrayVersion.m_vector.m_itemCount ) ) { TestFail(); }
        else if ( !Set_Outputs( "linkedQueue.m_list.m_itemCount", 0, linkedVersion.m_list.m_itemCount ) )   { TestFail(); }
        else                                                                                                { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. Add two items to queue and call Pop, check Front item" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        Set_Comments( "Pushing 50, 100 to the queue" );

        int input1 = 50;
        int input2 = 100;
        bool exceptionThrown = false;

        ArrayQueue<int> arrayVersion;
        LinkedQueue<int> linkedVersion;

        arrayVersion.m_vector.m_array = new int[3];
        arrayVersion.m_vector.m_arraySize = 3;
        arrayVersion.m_vector.m_array[0] = input1;
        arrayVersion.m_vector.m_array[1] = input2;
        arrayVersion.m_vector.m_itemCount = 2;

        DoublyLinkedListNode<int>* nodeA = new DoublyLinkedListNode<int>;
        DoublyLinkedListNode<int>* nodeB = new DoublyLinkedListNode<int>;
        nodeA->m_data = input1;
        nodeA->m_ptrNext = nodeB;
        nodeB->m_data = input2;
        nodeB->m_ptrPrev = nodeA;
        linkedVersion.m_list.m_ptrFirst = nodeA;
        linkedVersion.m_list.m_ptrLast = nodeB;
        linkedVersion.m_list.m_itemCount = 2;

        int expectedFrontItem = input2;

        try
        {
          arrayVersion.Pop();
          linkedVersion.Pop();
        }
        catch( ... )
        {
          exceptionThrown = true;
        }

        int arrayFirstItem = arrayVersion.m_vector.m_array[0];
        int linkedFirstItem = linkedVersion.m_list.m_ptrFirst->m_data;

        if      ( !Set_Outputs( "Exception thrown?",  false, exceptionThrown ) )                            { TestFail(); }
        else if ( !Set_Outputs( "Array Queue front item",  expectedFrontItem, arrayFirstItem ) )            { TestFail(); }
        else if ( !Set_Outputs( "Linked Queue front item", expectedFrontItem, linkedFirstItem ) )           { TestFail(); }
        else if ( !Set_Outputs( "arrayQueue.m_vector.m_itemCount", 1, arrayVersion.m_vector.m_itemCount ) ) { TestFail(); }
        else if ( !Set_Outputs( "linkedQueue.m_list.m_itemCount", 1, linkedVersion.m_list.m_itemCount ) )   { TestFail(); }
        else                                                                                                { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int QueueTester::Test_Front()
{
    std::string functionName = "Queue Front";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", functionName, 3 );
    StartTestSet( "Queue Test_" + functionName, { functionName } );

    StartTest( "0a. Check if function " + functionName + " is implemented (Array Version)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try  { ArrayQueue<int> arr; int a = arr.Front(); }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "0b. Check if function " + functionName + " is implemented (Linked Version)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try  { LinkedQueue<int> arr; int a = arr.Front(); }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Add two items to queue and call Front, verify correct Front item" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        Set_Comments( "Pushing 50, 100 to the queue" );

        int input1 = 50;
        int input2 = 100;
        bool exceptionThrown = false;

        ArrayQueue<int> arrayVersion;
        LinkedQueue<int> linkedVersion;

        arrayVersion.m_vector.m_array = new int[3];
        arrayVersion.m_vector.m_arraySize = 3;
        arrayVersion.m_vector.m_array[0] = input1;
        arrayVersion.m_vector.m_array[1] = input2;
        arrayVersion.m_vector.m_itemCount = 2;

        DoublyLinkedListNode<int>* nodeA = new DoublyLinkedListNode<int>;
        DoublyLinkedListNode<int>* nodeB = new DoublyLinkedListNode<int>;
        nodeA->m_data = input1;
        nodeA->m_ptrNext = nodeB;
        nodeB->m_data = input2;
        nodeB->m_ptrPrev = nodeA;
        linkedVersion.m_list.m_ptrFirst = nodeA;
        linkedVersion.m_list.m_ptrLast = nodeB;
        linkedVersion.m_list.m_itemCount = 2;

        int expectedFrontItem = input1;
        int arrayFrontItem = arrayVersion.Front();
        int linkedFrontItem = linkedVersion.Front();

        if      ( !Set_Outputs( "Array Queue front item",  expectedFrontItem, arrayFrontItem ) )            { TestFail(); }
        else if ( !Set_Outputs( "Linked Queue front item", expectedFrontItem, linkedFrontItem ) )           { TestFail(); }
        else                                                                                                { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int QueueTester::Test_Size()
{
    std::string functionName = "Queue Size";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", functionName, 3 );
    StartTestSet( "Queue Test_" + functionName, { "Size" } );

    StartTest( "0a. Check if function " + functionName + " is implemented (Array Version)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try  { ArrayQueue<int> arr; int s = arr.Size(); }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "0b. Check if function " + functionName + " is implemented (Linked Version)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try  { LinkedQueue<int> arr; int s = arr.Size(); }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Check size on empty queue" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        int expectedOutput = 0;

        ArrayQueue<int> arrayQueue;
        LinkedQueue<int> linkedQueue;

        if      ( !Set_Outputs( "arrayQueue.Size()",  expectedOutput, arrayQueue.Size() ) )   { TestFail(); }
        else if ( !Set_Outputs( "linkedQueue.Size()", expectedOutput, linkedQueue.Size() ) )  { TestFail(); }
        else                                                                                  { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. Check size on queue with 1 item in it" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        int expectedOutput = 1;

        ArrayQueue<int> arrayVersion;
        LinkedQueue<int> linkedVersion;

        arrayVersion.m_vector.m_itemCount = 1;
        linkedVersion.m_list.m_itemCount = 1;

        if      ( !Set_Outputs( "arrayQueue.Size()",  expectedOutput, arrayVersion.Size() ) )   { TestFail(); }
        else if ( !Set_Outputs( "linkedQueue.Size()", expectedOutput, linkedVersion.Size() ) )  { TestFail(); }
        else                                                                                  { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "3. Check size on queue with 3 items in it" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        int expectedOutput = 3;

        ArrayQueue<int> arrayVersion;
        LinkedQueue<int> linkedVersion;

        arrayVersion.m_vector.m_itemCount = 3;
        linkedVersion.m_list.m_itemCount = 3;

        if      ( !Set_Outputs( "arrayQueue.Size()",  expectedOutput, arrayVersion.Size() ) )   { TestFail(); }
        else if ( !Set_Outputs( "linkedQueue.Size()", expectedOutput, linkedVersion.Size() ) )  { TestFail(); }
        else                                                                                  { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int QueueTester::Test_IsEmpty()
{
    std::string functionName = "Queue IsEmpty";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", functionName, 3 );
    StartTestSet( "Queue Test_" + functionName, { functionName } );

    StartTest( "0a. Check if function " + functionName + " is implemented (Array Version)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try  { ArrayQueue<int> arr; bool a = arr.IsEmpty(); }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "0b. Check if function " + functionName + " is implemented (Linked Version)" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try  { LinkedQueue<int> arr; bool a = arr.IsEmpty(); }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Check size on IsEmpty with empty queue" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool expectedOutput = true;

        ArrayQueue<int> arrayVersion;
        LinkedQueue<int> linkedVersion;

        arrayVersion.m_vector.m_itemCount = 0;
        linkedVersion.m_list.m_itemCount = 0;

        if      ( !Set_Outputs( "arrayQueue.IsEmpty()",  expectedOutput, arrayVersion.IsEmpty() ) )   { TestFail(); }
        else if ( !Set_Outputs( "linkedQueue.IsEmpty()", expectedOutput, linkedVersion.IsEmpty() ) )  { TestFail(); }
        else                                                                                        { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. Check size on IsEmpty with non-empty queue" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool expectedOutput = false;

        ArrayQueue<int> arrayVersion;
        LinkedQueue<int> linkedVersion;

        arrayVersion.m_vector.m_itemCount = 1;
        linkedVersion.m_list.m_itemCount = 1;

        if      ( !Set_Outputs( "arrayQueue.IsEmpty()",  expectedOutput, arrayVersion.IsEmpty() ) )   { TestFail(); }
        else if ( !Set_Outputs( "linkedQueue.IsEmpty()", expectedOutput, linkedVersion.IsEmpty() ) )  { TestFail(); }
        else                                                                                          { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

} // End of namespace

#endif
