#ifndef _SMART_DYNAMIC_ARRAY_TESTER_HPP
#define _SMART_DYNAMIC_ARRAY_TESTER_HPP

// C++ Library includes
#include <iostream>
#include <string>

// Project includes
#include "SmartDynamicArray.h"
#include "../../CuTest/TesterBase.h"
#include "../../Utilities/Menu.h"
#include "../../Utilities/StringUtil.h"
#include "../../Utilities/Logger.h"
#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/StructureFullException.h"
#include "../../Exceptions/InvalidIndexException.h"
#include "../../Exceptions/NullptrException.h"

namespace DataStructure
{

//! TESTER for the SmartDynamicArray
class SmartDynamicArrayTester : public cuTest::TesterBase
{
public:
    SmartDynamicArrayTester()
        : TesterBase( "test_result_smart_dynamic_array.html" )
    {
        AddTest(cuTest::TestListItem("Constructor()",       std::bind(&SmartDynamicArrayTester::Test_Constructor, this)));

        AddTest(cuTest::TestListItem("ShiftLeft()",         std::bind(&SmartDynamicArrayTester::Test_ShiftLeft, this)));
        AddTest(cuTest::TestListItem("ShiftRight()",        std::bind(&SmartDynamicArrayTester::Test_ShiftRight, this)));

        AddTest(cuTest::TestListItem("RemoveBack()",        std::bind(&SmartDynamicArrayTester::Test_RemoveBack, this)));
        AddTest(cuTest::TestListItem("RemoveFront()",       std::bind(&SmartDynamicArrayTester::Test_RemoveFront, this)));
        AddTest(cuTest::TestListItem("RemoveAt()",          std::bind(&SmartDynamicArrayTester::Test_RemoveAt, this)));

        AddTest(cuTest::TestListItem("GetBack()",           std::bind(&SmartDynamicArrayTester::Test_GetBack, this)));
        AddTest(cuTest::TestListItem("GetFront()",          std::bind(&SmartDynamicArrayTester::Test_GetFront, this)));
        AddTest(cuTest::TestListItem("GetAt()",             std::bind(&SmartDynamicArrayTester::Test_GetAt, this)));

        AddTest(cuTest::TestListItem("AllocateMemory()",    std::bind(&SmartDynamicArrayTester::Test_AllocateMemory, this)));
        AddTest(cuTest::TestListItem("Resize()",            std::bind(&SmartDynamicArrayTester::Test_Resize, this)));
        AddTest(cuTest::TestListItem("IsFull()",            std::bind(&SmartDynamicArrayTester::Test_IsFull, this)));

        AddTest(cuTest::TestListItem("InsertBack()",        std::bind(&SmartDynamicArrayTester::Test_InsertBack, this)));
        AddTest(cuTest::TestListItem("InsertFront()",       std::bind(&SmartDynamicArrayTester::Test_InsertFront, this)));
        AddTest(cuTest::TestListItem("InsertAt()",          std::bind(&SmartDynamicArrayTester::Test_InsertAt, this)));

        AddTest(cuTest::TestListItem("Search()",            std::bind(&SmartDynamicArrayTester::Test_Search, this)));
    }

    virtual ~SmartDynamicArrayTester() { }

private:
    int Test_Constructor();
    int Test_InsertBack();
    int Test_InsertFront();
    int Test_InsertAt();
    int Test_RemoveBack();
    int Test_RemoveFront();
    int Test_RemoveAt();
    int Test_GetBack();
    int Test_GetFront();
    int Test_GetAt();
    int Test_AllocateMemory();
    int Test_Resize();
    int Test_IsFull();
    int Test_ShiftLeft();
    int Test_ShiftRight();
    int Test_Search();
};

int SmartDynamicArrayTester::Test_Constructor()
{
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartDynamicArrayTester::Test_Constructor", 3 );
    StartTestSet( "Test_Constructor", { } );
    std::ostringstream oss;

    StartTest( "1. Check initial member variable values" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        Set_Comments( "When a new SmartDynamicArray is created, m_array should be nullptr and m_itemCount and m_arraySize should be 0." );

        SmartDynamicArray<int> arr;

        oss << arr.m_array;
        Set_ExpectedOutput  ( "m_array is nullptr" );
        Set_ActualOutput    ( "m_array", oss.str() );

        if      ( arr.m_array != nullptr )                              { TestFail(); }
        else if ( !Set_Outputs( "m_arraySize", 0, arr.m_arraySize ) )   { TestFail(); }
        else if ( !Set_Outputs( "m_itemCount", 0, arr.m_itemCount ) )   { TestFail(); }
        else                                                            { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartDynamicArrayTester::Test_InsertBack()
{
    std::string functionName = "InsertBack";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartDynamicArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName, "Resize" } );

    StartTest( "0a. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartDynamicArray<int> arr;     /**/ arr.PushBack( 5 ); /**/   }
        catch( Exception::NotImplementedException& ex )    {   Set_Comments( ex.what() );      prereqsImplemented = false;     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "0b. Check if function AllocateMemory is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartDynamicArray<int> arr;     /**/ arr.AllocateMemory( 5 ); /**/             }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false; Utility::Logger::Out( ex.what() );     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Add three items, check they're in the correct position" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        std::string values[] = { "a", "b", "c" };
        int expectedItemCount = 3;

        SmartDynamicArray<std::string> arr;
        for ( int i = 0; i < 3; i++ )   { arr.PushBack( values[i] ); }

        Set_ExpectedOutput( "m_array", std::string( "not nullptr" ) );
        for ( int i = 0; i < 3; i++ )   { Set_ExpectedOutput( "m_array[" + Utility::StringUtil::ToString( i ) + "]", values[i] ); }

        std::ostringstream oss;
        oss << arr.m_array;
        Set_ActualOutput    ( "m_array", oss.str() );

        if      ( arr.m_array == nullptr )                                              { TestFail(); }
        else if ( !Set_Outputs( "m_itemCount", expectedItemCount, arr.m_itemCount ) )   { TestFail(); }
        else if ( !Set_Outputs( "m_array[0]", values[0], arr.m_array[0] ) )             { TestFail(); }
        else if ( !Set_Outputs( "m_array[1]", values[1], arr.m_array[1] ) )             { TestFail(); }
        else if ( !Set_Outputs( "m_array[2]", values[2], arr.m_array[2] ) )             { TestFail(); }
        else                                                                            { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. Check if prerequisite function Resize() is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartDynamicArray<int> arr;     /**/ arr.Resize( 15 ); /**/             }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "3. Add maximum amount of items, check if resize triggers, and can add more" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartDynamicArray<std::string> arr;
        arr.m_array = new std::string[3];
        arr.PushBack( "a" );
        arr.PushBack( "b" );
        arr.PushBack( "c" );
        // Resize should happen during this one:
        arr.PushBack( "d" );

        if      ( !Set_Outputs( "m_itemCount", 4, arr.m_itemCount ) )   { TestFail(); }
        else if ( !Set_Outputs( "m_array[0]", std::string( "a" ), arr.m_array[0] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[1]", std::string( "b" ), arr.m_array[1] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[2]", std::string( "c" ), arr.m_array[2] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[3]", std::string( "d" ), arr.m_array[3] ) )         { TestFail(); }
        else                                                                            { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartDynamicArrayTester::Test_InsertFront()
{
    std::string functionName = "InsertFront";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartDynamicArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName, "AllocateMemory", "Resize" } );
    std::ostringstream oss;

    StartTest( "0a. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartDynamicArray<int> arr;     /**/ arr.PushFront( 5 ); /**/             }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "0b. Check if function AllocateMemory is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartDynamicArray<int> arr;     /**/ arr.AllocateMemory( 5 ); /**/             }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Add three items, check they're in the correct position" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartDynamicArray<std::string> arr;
        arr.PushFront( "a" );
        arr.PushFront( "b" );
        arr.PushFront( "c" );

        std::ostringstream oss;
        oss << arr.m_array;
        Set_ExpectedOutput( "m_array", std::string( "not nullptr" ) );
        Set_ActualOutput    ( "m_array", oss.str() );

        if      ( arr.m_array == nullptr )                                              { TestFail(); }
        else if ( !Set_Outputs( "m_itemCount", 3, arr.m_itemCount ) )                   { TestFail(); }
        else if ( !Set_Outputs( "m_array[0]", std::string( "c" ), arr.m_array[0] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[1]", std::string( "b" ), arr.m_array[1] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[2]", std::string( "a" ), arr.m_array[2] ) )         { TestFail(); }
        else                                                                            { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. Check if prerequisite function Resize() is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartDynamicArray<int> arr;     /**/ arr.Resize( 15 ); /**/             }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "3. Add maximum amount of items, check if resize triggers, and can add more" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartDynamicArray<std::string> arr;
        arr.m_array = new std::string[3];
        arr.PushFront( "a" );
        arr.PushFront( "b" );
        arr.PushFront( "c" );
        // Resize should happen during this one:
        arr.PushFront( "d" );

        if      ( !Set_Outputs( "m_itemCount", 4, arr.m_itemCount ) )                   { TestFail(); }
        else if ( !Set_Outputs( "m_array[0]", std::string( "d" ), arr.m_array[0] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[1]", std::string( "c" ), arr.m_array[1] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[2]", std::string( "b" ), arr.m_array[2] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[3]", std::string( "a" ), arr.m_array[3] ) )         { TestFail(); }
        else                                                                            { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */


    FinishTestSet();
    return TestResult();
}

int SmartDynamicArrayTester::Test_InsertAt()
{
    std::string functionName = "InsertAt";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartDynamicArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
    std::ostringstream oss;

    StartTest( "0a. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartDynamicArray<int> arr;     arr.PushAt( 5, 5 );            }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "0b. Check if function AllocateMemory is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartDynamicArray<int> arr;     /**/ arr.AllocateMemory( 5 ); /**/             }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Set up array, insert at some position" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartDynamicArray<std::string> arr;
        arr.m_array = new std::string[ 5 ];
        arr.m_arraySize = 5;
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_itemCount = 3;

        arr.PushAt( "z", 1 );

        std::ostringstream oss;
        oss << arr.m_array;
        Set_ExpectedOutput  ( "m_array", std::string( "not nullptr" ) );
        Set_ActualOutput    ( "m_array", oss.str() );

        if      ( arr.m_array == nullptr )                                              { TestFail(); }
        else if ( !Set_Outputs( "m_itemCount", 4, arr.m_itemCount ) )                   { TestFail(); }
        else if ( !Set_Outputs( "m_array[0]", std::string( "a" ), arr.m_array[0] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[1]", std::string( "z" ), arr.m_array[1] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[2]", std::string( "b" ), arr.m_array[2] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[3]", std::string( "c" ), arr.m_array[3] ) )         { TestFail(); }
        else                                                                            { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Check for exception when inserting outside of range" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Array itemCount is 5, trying to call InsertAt( \"z\", 50 )." );
        SmartDynamicArray<std::string> arr;
        arr.m_array = new std::string[ 5 ];
        arr.m_arraySize = 5;
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_array[3] = "d";
        arr.m_array[4] = "e";
        arr.m_itemCount = 5;

        bool exceptionOccurred = false;

        try
        {
            arr.PushAt( "z", 50 );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartDynamicArrayTester::Test_RemoveBack()
{
    std::string functionName = "RemoveBack";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartDynamicArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
    std::ostringstream oss;

    StartTest( "0. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartDynamicArray<int> arr;     arr.PopBack();            }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Set up array, remove back" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartDynamicArray<std::string> arr;
        arr.m_array = new std::string[5];
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_arraySize = 5;
        arr.m_itemCount = 3;

        arr.PopBack();

        if      ( arr.m_array == nullptr )                                              { TestFail(); }
        else if ( !Set_Outputs( "m_itemCount", 2, arr.m_itemCount ) )                   { TestFail(); }
        else if ( !Set_Outputs( "m_array[0]", std::string( "a" ), arr.m_array[0] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[1]", std::string( "b" ), arr.m_array[1] ) )         { TestFail(); }
        else                                                                            { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Check for exception with empty array" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Array is empty, calling RemoveBack()." );
        SmartDynamicArray<std::string> arr;

        bool exceptionOccurred = false;

        try
        {
            arr.PopBack();
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartDynamicArrayTester::Test_RemoveFront()
{
    std::string functionName = "RemoveFront";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartDynamicArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
    std::ostringstream oss;

    StartTest( "0. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartDynamicArray<int> arr;     arr.PopFront();            }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Set up array, remove front" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartDynamicArray<std::string> arr;
        arr.m_array = new std::string[5];
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_arraySize = 5;
        arr.m_itemCount = 3;

        arr.PopFront();

        if      ( arr.m_array == nullptr )                                              { TestFail(); }
        else if ( !Set_Outputs( "m_itemCount", 2, arr.m_itemCount ) )                   { TestFail(); }
        else if ( !Set_Outputs( "m_array[0]", std::string( "b" ), arr.m_array[0] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[1]", std::string( "c" ), arr.m_array[1] ) )         { TestFail(); }
        else                                                                            { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Check for exception with empty array" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Array is empty, calling RemoveFront()." );
        SmartDynamicArray<std::string> arr;

        bool exceptionOccurred = false;

        try
        {
            arr.PopFront();
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartDynamicArrayTester::Test_RemoveAt()
{
    std::string functionName = "RemoveAt";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartDynamicArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
    std::ostringstream oss;

    StartTest( "0. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartDynamicArray<int> arr;     arr.PopAt( 1 );            }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Set up array, remove at position" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartDynamicArray<std::string> arr;
        arr.m_array = new std::string[5];
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_arraySize = 5;
        arr.m_itemCount = 3;

        arr.PopAt( 1 );

        if      ( arr.m_array == nullptr )                                              { TestFail(); }
        else if ( !Set_Outputs( "m_itemCount", 2, arr.m_itemCount ) )                   { TestFail(); }
        else if ( !Set_Outputs( "m_array[0]", std::string( "a" ), arr.m_array[0] ) )         { TestFail(); }
        else if ( !Set_Outputs( "m_array[1]", std::string( "c" ), arr.m_array[1] ) )         { TestFail(); }
        else                                                                            { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Check for exception with invalid index" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Array size is 5. Calling RemoveAt( 50 )." );
        SmartDynamicArray<std::string> arr;
        arr.m_arraySize = 5;
        arr.m_itemCount = 3;

        bool exceptionOccurred = false;

        try
        {
            arr.PopAt( 50 );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "3. ERROR CHECK: Check for exception with empty array" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Array is empty, calling RemoveAt( 0 )." );
        SmartDynamicArray<std::string> arr;

        bool exceptionOccurred = false;

        try
        {
            arr.PopAt( 0 );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartDynamicArrayTester::Test_GetBack()
{
    std::string functionName = "GetBack";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartDynamicArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
    std::ostringstream oss;

    StartTest( "0. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartDynamicArray<int> arr;     arr.GetBack();            }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Set up array, get back" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartDynamicArray<std::string> arr;
        arr.m_array = new std::string[5];
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_arraySize = 5;
        arr.m_itemCount = 3;

        if      ( !Set_Outputs( "GetBack()", std::string( "c" ), arr.GetBack() ) )       { TestFail(); }
        else                                                                        { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Check for exception when array is empty" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Array is empty. Calling GetBack()." );
        SmartDynamicArray<std::string> arr;

        bool exceptionOccurred = false;

        try
        {
            arr.GetBack();
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartDynamicArrayTester::Test_GetFront()
{
    std::string functionName = "GetFront";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartDynamicArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
    std::ostringstream oss;

    StartTest( "0. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartDynamicArray<int> arr;     arr.GetFront();            }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Set up array, get front" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartDynamicArray<std::string> arr;
        arr.m_array = new std::string[5];
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_arraySize = 5;
        arr.m_itemCount = 3;

        if      ( !Set_Outputs( "GetFront()", std::string( "a" ), arr.GetFront() ) )      { TestFail(); }
        else                                                                         { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Check for exception when array is empty" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Array is empty. Calling GetFront()." );
        SmartDynamicArray<std::string> arr;

        bool exceptionOccurred = false;

        try
        {
            arr.GetFront();
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartDynamicArrayTester::Test_GetAt()
{
    std::string functionName = "GetAt";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartDynamicArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
    std::ostringstream oss;

    StartTest( "0. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartDynamicArray<int> arr;     arr.GetAt( 5 );            }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Set up array, get at 1" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartDynamicArray<std::string> arr;
        arr.m_array = new std::string[5];
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_arraySize = 5;
        arr.m_itemCount = 3;

        if      ( !Set_Outputs( "GetAt( 1 )", std::string( "b" ), arr.GetAt( 1 ) ) )         { TestFail(); }
        else                                                                            { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Check for exception with invalid index" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Array size is 5. Calling GetAt( 50 )." );
        SmartDynamicArray<std::string> arr;
        arr.m_arraySize = 5;
        arr.m_itemCount = 3;

        bool exceptionOccurred = false;

        try
        {
            arr.GetAt( 50 );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "3. ERROR CHECK: Check for exception when array is empty" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Array is empty. Calling GetAt( 0 )." );
        SmartDynamicArray<std::string> arr;

        bool exceptionOccurred = false;

        try
        {
            arr.GetAt( 0 );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartDynamicArrayTester::Test_AllocateMemory()
{
    std::string functionName = "AllocateMemory";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartDynamicArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
    std::ostringstream oss;

    StartTest( "0. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartDynamicArray<int> arr;     arr.AllocateMemory( 5 );            }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Call AllocateMemory" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartDynamicArray<std::string> arr;
        arr.AllocateMemory( 5 );

        std::ostringstream oss;
        oss << arr.m_array;
        Set_ExpectedOutput  ( "m_array", std::string( "not nullptr" ) );
        Set_ActualOutput    ( "m_array", oss.str() );

        if      ( arr.m_array == nullptr )                              { TestFail(); }
        else if ( !Set_Outputs( "m_itemCount", 0, arr.m_itemCount ) )   { TestFail(); }
        else if ( !Set_Outputs( "m_arraySize", 5, arr.m_arraySize ) )   { TestFail(); }
        else                                                            { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Check for exception with negative size" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Calling AllocateMemory( -5 )." );
        SmartDynamicArray<std::string> arr;

        bool exceptionOccurred = false;

        try
        {
            arr.AllocateMemory( -5 );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartDynamicArrayTester::Test_Resize()
{
    std::string functionName = "Resize";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartDynamicArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
    std::ostringstream oss;

    StartTest( "0. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartDynamicArray<int> arr;     arr.Resize( 5 );            }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Create array, call Resize, check that values are copied over, new size" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        std::string values[] = { "a", "b", "c", "d" };

        SmartDynamicArray<std::string> arr;
        arr.m_array = new std::string[4];
        arr.m_arraySize = 4;
        arr.m_itemCount = 4;

        for ( int i = 0; i < 4; i++ )
        {
            arr.m_array[i] = values[i];
        }

        arr.Resize( 8 );

        std::ostringstream oss;
        oss << arr.m_array;
        Set_ExpectedOutput  ( "m_array", std::string( "not nullptr" ) );
        Set_ActualOutput    ( "m_array", oss.str() );

        if      ( arr.m_array == nullptr )                                      { TestFail(); }
        else if ( !Set_Outputs( "m_itemCount", 4, arr.m_itemCount ) )           { TestFail(); }
        else if ( !Set_Outputs( "m_arraySize", 8, arr.m_arraySize ) )           { TestFail(); }
        else if ( !Set_Outputs( "m_array[0]", values[0], arr.m_array[0] ) )     { TestFail(); }
        else if ( !Set_Outputs( "m_array[1]", values[1], arr.m_array[1] ) )     { TestFail(); }
        else if ( !Set_Outputs( "m_array[2]", values[2], arr.m_array[2] ) )     { TestFail(); }
        else if ( !Set_Outputs( "m_array[3]", values[3], arr.m_array[3] ) )     { TestFail(); }
        else                                                                    { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Check for exception when resize size is smaller than old size" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Array size is 5, calling Resize( 2 )." );
        SmartDynamicArray<std::string> arr;
        arr.m_arraySize = 5;

        bool exceptionOccurred = false;

        try
        {
            arr.Resize( 2 );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "3. ERROR CHECK: Check for exception when resize size is negative" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Array size is 5, calling Resize( -50 )." );
        SmartDynamicArray<std::string> arr;
        arr.m_arraySize = 5;

        bool exceptionOccurred = false;

        try
        {
            arr.Resize( -50 );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartDynamicArrayTester::Test_IsFull()
{
    std::string functionName = "IsFull";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartDynamicArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
    std::ostringstream oss;

    StartTest( "0. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartDynamicArray<int> arr;     arr.IsFull();            }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Check if IsFull returns true when full" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartDynamicArray<std::string> arr;
        arr.m_arraySize = 5;
        arr.m_itemCount = 5;
        if      ( !Set_Outputs( "IsFull()", true, arr.IsFull() ) )         { TestFail(); }
        else                                                               { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. Check if IsFull returns false when not full" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartDynamicArray<std::string> arr;
        arr.m_arraySize = 5;
        arr.m_itemCount = 3;
        if      ( !Set_Outputs( "IsFull()", false, arr.IsFull() ) )         { TestFail(); }
        else                                                                { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartDynamicArrayTester::Test_ShiftLeft()
{
    std::string functionName = "ShiftLeft";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartDynamicArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
    std::ostringstream oss;

    StartTest( "0. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        SmartDynamicArray<int> arr;
        arr.m_array = new int[5];
        try
        {
            arr.ShiftLeft( 0 );
        }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Populate array, call ShiftLeft, check positions" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        std::string originalValues[] = { "a", "b", "c", "d", "-" };

        SmartDynamicArray<std::string> arr;
        arr.m_array = new std::string[10];
        arr.m_arraySize = 10;
        arr.m_itemCount = 4;
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_array[3] = "d";
        arr.ShiftLeft( 1 );

        if      ( !Set_Outputs( "m_array[0]", std::string("a"), arr.m_array[0] ) )        { TestFail(); }
        else if ( !Set_Outputs( "m_array[1]", std::string("c"), arr.m_array[1] ) )        { TestFail(); }
        else if ( !Set_Outputs( "m_array[2]", std::string("d"), arr.m_array[2] ) )        { TestFail(); }
        else                                                                        { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Try to shift array out-of-bounds..." ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Array size is 5, calling ShiftLeft( 10 )." );
        SmartDynamicArray<std::string> arr;
        arr.m_array = new std::string[5];
        arr.m_arraySize = 5;
        arr.m_itemCount = 5;

        bool exceptionOccurred = false;

        try
        {
            arr.ShiftLeft( 10 );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        Set_ExpectedOutput( "Exception occurred", true );
        Set_ActualOutput( "Exception occurred", exceptionOccurred );

        if      ( !exceptionOccurred )          { TestFail(); }
        else                                    { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}


int SmartDynamicArrayTester::Test_ShiftRight()
{
    std::string functionName = "ShiftRight";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartDynamicArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
    std::ostringstream oss;

    StartTest( "0. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        SmartDynamicArray<int> arr;
        arr.m_array = new int[5];

        try                                     {
            arr.ShiftRight( 0 );
        }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false; Utility::Logger::Out( ex.what() );     }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Populate array, call ShiftRight, check positions" ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        std::string originalValues[] = { "a", "b", "c", "d", "-" };

        SmartDynamicArray<std::string> arr;
        arr.m_array = new std::string[10];
        arr.m_arraySize = 10;
        arr.m_itemCount = 4;
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_array[3] = "d";
        arr.ShiftRight( 1 );

        if      ( !Set_Outputs( "m_array[0]", std::string("a"), arr.m_array[0] ) )        { TestFail(); }
        else if ( !Set_Outputs( "m_array[2]", std::string("b"), arr.m_array[2] ) )        { TestFail(); }
        else if ( !Set_Outputs( "m_array[3]", std::string("c"), arr.m_array[3] ) )        { TestFail(); }
        else if ( !Set_Outputs( "m_array[4]", std::string("d"), arr.m_array[4] ) )        { TestFail(); }
        else                                                                        { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Try to shift array out-of-bounds..." ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Array size is 5, calling ShiftRight( 10 )." );
        SmartDynamicArray<std::string> arr;
        arr.m_array = new std::string[5];
        arr.m_arraySize = 5;
        arr.m_itemCount = 5;

        bool exceptionOccurred = false;

        try
        {
            arr.ShiftRight( 10 );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        Set_ExpectedOutput( "Exception occurred", true );
        Set_ActualOutput( "Exception occurred", exceptionOccurred );

        if      ( !exceptionOccurred )          { TestFail(); }
        else                                    { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

int SmartDynamicArrayTester::Test_Search()
{
    std::string functionName = "Search";
    Utility::Logger::OutHighlight( "TEST SET BEGIN", "SmartDynamicArrayTester::Test_" + functionName, 3 );
    StartTestSet( "Test_" + functionName, { functionName } );
    std::ostringstream oss;

    StartTest( "0. Check if function " + functionName + " is implemented" ); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
    {
        bool prereqsImplemented = true;
        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );

        try                                     {   SmartDynamicArray<int> arr;     arr.Search( 10 );            }
        catch( Exception::NotImplementedException& ex )    {   prereqsImplemented = false;  Utility::Logger::Out( ex.what() );    }
        catch( ... ) { }

        if  ( prereqsImplemented )  { PrereqTest_Success( functionName ); }
        else                        { return PrereqTest_Abort( functionName ); }
    } /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "1. Set up array, search for an item." ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        SmartDynamicArray<std::string> arr;
        arr.m_array = new std::string[5];
        arr.m_array[0] = "a";
        arr.m_array[1] = "b";
        arr.m_array[2] = "c";
        arr.m_arraySize = 5;
        arr.m_itemCount = 3;

        if      ( !Set_Outputs( "Search( \"b\" )", 1, arr.Search( "b" ) ) )             { TestFail(); }
        else                                                                            { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    StartTest( "2. ERROR CHECK: Search for an item that isn't in the array." ); { /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        Set_Comments( "Searching for \"z\" in the array." );
        SmartDynamicArray<std::string> arr;
        arr.m_array = new std::string[5];
        arr.m_arraySize = 5;
        arr.m_itemCount = 3;

        bool exceptionOccurred = false;

        try
        {
            arr.Search( "z" );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        if      ( !Set_Outputs( "Exception occurred", true, exceptionOccurred ) )        { TestFail(); }
        else                                                                             { TestPass(); }
    } FinishTest(); /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

    FinishTestSet();
    return TestResult();
}

} // End of namespace

#endif
