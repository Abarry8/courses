#include "Message.h"

void Message::Setup( std::string newAction, std::string newValue )
{
  m_action = newAction;
  m_value = newValue;
}

std::string Message::GetAction()
{
  return m_action;
}

std::string Message::GetValue()
{
  return m_value;
}
