#ifndef _SANDWICH
#define _SANDWICH

#include <stack>
#include <string>
#include <ostream>

class Sandwich
{
  public:
  void Clear();
  void Add( std::string topping );
  void Remove();

  private:
  std::stack<std::string> m_toppings;

  friend std::ostream& operator<<( std::ostream& out, const Sandwich& me );
};

#endif
