#include "Utilities/Logger.h"
#include "Program/Program.h"
#include "DataStructure/LinkedList/LinkedListTester.h"
#include "DataStructure/Stack/StackTester.h"
#include "DataStructure/Queue/QueueTester.h"

#include <iostream>
using namespace std;

int main()
{
	// Initialize the debug logger
	Utility::Logger::Setup();

	cout << "STARTUP" << endl;
	cout << "1. Run unit tests" << endl;
	cout << "2. Run program" << endl;
	cout << endl << ">> ";
	int choice;
	cin >> choice;

	switch (choice)
	{
		case 1: {
			DataStructure::LinkedListTester llTester;
			llTester.Start();

			DataStructure::QueueTester qTester;
			qTester.Start();

			DataStructure::StackTester sTester;
			sTester.Start();
			break;
		}
		case 2: {
			Program program;
			program.SetDataPath("../Data/");
			program.Run();
			break;
		}
	}

	// Clean up the debug logger
	Utility::Logger::Cleanup();

	return 0;
}







