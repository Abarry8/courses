#ifndef _BINARY_SEARCH_TREE_HPP
#define _BINARY_SEARCH_TREE_HPP

// Project includes
#include "BinarySearchTreeNode.h"
#include "../../Utilities/Logger.h"
#include "../../Utilities/StringUtil.h"

#include <stdexcept>
using namespace std;

namespace DataStructure
{

template <typename TK, typename TD>
//! A template binary search tree class that takes a KEY and a DATA
class BinarySearchTree
{
private:
    //! A pointer to the root node of the tree; TK = data type of the key, TD = data type of the data.
    BinarySearchTreeNode<TK, TD>* m_ptrRoot;

    //! The amount of nodes in the tree
    int m_nodeCount;

    //! The tester is our friend
    friend class BinarySearchTreeTester;

public:
    //! BinarySearchTree Constructor
    BinarySearchTree();
    //! BinarySearchTree Destructor
    ~BinarySearchTree();

    // Basic functionality
    //! Function that begins the recursive push function. Add new item to tree.
    void Push( const TK& newKey, const TD& newData );
    //! Function that begins the recursive contains function. Check if key is contained within the tree.
    bool Contains( const TK& key );
    //! Function that begins the recursive find node function. Get data relayed to some key stored in the tree.
    TD& GetData( const TK& key );

    //! Retrieves the item at the root
    TD& GetRoot();
    //! Removes the root item and reorganizes the tree
    void PopRoot();
    //! Shifts nodes for deletion operation
    void ShiftNodes(BinarySearchTreeNode<TK, TD>* node1, BinarySearchTreeNode<TK, TD>* node2);

    // Traversal functions
    //! Function that begins the recursive in-order traversal.
    std::string GetInOrder();
    //! Function that begins the recursive pre-order traversal.
    std::string GetPreOrder();
    //! Function that begins the recursive post-order traversal.
    std::string GetPostOrder();

    // Additional functionality
    //! Function that begins the recursive get min key, returns key with smallest value.
    TK& GetMinKey();
    //! Function that begins the recursive get max key, returns key with largest value.
    TK& GetMaxKey();
    //! Function that beginst he recursive get height, returns the height of the tree.
    int GetHeight();
    //! Get amount of nodes in tree.
    int GetCount();

    //    void Delete( const TK& key );

private:
    //! Function that begins the recursive find node process based on node's key.
    BinarySearchTreeNode<TK, TD>* FindNode( const TK& key );

    // Recursive traversal functions
    //! Recursive in-order traversal function
    void RecursiveGetInOrder  ( BinarySearchTreeNode<TK, TD>* ptrCurrent, std::stringstream& stream );
    //! Recursive pre-order traversal function
    void RecursiveGetPreOrder ( BinarySearchTreeNode<TK, TD>* ptrCurrent, std::stringstream& stream );
    //! Recursive post-order traversal function
    void RecursiveGetPostOrder( BinarySearchTreeNode<TK, TD>* ptrCurrent, std::stringstream& stream );

    // Recursive additional functionality
    //! Recurses through nodes in tree looking for node with given key.
    BinarySearchTreeNode<TK, TD>* RecursiveFindNode( const TK& key, BinarySearchTreeNode<TK, TD>* ptrCurrent );
    //! Recurses through tree looking for the node with the maximum key value.
    BinarySearchTreeNode<TK, TD>* RecursiveGetMax( BinarySearchTreeNode<TK, TD>* ptrCurrent );
    //! Recurses through tree looking for the node with the minimum key value.
    BinarySearchTreeNode<TK, TD>* RecursiveGetMin( BinarySearchTreeNode<TK, TD>* ptrCurrent );
    //! Recurses through the tree comparing heights of left and right sub-trees to get the overall height.
    int RecursiveGetHeight( BinarySearchTreeNode<TK, TD>* ptrCurrent );
    //! Recurses through the tree searching for a node with a matching key.
    bool RecursiveContains( const TK& key, BinarySearchTreeNode<TK, TD>* ptrCurrent );
    //! Recurses through the tree searching for a position to insert the new data based on its key.
    void RecursivePush( const TK& newKey, const TD& newData, BinarySearchTreeNode<TK, TD>* ptrCurrent );

    //! Removes a node - the root of its own subtree
    void PopNode(BinarySearchTreeNode<TK, TD>* ptrCurrent);

    //! Gets the node with the smallest key that is greater than ptrCurrent's key.
    BinarySearchTreeNode<TK, TD>* GetSuccessor(BinarySearchTreeNode<TK, TD>* ptrCurrent);

};

/* **************************************************************************************** */
/* ******************************************************************* Function definitions */
/* **************************************************************************************** */

/**
Initialize the root node pointer to nullptr and the node count to 0.
*/
template <typename TK, typename TD>
BinarySearchTree<TK,TD>::BinarySearchTree()
{
    Utility::Logger::OutHighlight( "FUNCTION BEGIN", "BinarySearchTree<TK,TD> Constructor" );
    m_ptrRoot = nullptr;
    m_nodeCount = 0;
}


/**
If the root node pointer is not nullptr, then delete it.
*/
template <typename TK, typename TD>
BinarySearchTree<TK,TD>::~BinarySearchTree()
{
    Utility::Logger::OutHighlight( "FUNCTION BEGIN", "BinarySearchTree<TK,TD> Destructor" );
    if ( m_ptrRoot != nullptr )
    {
        delete m_ptrRoot;
    }
}


/**
Error check: If the key is already in the tree, then throw a std::runtime_error with an error message. (Use teh Contains method)

Scenario 1: If m_ptrRoot is nullptr, create the new node here.

Scenario 2: Otherwise, start the push by calling the RecursivePush function, passing in the key, data, and the root node pointer.

@param  newKey      The key of the new item to be added.
@param  newData     The data of the new item to be added.
*/
template <typename TK, typename TD>
void BinarySearchTree<TK,TD>::Push( const TK& newKey, const TD& newData )
{
    throw Exception::NotImplementedException( "Push" ); // Erase this once you work on this function
}


/**
Recurses through the tree, finding the proper place to put a new node.

#### Creating a node at ptrCurrent:

- Initialize the new node at the pointer. You can set its key and data via the Node constructor.
    - `ptrCurrent = new Node<TK,TD>( newKey, newData );` OR
    - `ptrCurrent->ptrLeft = new Node<TK,TD>( newKey, newData );` OR
    - `ptrCurrent->ptrRight = new Node<TK,TD>( newKey, newData );`
- Increment the node count by 1.
    - `m_nodeCount++;`

#### TERMINATING CASES:

1. If ptrCurrent is nullptr, throw a runtime error stating that the node is nullptr.
2. Otherwise if the newKey is less than the ptrCurrent's key AND there is no node to the left, then create the node to the left.
3. Otherwise if the newKey is greater than the ptrCurrent's key AND there is no node to the right, then create the node to the right.

#### RECURSIVE CASES:

1. If the newKey is less than the ptrCurrent's key AND there IS a node to the left, recurse left.
2. Otherwise if the newKey is greater than the ptrCurrent's key AND there IS a node to the right, recurse right.
*/
template <typename TK, typename TD>
void BinarySearchTree<TK,TD>::RecursivePush( const TK& newKey, const TD& newData, BinarySearchTreeNode<TK, TD>* ptrCurrent )
{
    throw Exception::NotImplementedException( "RecursivePush" ); // Erase this once you work on this function
}

/**
Return the result of the RecursiveContains function, passing in the key and the root node pointer.

@param  key     The key to search for in the tree.
@return         True if the key is in the tree, or false otherwise.
*/
template <typename TK, typename TD>
bool BinarySearchTree<TK,TD>::Contains( const TK& key )
{
    throw Exception::NotImplementedException( "Contains" ); // Erase this once you work on this function
}

/**
Recurses through the tree, looking to see if a node with the key given exists.

#### TERMINATING CASES:

1. False if ptrCurrent is nullptr.
2. True if ptrCurrent's key matches the key.
3. False if there are no more child nodes to traverse (no left child / right child of ptrCurrent).

#### RECURSIVE CASES:

1. Recurse left if the key is less than ptrCurrent->key, returning that value.
2. Recurse right if the key is greater than ptrCurrent->key, returning that value.

@param  key             The key we're searching for.
@param  ptrCurrent      The current node we're investigating.
@return                 True if the key is found, or false otherwise.
*/
template <typename TK, typename TD>
bool BinarySearchTree<TK,TD>::RecursiveContains( const TK& key, BinarySearchTreeNode<TK, TD>* ptrCurrent )
{
    throw Exception::NotImplementedException( "RecursiveContains" ); // Erase this once you work on this function
}

/**
Recurses through the tree, looking to see if a node with the key given exists.

#### TERMINATING CASES:

1. nullptr if ptrCurrent is nullptr.
2. ptrCurrent if ptrCurrent's key matches the key.

#### RECURSIVE CASES:

1. Recurse left if the key is less than ptrCurrent->key, returning that value.
2. Recurse right if the key is greater than ptrCurrent->key, returning that value.

@param  key             The key we're searching for.
@param  ptrCurrent      The current node we're investigating.
@return                 The node with the matching key if found, nullptr otherwise.
*/
template <typename TK, typename TD>
BinarySearchTreeNode<TK, TD>* BinarySearchTree<TK,TD>::RecursiveFindNode( const TK& key, BinarySearchTreeNode<TK, TD>* ptrCurrent )
{
    throw Exception::NotImplementedException( "RecursiveFindNode" ); // Erase this once you work on this function
}

/**
Begin the recursive in-order traversal by calling RecursiveGetInOrder and passing in the root node pointer and the std::string stream.

@return     The list of keys in the tree, in-order.
*/
template <typename TK, typename TD>
std::string BinarySearchTree<TK,TD>::GetInOrder()
{
//    Utility::Logger::OutHighlight( "FUNCTION BEGIN", "BinarySearchTree<TK,TD>::GetInOrder" );
    std::stringstream stream;
    RecursiveGetInOrder( m_ptrRoot, stream );
    return stream.str();
}

/**
Recurse in-order, building the stream.

IF ptrCrrrent IS nullptr THEN JUST RETURN.

1. Recurse to the left child.
2. Stream out the current node's key (`stream << ptrCurrent->key;`)
3. Recurse to the right child.
*/
template <typename TK, typename TD>
void BinarySearchTree<TK,TD>::RecursiveGetInOrder( BinarySearchTreeNode<TK, TD>* ptrCurrent, std::stringstream& stream )
{
    throw Exception::NotImplementedException( "RecursiveGetInOrder" ); // Erase this once you work on this function
}

/**
Begin the recursive pre-order traversal by calling RecursiveGetPreOrder and passing in the root node pointer and the std::string stream.

@return     The list of keys in the tree, pre-order.
*/
template <typename TK, typename TD>
std::string BinarySearchTree<TK,TD>::GetPreOrder()
{
//    Utility::Logger::OutHighlight( "FUNCTION BEGIN", "BinarySearchTree<TK,TD>::GetPreOrder" );
    std::stringstream stream;
    RecursiveGetPreOrder( m_ptrRoot, stream );
    return stream.str();
}

/**
Recurse pre-order, building the stream.

IF ptrCrrrent IS nullptr THEN JUST RETURN.

1. Stream out the current node's key (`stream << ptrCurrent->key;`)s
2. Recurse to the left child.
3. Recurse to the right child.
*/
template <typename TK, typename TD>
void BinarySearchTree<TK,TD>::RecursiveGetPreOrder( BinarySearchTreeNode<TK, TD>* ptrCurrent, std::stringstream& stream )
{
    throw Exception::NotImplementedException( "RecursiveGetPreOrder" ); // Erase this once you work on this function
}

/**
Begin the recursive post-order traversal by calling RecursiveGetPostOrder and passing in the root node pointer and the std::string stream.

@return     The list of keys in the tree, post-order.
*/
template <typename TK, typename TD>
std::string BinarySearchTree<TK,TD>::GetPostOrder()
{
//    Utility::Logger::OutHighlight( "FUNCTION BEGIN", "BinarySearchTree<TK,TD>::GetPostOrder" );
    std::stringstream stream;
    RecursiveGetPostOrder( m_ptrRoot, stream );
    return stream.str();
}

/**
Recurse post-order, building the stream.

IF ptrCrrrent IS nullptr THEN JUST RETURN.

1. Recurse to the left child.
3. Recurse to the right child.
2. Stream out the current node's key (`stream << ptrCurrent->key;`)
*/
template <typename TK, typename TD>
void BinarySearchTree<TK,TD>::RecursiveGetPostOrder( BinarySearchTreeNode<TK, TD>* ptrCurrent, std::stringstream& stream )
{
    throw Exception::NotImplementedException( "RecursiveGetPostOrder" ); // Erase this once you work on this function
}

/**
Begin the recursive search for the max key by calling the RecursiveGetMax function, passing in the root node pointer.

@return     A reference to the max key in the tree.
*/
template <typename TK, typename TD>
TK& BinarySearchTree<TK,TD>::GetMaxKey()
{
    throw Exception::NotImplementedException( "GetMaxKey" ); // Erase this once you work on this function
}

/**
Recurse to find the node with the max key in the tree.

#### TERMINATING CASE:

1. If ptrCurrent is nullptr, throw a std::runtime_error.
2. Otherwise, if ptrCurrent does not have a right child, we are at the max node: return ptrCurrent.

#### RECURSIVE CASE:

1. Recurse to the right, returning the result.
*/
template <typename TK, typename TD>
BinarySearchTreeNode<TK, TD>* BinarySearchTree<TK,TD>::RecursiveGetMax( BinarySearchTreeNode<TK, TD>* ptrCurrent )
{
    throw Exception::NotImplementedException( "RecursiveGetMax" ); // Erase this once you work on this function
}

/**
Begin the recursive search for the min key by calling the RecursiveGetMin function, passing in the root node pointer.

@return     A reference to the min key in the tree.
*/
template <typename TK, typename TD>
TK& BinarySearchTree<TK,TD>::GetMinKey()
{
    throw Exception::NotImplementedException( "GetMinKey" ); // Erase this once you work on this function
}


/**
Recurse to find the node with the min key in the tree.

#### TERMINATING CASES:

1. If ptrCurrent is nullptr, throw a std::runtime_error.
2. Otherwise, if ptrCurrent does not have a left child, we are at the min node: return ptrCurrent.

#### RECURSIVE CASE:

1. Recurse to the left, returning the result.
*/
template <typename TK, typename TD>
BinarySearchTreeNode<TK, TD>* BinarySearchTree<TK,TD>::RecursiveGetMin( BinarySearchTreeNode<TK, TD>* ptrCurrent )
{
    throw Exception::NotImplementedException( "RecursiveGetMin" ); // Erase this once you work on this function
}

/**
Return the count of nodes.

@return     The amonut of nodes in the tree.
*/
template <typename TK, typename TD>
int BinarySearchTree<TK,TD>::GetCount()     // done
{
    return m_nodeCount;
}


/**
Begin the recursive search for a node with a key by calling the RecursiveFindNode, passing in the key and root node pointer.

@param  key     The key to search for in the tree.
@return         The node found with the key given (possibly nullptr).
*/
template <typename TK, typename TD>
BinarySearchTreeNode<TK, TD>* BinarySearchTree<TK,TD>::FindNode( const TK& key )
{
    throw Exception::NotImplementedException( "FindNode" ); // Erase this once you work on this function
}


/**
If the root node pointer is set to nullptr, return 0. Otherwise, begin the recursive get height process
by calling RecursiveGetHeight and passing in the root node pointer.

@return     The height of the tree.
*/
template <typename TK, typename TD>
int BinarySearchTree<TK,TD>::GetHeight()
{
    throw Exception::NotImplementedException( "GetHeight" ); // Erase this once you work on this function
}

/**
Recurse to get the max height of the left and right subtree in order to find this tree's height.

1. If ptrCurrent is nullptr, return 0.
2. Create two int variables to store the leftHeight and the rightHeight. Initialize both to 0.
3. If ptrCurrent has a left child, recurse left and store the result into leftHeight, + 1 to count the current node.
4. If ptrCurrent has a right child, recurse right and store the result into rightHeight, +1 to count the current node.
5. If leftHeight is greater than rightHeight, return leftHeight.
6. Otherwise, return rightHeight.
*/
template <typename TK, typename TD>
int BinarySearchTree<TK,TD>::RecursiveGetHeight( BinarySearchTreeNode<TK, TD>* ptrCurrent )
{
    throw Exception::NotImplementedException( "RecursiveGetHeight" ); // Erase this once you work on this function
}

/**
Use the FindNode function to find the node that has the key given.
If FindNode returns nullptr, then throw a std::runtime_error exception.
Otherwise, return the node's data.

@param  key     The key to search for in the tree.
@return         A reference to the data of the node.
*/
template <typename TK, typename TD>
TD& BinarySearchTree<TK,TD>::GetData( const TK& key )
{
    throw Exception::NotImplementedException( "GetData" ); // Erase this once you work on this function
}

/* **************************************************************************************** */
/* ****************************************************** Deletion functions (already done) */
/* **************************************************************************************** */

/**
Returns the data located at the root of the tree, or throws an exception
if the tree is empty.

@return         The data stored at the root of the tree
*/
template <typename TK, typename TD>
TD& BinarySearchTree<TK, TD>::GetRoot()
{
    if (m_ptrRoot == nullptr) { throw runtime_error("Tree is empty!"); }
    return m_ptrRoot->data;
}

template <typename TK, typename TD>
void BinarySearchTree<TK, TD>::PopRoot()
{
    PopNode(m_ptrRoot);
}

// https://en.wikipedia.org/wiki/Binary_search_tree#Deletion
template <typename TK, typename TD>
void BinarySearchTree<TK, TD>::PopNode(BinarySearchTreeNode<TK, TD>* ptrCurrent)
{
    if (m_ptrRoot->ptrLeft == nullptr)
    {
        ShiftNodes(ptrCurrent, ptrCurrent->ptrRight);
    }
    else if (m_ptrRoot->ptrRight == nullptr)
    {
        ShiftNodes(ptrCurrent, ptrCurrent->ptrLeft);
    }
    else
    {
        BinarySearchTreeNode<TK, TD>* ptrSuccessor = GetSuccessor(ptrCurrent);
        if (ptrSuccessor->ptrParent != ptrCurrent)
        {
            ShiftNodes(ptrSuccessor, ptrSuccessor->ptrRight);
            ptrSuccessor->ptrRight = ptrCurrent->ptrRight;
            ptrSuccessor->ptrRight->ptrParent = ptrSuccessor;
        }

        ShiftNodes(ptrCurrent, ptrSuccessor);
        ptrSuccessor->ptrLeft = ptrCurrent->ptrLeft;
        ptrSuccessor->ptrLeft->ptrParent = ptrSuccessor;
    }

    m_nodeCount--;
}

// https://en.wikipedia.org/wiki/Binary_search_tree#Deletion
template <typename TK, typename TD>
void BinarySearchTree<TK, TD>::ShiftNodes(BinarySearchTreeNode<TK, TD>* node1, BinarySearchTreeNode<TK, TD>* node2)
{
    if (node1->ptrParent == nullptr)
    {
        m_ptrRoot = node2;
    }
    else if (node1 == node1->ptrParent->ptrLeft)
    {
        node1->ptrParent->ptrLeft = node2;
    }
    else
    {
        node1->ptrParent->ptrRight = node2;
    }

    if (node2 != nullptr)
    {
        node2->ptrParent = node1->ptrParent;
    }
}

// https://en.wikipedia.org/wiki/Binary_search_tree#Successor_and_predecessor
template <typename TK, typename TD>
BinarySearchTreeNode<TK, TD>* BinarySearchTree<TK, TD>::GetSuccessor(BinarySearchTreeNode<TK, TD>* ptrCurrent)
{
    if (ptrCurrent->ptrRight != nullptr)
    {
        return RecursiveGetMin(ptrCurrent->ptrRight);
    }

    BinarySearchTreeNode<TK, TD>* ptrPrevious = ptrCurrent->ptrParent;
    while (ptrPrevious != nullptr && ptrCurrent == ptrPrevious->ptrRight)
    {
        ptrCurrent = ptrPrevious;
        ptrPrevious = ptrCurrent->ptrParent;
    }

    return ptrPrevious;
}

} // End namespace

#endif
