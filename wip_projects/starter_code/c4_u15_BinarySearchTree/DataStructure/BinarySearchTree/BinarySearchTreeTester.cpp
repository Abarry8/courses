#include "BinarySearchTreeTester.h"

namespace DataStructure
{

    int BinarySearchTreeTester::Test_NodeConstructor()
    {
        const std::string TESTFUNCTION(__func__);
        const std::string FUNCTION = std::string(__func__).replace(0, 5, "");
        StartTestSet(TESTFUNCTION, { FUNCTION });
        std::ostringstream oss;

        // PREREQUISITES IMPLEMENTED?
        StartTest("0a. Check if function " + FUNCTION + " is implemented");
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput(FUNCTION, std::string("Implemented"));

            try { BinarySearchTreeNode<int, int> item; }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what()); prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success(FUNCTION); }
            else { return PrereqTest_Abort(FUNCTION); }
        }

        // ADDITIONAL TESTS
        StartTest( "1. Create new node, ptrLeft/ptrRight should be nullptr." ); {
            Set_Comments( "Make sure you're initializing pointers to nullptr from within constructors!" );
    
            BinarySearchTreeNode<char, std::string> node;
    
            Set_ExpectedOutput  ( "Node's ptrLeft is nullptr" );
            oss << node.ptrLeft;
            Set_ActualOutput    ( "Node's ptrLeft", oss.str() );

            Set_ExpectedOutput("Node's ptrRight is nullptr");
            oss.str("");
            oss.clear();
            oss << node.ptrRight;
            Set_ActualOutput    ( "Node's ptrRight", oss.str() );
    
            if ( node.ptrLeft != nullptr ) { TestFail(); }
            else if ( node.ptrRight != nullptr ) { TestFail(); }
            else { TestPass(); }
        } FinishTest();
        
    
        FinishTestSet();
        return TestResult();
    }
    
    int BinarySearchTreeTester::Test_TreeConstructor()
    {
        const std::string TESTFUNCTION(__func__);
        const std::string FUNCTION = std::string(__func__).replace(0, 5, "");
        StartTestSet(TESTFUNCTION, { FUNCTION });
        std::ostringstream oss;

        // PREREQUISITES IMPLEMENTED?
        StartTest("0a. Check if function " + FUNCTION + " is implemented");
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput(FUNCTION, std::string("Implemented"));

            try { BinarySearchTreeNode<int, int> item; }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what()); prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success(FUNCTION); }
            else { return PrereqTest_Abort(FUNCTION); }
        }

        // ADDITIONAL TESTS
        StartTest("1. Create new tree, check that m_ptrRoot is nullptr and m_nodeCount is 0.");  {
            Set_Comments( "Make sure you're initializing pointers to nullptr from within constructors!" );
    
            BinarySearchTree<char, std::string> tree;
    
            int expectedResult = 0;
            int actualResult = tree.m_nodeCount;
    
            Set_ExpectedOutput  ( "m_nodeCount", expectedResult );
            Set_ActualOutput    ( "m_nodeCount", actualResult );
    
            oss << tree.m_ptrRoot;
            Set_ExpectedOutput  ( "Trees's m_ptrRoot", std::string( "nullptr" ) );
            Set_ActualOutput    ( "Trees's m_ptrRoot", oss.str() );
    
            if ( actualResult != expectedResult ) { TestFail(); }
            else if ( tree.m_ptrRoot != nullptr ) { TestFail(); }
            else { TestPass(); }
        }  FinishTest();
    
        FinishTestSet();
        return TestResult();
    }
    
    int BinarySearchTreeTester::Test_Push()
    {
        const std::string TESTFUNCTION(__func__);
        const std::string FUNCTION = std::string(__func__).replace(0, 5, "");
        StartTestSet(TESTFUNCTION, { FUNCTION, "RecursivePush", "Contains" });
        std::ostringstream oss;

        // PREREQUISITES IMPLEMENTED?
        StartTest("0a. Check if function " + FUNCTION + " is implemented");
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput(FUNCTION, std::string("Implemented"));

            try { BinarySearchTree<int, int> item; item.Push(1, 2); }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what()); prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success(FUNCTION); }
            else { return PrereqTest_Abort(FUNCTION); }
        }

        StartTest("0b. Check if prereq function RecursivePush is implemented"); 
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput("RecursivePush", std::string("Implemented"));

            try { BinarySearchTree<char, std::string> tree; tree.RecursivePush('a', "A", tree.m_ptrRoot); }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what());      prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success("RecursivePush"); }
            else { return PrereqTest_Abort("RecursivePush"); }
        } 

        StartTest("0c. Check if prereq function Contains is implemented");
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput("Contains", std::string("Implemented"));

            try { BinarySearchTree<char, std::string> tree; tree.Contains('a'); }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what());      prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success("Contains"); }
            else { return PrereqTest_Abort("Contains"); }
        }

        StartTest("1. Push one item to a tree. Make sure it becomes the root. Validate key/data.");  {
            Set_Comments( "Push( 'a', \"apple\" )" );
    
            BinarySearchTree<char, std::string> tree;
            tree.Push( 'a', "apple" );
    
            if ( !Set_Outputs( "m_nodeCount",       1,          tree.m_nodeCount ) )        { TestFail(); }
    
            // Check ROOT
            else if ( tree.m_ptrRoot == nullptr )
            {
                Set_Comments( "m_ptrRoot was nullptr" );
                TestFail();
            }
            else if ( !Set_Outputs( "m_ptrRoot's key",   'a',        tree.m_ptrRoot->key ) )     { TestFail(); }
            else if ( !Set_Outputs( "m_ptrRoot's data",  "apple",    tree.m_ptrRoot->data ) )    { TestFail(); }
            else                                                                                 { TestPass(); }
        } FinishTest();
    
        StartTest("2. Push 3 items to a tree. Check positions.");  {    
            Set_Comments( "Push( 'b', \"banana\" )" );
            Set_Comments( "Push( 'a', \"apple\" )" );
            Set_Comments( "Push( 'c', \"cranberry\" )" );
    
            BinarySearchTree<char, std::string> tree;
            tree.Push( 'b', "banana" );
            tree.Push( 'a', "apple" );
            tree.Push( 'c', "cranberry" );
    
            std::string txtTree = "<pre>";
            txtTree += "  b     \n";
            txtTree += " / \\   \n";
            txtTree += "a   c   \n";
            txtTree += "</pre>";
            Set_Comments( txtTree );
    
            if ( !Set_Outputs( "m_nodeCount",       3,          tree.m_nodeCount ) )        { TestFail(); }
    
            // Check ROOT
            else if ( tree.m_ptrRoot == nullptr )
            {
                Set_Comments( "m_ptrRoot was nullptr" );
                TestFail();
            }
            else if ( !Set_Outputs( "m_ptrRoot's key",   'b',         tree.m_ptrRoot->key ) )     { TestFail(); }
            else if ( !Set_Outputs( "m_ptrRoot's data",  "banana",    tree.m_ptrRoot->data ) )    { TestFail(); }
    
            // Check ROOT- > LEFT
            else if ( tree.m_ptrRoot->ptrLeft == nullptr )
            {
                Set_Comments( "m_ptrRoot->ptrLeft was nullptr" );
                TestFail();
            }
            else if ( !Set_Outputs( "m_ptrRoot->ptrLeft's key",   'a',         tree.m_ptrRoot->ptrLeft->key ) )     { TestFail(); }
            else if ( !Set_Outputs( "m_ptrRoot->ptrLeft's data",  "apple",     tree.m_ptrRoot->ptrLeft->data ) )    { TestFail(); }
    
            // Check ROOT -> RIGHT
            else if ( tree.m_ptrRoot->ptrRight == nullptr )
            {
                Set_Comments( "m_ptrRoot->ptrRight was nullptr" );
                TestFail();
            }
            else if ( !Set_Outputs( "m_ptrRoot->ptrRight's key",   'c',             tree.m_ptrRoot->ptrRight->key ) )     { TestFail(); }
            else if ( !Set_Outputs( "m_ptrRoot->ptrRight's data",  "cranberry",     tree.m_ptrRoot->ptrRight->data ) )    { TestFail(); }
            else                                                                                 { TestPass(); }
        } FinishTest();
    
        StartTest("3. Push 7 items to a tree. Check positions.");  {
            Set_Comments( "Push( 'd', \"dates\" )" );
            Set_Comments( "Push( 'b', \"banana\" )" );
            Set_Comments( "Push( 'f', \"figs\" )" );
            Set_Comments( "Push( 'a', \"apple\" )" );
            Set_Comments( "Push( 'c', \"cranberry\" )" );
            Set_Comments( "Push( 'e', \"elderberry\" )" );
            Set_Comments( "Push( 'g', \"grapefruit\" )" );
    
            std::string txtTree = "<pre>";
            txtTree += "       d                \n";
            txtTree += "    /     \\            \n";
            txtTree += "  b         f           \n";
            txtTree += " / \\       / \\        \n";
            txtTree += "a   c     e   g         \n";
            txtTree += "</pre>";
            Set_Comments( txtTree );
    
            BinarySearchTree<char, std::string> tree;
            tree.Push( 'd', "dates" );
            tree.Push( 'b', "banana" );
            tree.Push( 'f', "figs" );
            tree.Push( 'a', "apple" );
            tree.Push( 'c', "cranberry" );
            tree.Push( 'e', "elderberry" );
            tree.Push( 'g', "grapefruit" );
    
            if ( !Set_Outputs( "m_nodeCount",       7,          tree.m_nodeCount ) )        { TestFail(); }
    
            // Check ROOT (d)
            else if ( tree.m_ptrRoot == nullptr ) { Set_Comments( "m_ptrRoot was nullptr" );      TestFail(); }
            else if ( !Set_Outputs( "m_ptrRoot's key",   'd',                   tree.m_ptrRoot->key ) )                       { TestFail(); }
            else if ( !Set_Outputs( "m_ptrRoot's data",  "dates",               tree.m_ptrRoot->data ) )                      { TestFail(); }
    
            // Check ROOT- > LEFT (b)
            else if ( tree.m_ptrRoot->ptrLeft == nullptr ) { Set_Comments( "m_ptrRoot->ptrLeft was nullptr" );     TestFail(); }
            else if ( !Set_Outputs( "m_ptrRoot->ptrLeft's key",   'b',          tree.m_ptrRoot->ptrLeft->key ) )              { TestFail(); }
            else if ( !Set_Outputs( "m_ptrRoot->ptrLeft's data",  "banana",     tree.m_ptrRoot->ptrLeft->data ) )             { TestFail(); }
    
            // Check ROOT- > LEFT -> LEFT (a)
            else if ( tree.m_ptrRoot->ptrLeft->ptrLeft == nullptr ) { Set_Comments( "m_ptrRoot->ptrLeft->ptrLeft was nullptr" );     TestFail(); }
            else if ( !Set_Outputs( "m_ptrRoot->ptrLeft->ptrLeft's key",   'a',          tree.m_ptrRoot->ptrLeft->ptrLeft->key ) )              { TestFail(); }
            else if ( !Set_Outputs( "m_ptrRoot->ptrLeft->ptrLeft's data",  "apple",      tree.m_ptrRoot->ptrLeft->ptrLeft->data ) )             { TestFail(); }
    
            // Check ROOT -> RIGHT (f)
            else if ( tree.m_ptrRoot->ptrRight == nullptr ) { Set_Comments( "m_ptrRoot->ptrRight was nullptr" );    TestFail(); }
            else if ( !Set_Outputs( "m_ptrRoot->ptrRight's key",   'f',         tree.m_ptrRoot->ptrRight->key ) )             { TestFail(); }
            else if ( !Set_Outputs( "m_ptrRoot->ptrRight's data",  "figs",      tree.m_ptrRoot->ptrRight->data ) )            { TestFail(); }
    
            // Check ROOT -> RIGHT -> RIGHT (g)
            else if ( tree.m_ptrRoot->ptrRight->ptrRight == nullptr ) { Set_Comments( "m_ptrRoot->ptrRight->ptrRight was nullptr" );    TestFail(); }
            else if ( !Set_Outputs( "m_ptrRoot->ptrRight->ptrRight's key",   'g',         tree.m_ptrRoot->ptrRight->ptrRight->key ) )           { TestFail(); }
            else if ( !Set_Outputs( "m_ptrRoot->ptrRight->ptrRight's data",  "grapefruit",      tree.m_ptrRoot->ptrRight->ptrRight->data ) )    { TestFail(); }
    
            // Check ROOT- > LEFT -> RIGHT (c)
            else if ( tree.m_ptrRoot->ptrLeft->ptrRight == nullptr ) { Set_Comments( "m_ptrRoot->ptrLeft->ptrRight was nullptr" );     TestFail(); }
            else if ( !Set_Outputs( "m_ptrRoot->ptrLeft->ptrRight's key",   'c',          tree.m_ptrRoot->ptrLeft->ptrRight->key ) )              { TestFail(); }
            else if ( !Set_Outputs( "m_ptrRoot->ptrLeft->ptrRight's data",  "cranberry",      tree.m_ptrRoot->ptrLeft->ptrRight->data ) )             { TestFail(); }
    
            // Check ROOT -> RIGHT -> LEFT (e)
            else if ( tree.m_ptrRoot->ptrRight->ptrLeft == nullptr ) { Set_Comments( "m_ptrRoot->ptrRight->ptrLeft was nullptr" );    TestFail(); }
            else if ( !Set_Outputs( "m_ptrRoot->ptrRight->ptrLeft's key",   'e',         tree.m_ptrRoot->ptrRight->ptrLeft->key ) )           { TestFail(); }
            else if ( !Set_Outputs( "m_ptrRoot->ptrRight->ptrLeft's data",  "elderberry",      tree.m_ptrRoot->ptrRight->ptrLeft->data ) )    { TestFail(); }
            else                                                                                 { TestPass(); }
        } FinishTest();
    
        StartTest("4. Don't allow the same key to be entered more than once. Throw std::runtime_error if this happens.");  {
            Set_Comments( "Push( 'a', \"apple\" )" );
            Set_Comments( "Push( 'a', \"apple\" )" );
    
            BinarySearchTree<char, std::string> tree;
    
            tree.Push( 'a', "apple" );
    
            bool exceptionHappened = false;
            try
            {
                tree.Push( 'a', "aardvark" );
            }
            catch( const std::runtime_error& ex )
            {
                exceptionHappened = true;
                Utility::Logger::Out(ex.what());
            }
    
            Set_ExpectedOutput  ( "runtime_error Exception thrown", bool( true ) );
            Set_ActualOutput    ( "runtime_error Exception thrown", bool( exceptionHappened ) );
    
            if ( !exceptionHappened ) { TestFail(); }
            else { TestPass(); }
    
            
        } FinishTest();
        
        FinishTestSet();
        return TestResult();
    }
    
    int BinarySearchTreeTester::Test_Contains()
    {
        const std::string TESTFUNCTION(__func__);
        const std::string FUNCTION = std::string(__func__).replace(0, 5, "");
        StartTestSet(TESTFUNCTION, { FUNCTION, "RecursiveContains" });
        std::ostringstream oss;

        // PREREQUISITES IMPLEMENTED?
        StartTest("0a. Check if function " + FUNCTION + " is implemented");
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput(FUNCTION, std::string("Implemented"));

            try { BinarySearchTree<int, int> item; item.Contains('a'); }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what()); prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success(FUNCTION); }
            else { return PrereqTest_Abort(FUNCTION); }
        }

        StartTest("0b. Check if prereq function RecursiveContains is implemented");
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput("RecursiveContains", std::string("Implemented"));

            try { BinarySearchTree<char, std::string> tree; tree.RecursiveContains('a', tree.m_ptrRoot); }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what());      prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success("RecursiveContains"); }
            else { return PrereqTest_Abort("RecursiveContains"); }
        }

        // ADDITIONAL TESTS
        StartTest("1. Add some items to a tree; Check if Contains() locates an item.");  {
            std::string txtTree = "<pre>";
            txtTree += "  b     \n";
            txtTree += " / \\   \n";
            txtTree += "a   c   \n";
            txtTree += "</pre>";
            Set_Comments( txtTree );
    
            BinarySearchTree<char, std::string> tree;
            tree.m_ptrRoot = new BinarySearchTreeNode<char,std::string>( 'b', "banana" );
            tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<char,std::string>( 'a', "apple" );
            tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<char,std::string>( 'c', "cantalope" );
    
            if ( !Set_Outputs( "tree.Contains( 'c' )", true, tree.Contains( 'c' ) ) )           { TestFail(); }
            else                                                                                { TestPass(); }
    
            
        } FinishTest();
    
        StartTest("2. Add some items to a tree; Check if Contains() returns false for item not in tree.");  {
            std::string txtTree = "<pre>";
            txtTree += "  b     \n";
            txtTree += " / \\   \n";
            txtTree += "a   c   \n";
            txtTree += "</pre>";
            Set_Comments( txtTree );
    
            BinarySearchTree<char, std::string> tree;
            tree.m_ptrRoot = new BinarySearchTreeNode<char,std::string>( 'b', "banana" );
            tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<char,std::string>( 'a', "apple" );
            tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<char,std::string>( 'c', "cantalope" );
    
            if ( !Set_Outputs( "tree.Contains( 'z' )", false, tree.Contains( 'z' ) ) )      { TestFail(); }
            else                                                                            { TestPass(); }
        } FinishTest();
    
        FinishTestSet();
        return TestResult();
    }
    
    int BinarySearchTreeTester::Test_FindNode()
    {
        const std::string TESTFUNCTION(__func__);
        const std::string FUNCTION = std::string(__func__).replace(0, 5, "");
        StartTestSet(TESTFUNCTION, { FUNCTION, "RecursiveFindNode" });
        std::ostringstream oss;

        // PREREQUISITES IMPLEMENTED?
        StartTest("0a. Check if function " + FUNCTION + " is implemented");
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput(FUNCTION, std::string("Implemented"));

            try { BinarySearchTree<int, int> item; item.FindNode(10); }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what()); prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success(FUNCTION); }
            else { return PrereqTest_Abort(FUNCTION); }
        }

        StartTest("0b. Check if prereq function RecursiveFindNode is implemented");
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput("RecursiveFindNode", std::string("Implemented"));

            try { BinarySearchTree<char, std::string> tree; tree.RecursiveFindNode('a', tree.m_ptrRoot); }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what());      prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success("RecursiveFindNode"); }
            else { return PrereqTest_Abort("RecursiveFindNode"); }
        }

        // ADDITIONAL TESTS
        StartTest("Fill a tree, use FindNode() to find node 'g' in the tree.");  {
            std::string txtTree = "<pre>";
            txtTree += "       d                \n";
            txtTree += "    /     \\            \n";
            txtTree += "  b         f           \n";
            txtTree += " / \\       / \\        \n";
            txtTree += "a   c     e   g         \n";
            txtTree += "</pre>";
            Set_Comments( txtTree );
            Set_Comments( "FindNode( 'g' )" );
    
            BinarySearchTree<char, std::string> tree;
            tree.m_ptrRoot = new BinarySearchTreeNode<char, std::string>( 'd', "dates" );
    
            tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'b', "banana" );
            tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'a', "apple" );
            tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<char, std::string>( 'c', "carrot" );
    
            tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<char, std::string>( 'f', "figs" );
            tree.m_ptrRoot->ptrRight->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'e', "eggplant" );
            tree.m_ptrRoot->ptrRight->ptrRight = new BinarySearchTreeNode<char, std::string>( 'g', "grapefruit" );
    
            BinarySearchTreeNode<char, std::string>* findNode = tree.FindNode( 'g' );
    
            if ( findNode == nullptr ) { Set_Comments( "findNode was nullptr" );      TestFail(); }
            else if ( !Set_Outputs( "findNode's key",   'g',     findNode->key ) )           { TestFail(); }
            else if ( !Set_Outputs( "findNode's data",  "grapefruit", findNode->data ) )     { TestFail(); }
            else                                                                             { TestPass(); }
        } FinishTest();
    
        StartTest("Fill a tree, use FindNode() for item not in tree ('z'). Result should be nullptr.");  {
            std::string txtTree = "<pre>";
            txtTree += "       d                \n";
            txtTree += "    /     \\            \n";
            txtTree += "  b         f           \n";
            txtTree += " / \\       / \\        \n";
            txtTree += "a   c     e   g         \n";
            txtTree += "</pre>";
            Set_Comments( txtTree );
            Set_Comments( "FindNode( 'z' )" );
    
            BinarySearchTree<char, std::string> tree;
            tree.m_ptrRoot = new BinarySearchTreeNode<char, std::string>( 'd', "dates" );
    
            tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'b', "banana" );
            tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'a', "apple" );
            tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<char, std::string>( 'c', "carrot" );
    
            tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<char, std::string>( 'f', "figs" );
            tree.m_ptrRoot->ptrRight->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'e', "eggplant" );
            tree.m_ptrRoot->ptrRight->ptrRight = new BinarySearchTreeNode<char, std::string>( 'g', "grapefruit" );
    
            BinarySearchTreeNode<char, std::string>* findNode = tree.FindNode( 'z' );
    
            if ( findNode != nullptr ) { Set_Comments( "findNode was NOT nullptr; should return nullptr for item not in tree!" );      TestFail(); }
            else                                                                        { TestPass(); }
        } FinishTest();
    
        FinishTestSet();
        return TestResult();
    }
    
    int BinarySearchTreeTester::Test_GetInOrder()
    {
        const std::string TESTFUNCTION(__func__);
        const std::string FUNCTION = std::string(__func__).replace(0, 5, "");
        StartTestSet(TESTFUNCTION, { FUNCTION, "RecursiveGetInOrder" });
        std::ostringstream oss;

        // PREREQUISITES IMPLEMENTED?
        StartTest("0a. Check if function " + FUNCTION + " is implemented");
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput(FUNCTION, std::string("Implemented"));

            try { BinarySearchTree<int, int> item; item.GetInOrder(); }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what()); prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success(FUNCTION); }
            else { return PrereqTest_Abort(FUNCTION); }
        }

        StartTest("0b. Check if prereq function RecursiveGetInOrder is implemented");
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput("RecursiveGetInOrder", std::string("Implemented"));

            std::stringstream str;
            try { BinarySearchTree<char, std::string> tree; tree.RecursiveGetInOrder(tree.m_ptrRoot, str); }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what());      prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success("RecursiveGetInOrder"); }
            else { return PrereqTest_Abort("RecursiveGetInOrder"); }
        }

        // ADDITIONAL TESTS
        StartTest("Add things to a tree and then check the InOrder traversal."); {
            std::string txtTree = "<pre>";
            txtTree += "       d                \n";
            txtTree += "    /     \\            \n";
            txtTree += "  b         f           \n";
            txtTree += " / \\       / \\        \n";
            txtTree += "a   c     e   g         \n";
            txtTree += "</pre>";
            Set_Comments( txtTree );
            Set_Comments( "InOrder traversal: LEFT, SELF, RIGHT" );
    
            BinarySearchTree<char, std::string> tree;
            tree.m_ptrRoot = new BinarySearchTreeNode<char, std::string>( 'd', "dates" );
            tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'b', "banana" );
            tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'a', "apple" );
            tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<char, std::string>( 'c', "carrot" );
            tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<char, std::string>( 'f', "figs" );
            tree.m_ptrRoot->ptrRight->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'e', "eggplant" );
            tree.m_ptrRoot->ptrRight->ptrRight = new BinarySearchTreeNode<char, std::string>( 'g', "grapefruit" );
    
            if ( !Set_Outputs( "GetInOrder()", "abcdefg", tree.GetInOrder() ) )     { TestFail(); }
            else                                                                    { TestPass(); }
    
            
        } FinishTest();
    
        FinishTestSet();
        return TestResult();
    }
    
    int BinarySearchTreeTester::Test_GetPreOrder()
    {
        const std::string TESTFUNCTION(__func__);
        const std::string FUNCTION = std::string(__func__).replace(0, 5, "");
        StartTestSet(TESTFUNCTION, { FUNCTION, "RecursiveGetPreOrder" });
        std::ostringstream oss;

        // PREREQUISITES IMPLEMENTED?
        StartTest("0a. Check if function " + FUNCTION + " is implemented");
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput(FUNCTION, std::string("Implemented"));

            try { BinarySearchTree<int, int> item; item.GetPreOrder(); }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what()); prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success(FUNCTION); }
            else { return PrereqTest_Abort(FUNCTION); }
        }

        StartTest("0b. Check if prereq function RecursiveGetPreOrder is implemented");
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput("RecursiveGetPreOrder", std::string("Implemented"));
            
            std::stringstream str;
            try { BinarySearchTree<char, std::string> tree; tree.RecursiveGetPreOrder(tree.m_ptrRoot, str); }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what());      prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success("RecursiveGetPreOrder"); }
            else { return PrereqTest_Abort("RecursiveGetPreOrder"); }
        }

        // ADDITIONAL TESTS
        StartTest("Add things to a tree and then check the PreOrder traversal.");  {
            std::string txtTree = "<pre>";
            txtTree += "       d                \n";
            txtTree += "    /     \\            \n";
            txtTree += "  b         f           \n";
            txtTree += " / \\       / \\        \n";
            txtTree += "a   c     e   g         \n";
            txtTree += "</pre>";
            Set_Comments( txtTree );
            Set_Comments( "PreOrder traversal: SELF, LEFT, RIGHT" );
    
            BinarySearchTree<char, std::string> tree;
            tree.m_ptrRoot = new BinarySearchTreeNode<char, std::string>( 'd', "dates" );
            tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'b', "banana" );
            tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'a', "apple" );
            tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<char, std::string>( 'c', "carrot" );
            tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<char, std::string>( 'f', "figs" );
            tree.m_ptrRoot->ptrRight->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'e', "eggplant" );
            tree.m_ptrRoot->ptrRight->ptrRight = new BinarySearchTreeNode<char, std::string>( 'g', "grapefruit" );
    
            if ( !Set_Outputs( "GetPreOrder()", "dbacfeg", tree.GetPreOrder() ) )     { TestFail(); }
            else                                                                      { TestPass(); }
        } FinishTest();
    
        FinishTestSet();
        return TestResult();
    }
    
    int BinarySearchTreeTester::Test_GetPostOrder()
    {
        const std::string TESTFUNCTION(__func__);
        const std::string FUNCTION = std::string(__func__).replace(0, 5, "");
        StartTestSet(TESTFUNCTION, { FUNCTION, "RecursiveGetPostOrder" });
        std::ostringstream oss;

        // PREREQUISITES IMPLEMENTED?
        StartTest("0a. Check if function " + FUNCTION + " is implemented");
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput(FUNCTION, std::string("Implemented"));

            try { BinarySearchTree<int, int> item; item.GetInOrder(); }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what()); prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success(FUNCTION); }
            else { return PrereqTest_Abort(FUNCTION); }
        }

        StartTest("0b. Check if prereq function RecursiveGetPostOrder is implemented");
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput("RecursiveGetPostOrder", std::string("Implemented"));

            std::stringstream str;
            try { BinarySearchTree<char, std::string> tree; tree.RecursiveGetPostOrder(tree.m_ptrRoot, str); }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what());      prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success("RecursiveGetPostOrder"); }
            else { return PrereqTest_Abort("RecursiveGetPostOrder"); }
        }

        // ADDITIONAL TESTS
        StartTest("Add things to a tree and then check the PostOrder traversal.");  {
            std::string txtTree = "<pre>";
            txtTree += "       d                \n";
            txtTree += "    /     \\            \n";
            txtTree += "  b         f           \n";
            txtTree += " / \\       / \\        \n";
            txtTree += "a   c     e   g         \n";
            txtTree += "</pre>";
            Set_Comments( txtTree );
            Set_Comments( "PostOrder traversal: LEFT, RIGHT, SELF" );
    
            BinarySearchTree<char, std::string> tree;
            tree.m_ptrRoot = new BinarySearchTreeNode<char, std::string>( 'd', "dates" );
            tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'b', "banana" );
            tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'a', "apple" );
            tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<char, std::string>( 'c', "carrot" );
            tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<char, std::string>( 'f', "figs" );
            tree.m_ptrRoot->ptrRight->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'e', "eggplant" );
            tree.m_ptrRoot->ptrRight->ptrRight = new BinarySearchTreeNode<char, std::string>( 'g', "grapefruit" );
    
            if ( !Set_Outputs( "GetPostOrder()", "acbegfd", tree.GetPostOrder() ) )     { TestFail(); }
            else                                                                        { TestPass(); }        
        } FinishTest();
    
        FinishTestSet();
        return TestResult();
    }
    
    int BinarySearchTreeTester::Test_GetMaxKey()
    {
        const std::string TESTFUNCTION(__func__);
        const std::string FUNCTION = std::string(__func__).replace(0, 5, "");
        StartTestSet(TESTFUNCTION, { FUNCTION });
        std::ostringstream oss;

        // PREREQUISITES IMPLEMENTED?
        StartTest("0a. Check if function " + FUNCTION + " is implemented");
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput(FUNCTION, std::string("Implemented"));

            try { BinarySearchTree<int, int> item; item.GetMaxKey(); }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what()); prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success(FUNCTION); }
            else { return PrereqTest_Abort(FUNCTION); }
        }

        StartTest("0b. Check if prereq function RecursiveGetMax is implemented");
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput("RecursiveGetMax", std::string("Implemented"));

            std::stringstream str;
            try { BinarySearchTree<char, std::string> tree; tree.RecursiveGetMax(tree.m_ptrRoot); }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what());      prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success("RecursiveGetMax"); }
            else { return PrereqTest_Abort("RecursiveGetMax"); }
        }

        // ADDITIONAL TESTS
        StartTest("Create a tree and then get the max key.");  {
            std::string txtTree = "<pre>";
            txtTree += "       d                \n";
            txtTree += "    /     \\            \n";
            txtTree += "  b         f           \n";
            txtTree += " / \\       / \\        \n";
            txtTree += "a   c     e   g         \n";
            txtTree += "</pre>";
            Set_Comments( txtTree );
            Set_Comments( "Max key: Keep traversing RIGHT until there are no more nodes." );
    
            BinarySearchTree<char, std::string> tree;
            tree.m_ptrRoot = new BinarySearchTreeNode<char, std::string>( 'd', "dates" );
            tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'b', "banana" );
            tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'a', "apple" );
            tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<char, std::string>( 'c', "carrot" );
            tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<char, std::string>( 'f', "figs" );
            tree.m_ptrRoot->ptrRight->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'e', "eggplant" );
            tree.m_ptrRoot->ptrRight->ptrRight = new BinarySearchTreeNode<char, std::string>( 'g', "grapefruit" );
    
            if ( !Set_Outputs( "GetMaxKey()", 'g', tree.GetMaxKey() ) )     { TestFail(); }
            else                                                            { TestPass(); }
        } FinishTest();
    
        FinishTestSet();
        return TestResult();
    }
    
    int BinarySearchTreeTester::Test_GetMinKey()
    {
        const std::string TESTFUNCTION(__func__);
        const std::string FUNCTION = std::string(__func__).replace(0, 5, "");
        StartTestSet(TESTFUNCTION, { /* Prerequisite functions */ });
        std::ostringstream oss;

        // PREREQUISITES IMPLEMENTED?
        StartTest("0a. Check if function " + FUNCTION + " is implemented");
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput(FUNCTION, std::string("Implemented"));

            try { BinarySearchTreeNode<int, int> item; }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what()); prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success(FUNCTION); }
            else { return PrereqTest_Abort(FUNCTION); }
        }

        StartTest("0b. Check if prereq function RecursiveGetMin is implemented");
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput("RecursiveGetMin", std::string("Implemented"));

            std::stringstream str;
            try { BinarySearchTree<char, std::string> tree; tree.RecursiveGetMin(tree.m_ptrRoot); }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what());      prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success("RecursiveGetMin"); }
            else { return PrereqTest_Abort("RecursiveGetMin"); }
        }

        // ADDITIONAL TESTS
        StartTest("Create a tree and then get the min key.");  {
            std::string txtTree = "<pre>";
            txtTree += "       d                \n";
            txtTree += "    /     \\            \n";
            txtTree += "  b         f           \n";
            txtTree += " / \\       / \\        \n";
            txtTree += "a   c     e   g         \n";
            txtTree += "</pre>";
            Set_Comments( txtTree );
            Set_Comments( "Max key: Keep traversing LEFT until there are no more nodes." );
    
            BinarySearchTree<char, std::string> tree;
            tree.m_ptrRoot = new BinarySearchTreeNode<char, std::string>( 'd', "dates" );
            tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'b', "banana" );
            tree.m_ptrRoot->ptrLeft->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'a', "apple" );
            tree.m_ptrRoot->ptrLeft->ptrRight = new BinarySearchTreeNode<char, std::string>( 'c', "carrot" );
            tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<char, std::string>( 'f', "figs" );
            tree.m_ptrRoot->ptrRight->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'e', "eggplant" );
            tree.m_ptrRoot->ptrRight->ptrRight = new BinarySearchTreeNode<char, std::string>( 'g', "grapefruit" );
    
            if ( !Set_Outputs( "GetMinKey()", 'a', tree.GetMinKey() ) )     { TestFail(); }
            else                                                            { TestPass(); }
        } FinishTest();
    
        FinishTestSet();
        return TestResult();
    }
    
    int BinarySearchTreeTester::Test_GetCount()
    {
        const std::string TESTFUNCTION(__func__);
        const std::string FUNCTION = std::string(__func__).replace(0, 5, "");
        StartTestSet(TESTFUNCTION, { FUNCTION });
        std::ostringstream oss;

        // PREREQUISITES IMPLEMENTED?
        StartTest("0a. Check if function " + FUNCTION + " is implemented");
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput(FUNCTION, std::string("Implemented"));

            try { BinarySearchTreeNode<int, int> item; }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what()); prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success(FUNCTION); }
            else { return PrereqTest_Abort(FUNCTION); }
        }

        // ADDITIONAL TESTS
        StartTest("Create an empty tree and get the node count.");  {
            BinarySearchTree<char, std::string> tree;
            if ( !Set_Outputs( "GetCount()", 0, tree.GetCount() ) )             { TestFail(); }
            else                                                                { TestPass(); }
        } FinishTest();
    
        StartTest("Create a tree, set node count to 1, get the node count."); {
            BinarySearchTree<char, std::string> tree;
            tree.m_nodeCount = 1;
            if ( !Set_Outputs( "GetCount()", 1, tree.GetCount() ) )             { TestFail(); }
            else                                                                { TestPass(); }
        } FinishTest();
    
        FinishTestSet();
        return TestResult();
    }
    
    int BinarySearchTreeTester::Test_GetHeight()
    {
        const std::string TESTFUNCTION(__func__);
        const std::string FUNCTION = std::string(__func__).replace(0, 5, "");
        StartTestSet(TESTFUNCTION, { FUNCTION, "RecursiveGetHeight" });
        std::ostringstream oss;

        // PREREQUISITES IMPLEMENTED?
        StartTest("0a. Check if function " + FUNCTION + " is implemented");
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput(FUNCTION, std::string("Implemented"));

            try { BinarySearchTreeNode<int, int> item; }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what()); prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success(FUNCTION); }
            else { return PrereqTest_Abort(FUNCTION); }
        }

        StartTest("0b. Check if prereq function RecursiveGetHeight is implemented");
        {
            bool prereqsImplemented = true;
            Set_ExpectedOutput("RecursiveGetHeight", std::string("Implemented"));

            std::stringstream str;
            try { BinarySearchTree<char, std::string> tree; tree.RecursiveGetHeight(tree.m_ptrRoot); }
            catch (Exception::NotImplementedException& ex) { Set_Comments(ex.what());      prereqsImplemented = false; }
            catch (...) {}

            if (prereqsImplemented) { PrereqTest_Success("RecursiveGetHeight"); }
            else { return PrereqTest_Abort("RecursiveGetHeight"); }
        }


        // ADDITIONAL TESTS
        StartTest("Create tree with one node (root), check height.");  {
            BinarySearchTree<char, std::string> tree;
            tree.m_ptrRoot = new BinarySearchTreeNode<char, std::string>( 'a', "apple" );
            Set_Comments( "Height of a node is the # of EDGES from the node to the deepest leaf." );
    
            if ( !Set_Outputs( "GetHeight()", 0, tree.GetHeight() ) )     { TestFail(); }
            else                                                          { TestPass(); }
        } FinishTest();
    
        { StartTest("Create tree with several nodes, check height.");
            BinarySearchTree<char, std::string> tree;
            tree.m_ptrRoot = new BinarySearchTreeNode<char, std::string>( 'd', "dates" );
            tree.m_ptrRoot->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'b', "banana" );
            tree.m_ptrRoot->ptrRight = new BinarySearchTreeNode<char, std::string>( 'g', "grapefruit" );
            tree.m_ptrRoot->ptrRight->ptrLeft = new BinarySearchTreeNode<char, std::string>( 'e', "elderberry" );
            tree.m_ptrRoot->ptrRight->ptrLeft->ptrRight = new BinarySearchTreeNode<char, std::string>( 'f', "elderberry" );
    
            std::string txtTree = "<pre>";
            txtTree += "       d                \n";
            txtTree += "    /     \\            \n";
            txtTree += "  b         g           \n";
            txtTree += "           /            \n";
            txtTree += "          e             \n";
            txtTree += "           \\           \n";
            txtTree += "            f           \n";
            txtTree += "</pre>";
            Set_Comments( txtTree );
            Set_Comments( "Height of a node is the # of EDGES from the node to the deepest leaf." );
    
            if ( !Set_Outputs( "GetHeight()", 3, tree.GetHeight() ) )     { TestFail(); }
            else                                                          { TestPass(); }        
        } FinishTest();
    
        FinishTestSet();
        return TestResult();
    }
    
    
    //int BinarySearchTreeTester::Test_FindParentOfNode()
    //{
    //    Utility::Logger::OutHighlight( "TEST SET BEGIN", "BinarySearchTreeTester::Test_FindParentOfNode", 3 );
    //    StartTestSet( "Test_FindParentOfNode", { "RecursiveFindNode", "Push" } );
    //    std::ostringstream oss;
    //
    //    { /* TEST BEGIN ************************************************************/
    //        std::string functionName = "BinarySearchTree::FindParentOfNode";
    //        StartTest( "1. Check if " + functionName + " has been implemented yet..." );
    //
    //        bool prereqsImplemented = true;
    //        Set_ExpectedOutput( functionName, std::string( "Implemented" ) );
    //        try
    //        {
    //            BinarySearchTree<char, std::string> tree;
    //            tree.FindParentOfNode( 'a' );
    //        }
    //        catch ( Exception::NotImplementedException& ex )
    //        {
    //            Set_Comments( ex.what() );
    //            prereqsImplemented = false;
    //        }
    //
    //        if ( prereqsImplemented )
    //        {
    //            Set_ActualOutput( functionName, std::string( "Implemented" ) );
    //            TestPass();
    //            FinishTest();
    //        }
    //        else
    //        {
    //            Set_ActualOutput( functionName, std::string( "Not implemented" ) );
    //            TestFail();
    //            FinishTest();
    //            FinishTestSet();
    //            return TestResult();
    //        }
    //    } /* TEST END **************************************************************/
    //
    //    { /* TEST BEGIN ************************************************************/
    //        StartTest( "Fill a tree, use FindNode() to find node in the tree." );
    //
    //        Set_Comments( "Push( 'd', \"dates\" )" );
    //        Set_Comments( "Push( 'b', \"banana\" )" );
    //        Set_Comments( "Push( 'f', \"figs\" )" );
    //        Set_Comments( "Push( 'a', \"apple\" )" );
    //        Set_Comments( "Push( 'c', \"cranberry\" )" );
    //        Set_Comments( "Push( 'e', \"elderberry\" )" );
    //        Set_Comments( "Push( 'g', \"grapefruit\" )" );
    //
    //        std::string txtTree = "<pre>";
    //        txtTree += "       d                \n";
    //        txtTree += "    /     \\            \n";
    //        txtTree += "  b         f           \n";
    //        txtTree += " / \\       / \\        \n";
    //        txtTree += "a   c     e   g         \n";
    //        txtTree += "</pre>";
    //        Set_Comments( txtTree );
    //        Set_Comments( "FindNode( 'g' )" );
    //
    //        BinarySearchTree<char, std::string> tree;
    //        tree.Push( 'd', "dates" );
    //        tree.Push( 'b', "banana" );
    //        tree.Push( 'f', "figs" );
    //        tree.Push( 'a', "apple" );
    //        tree.Push( 'c', "cranberry" );
    //        tree.Push( 'e', "elderberry" );
    //        tree.Push( 'g', "grapefruit" );
    //
    //        char expectedKey = 'g';
    //        std::string expectedData = "grapefruit";
    //
    //        Set_ExpectedOutput  ( "found node address",  std::string("NOT nullptr") );
    //        Set_ExpectedOutput  ( "found node key",  char(expectedKey) );
    //        Set_ExpectedOutput  ( "found node data", std::string(expectedData) );
    //
    //        char actualKey = '-';
    //        std::string actualData = "-";
    //
    //        Node<char, std::string>* findNode = tree.FindNode( 'g' );
    //
    //        if ( findNode == nullptr )
    //        {
    //            Set_ActualOutput  ( "found node address",  std::string("nullptr") );
    //        }
    //        else
    //        {
    //            actualKey = findNode->key;
    //            actualData = findNode->data;
    //
    //            Set_ActualOutput    ( "found node key", actualKey );
    //            Set_ActualOutput    ( "found node data", actualData );
    //        }
    //
    //        if ( findNode == nullptr )
    //        {
    //            TestFail();
    //            Set_Comments( "FindNode returned nullptr!" );
    //        }
    //        else if ( actualKey != expectedKey )
    //        {
    //            TestFail();
    //            Set_Comments( "FindNode key was wrong!" );
    //        }
    //        else if ( actualData != expectedData )
    //        {
    //            TestFail();
    //            Set_Comments( "FindNode data was wrong!" );
    //        }
    //        else
    //        {
    //            TestPass();
    //        }
    //
    //        FinishTest();
    //    } /* TEST END **************************************************************/
    //
    //    { /* TEST BEGIN ************************************************************/
    //        StartTest( "Fill a tree, use FindNode() for item not in tree. Result should be nullptr." );
    //
    //        Set_Comments( "Push( 'd', \"dates\" )" );
    //        Set_Comments( "Push( 'b', \"banana\" )" );
    //        Set_Comments( "Push( 'f', \"figs\" )" );
    //        Set_Comments( "Push( 'a', \"apple\" )" );
    //        Set_Comments( "Push( 'c', \"cranberry\" )" );
    //        Set_Comments( "Push( 'e', \"elderberry\" )" );
    //        Set_Comments( "Push( 'g', \"grapefruit\" )" );
    //
    //        std::string txtTree = "<pre>";
    //        txtTree += "       d                \n";
    //        txtTree += "    /     \\            \n";
    //        txtTree += "  b         f           \n";
    //        txtTree += " / \\       / \\        \n";
    //        txtTree += "a   c     e   g         \n";
    //        txtTree += "</pre>";
    //        Set_Comments( txtTree );
    //        Set_Comments( "FindNode( 'z' )" );
    //
    //        BinarySearchTree<char, std::string> tree;
    //        tree.Push( 'd', "dates" );
    //        tree.Push( 'b', "banana" );
    //        tree.Push( 'f', "figs" );
    //        tree.Push( 'a', "apple" );
    //        tree.Push( 'c', "cranberry" );
    //        tree.Push( 'e', "elderberry" );
    //        tree.Push( 'g', "grapefruit" );
    //
    //        Set_ExpectedOutput  ( "found node address",  std::string("nullptr") );
    //
    //        Node<char, std::string>* findNode = tree.FindNode( 'z' );
    //
    //        if ( findNode == nullptr )
    //        {
    //            Set_ActualOutput  ( "found node address",  std::string("nullptr") );
    //        }
    //        else
    //        {
    //            oss << findNode;
    //            Set_ActualOutput    ( "found node address", oss.str() );
    //        }
    //
    //        if ( findNode != nullptr )
    //        {
    //            TestFail();
    //            Set_Comments( "FindNode was supposd to return nullptr!" );
    //        }
    //        else
    //        {
    //            TestPass();
    //        }
    //
    //        FinishTest();
    //    } /* TEST END **************************************************************/
    //
    //    FinishTestSet();
    //    return TestResult();
    //}
    
    
    
    //int BinarySearchTreeTester::Test_Delete()
    //{
    //    StartTestSet( "Test_Delete", { } );
    //    std::ostringstream oss;
    //
    //    { /* TEST BEGIN ************************************************************/
    //        StartTest( "DESCRIPTION" );
    //
    //        int expectedResult = 0;
    //        int actualResult = 0;
    //
    //        Set_ExpectedOutput  ( "ITEM", expectedResult );
    //        Set_ActualOutput    ( "ITEM", actualResult );
    //
    //        if ( actualResult != expectedResult )
    //        {
    //            TestFail();
    //        }
    //        else
    //        {
    //            TestPass();
    //        }
    //
    //        FinishTest();
    //    } /* TEST END **************************************************************/
    //
    //    FinishTestSet();
    //    return TestResult();
    //}

} // end of namespace