#ifndef _STRUCTURE_EMPTY_EXCEPTION
#define _STRUCTURE_EMPTY_EXCEPTION

// C++ Library includes
#include <stdexcept>
#include <string>

namespace Exception
{

//! EXCEPTION for when a structure is empty
class StructureEmptyException : public std::runtime_error
{
    public:
    StructureEmptyException( std::string functionName, std::string message )
        : std::runtime_error( "[" + functionName + "] " + message  ) { ; }
};

} // End of namespace

#endif
