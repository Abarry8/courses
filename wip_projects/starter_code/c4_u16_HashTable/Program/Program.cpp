#include "Program.h"

#include "../Utilities/Menu.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <functional>
#include <string>

Program::Program()
//	: m_employees(101) // TODO: Uncomment me out once using HashTable.
{
}

void Program::SetDataPath(std::string path)
{
	m_dataPath = path;
	std::cout << "Program data should be located at \"" << m_dataPath << "\"..." << std::endl;
}

void Program::Setup()
{
	m_dataGen.Setup(m_dataPath);
	std::hash<std::string> stringHash;

	std::cout << std::left;

	// Create employees
	for (int i = 0; i < 100; i++)
	{
		std::string fname = m_dataGen.GetName();
		Employee newEmployee;
		newEmployee.m_id = 1337 + i*3;
		newEmployee.m_email = m_dataGen.GetEmail();
		newEmployee.m_name = fname + " " + m_dataGen.GetLetter() + ".";
		newEmployee.m_passwordHash = stringHash(fname);

		std::cout << std::setw(30) << newEmployee.m_name << std::setw( 10 );

		m_employees[ newEmployee.m_id ] = newEmployee; // TODO: Replace me
	}
}

void Program::Cleanup()
{
}

void Program::Run()
{
	Setup();

	std::cout << std::endl << std::string(80, '-') << std::endl;
	std::cout << "LOGIN" << std::endl << std::endl;
	std::cout << "Enter employee ID: ";
	size_t id;
	std::cin >> id;

	Employee employee;

	try
	{
		employee = m_employees.at(id); // TODO: Replace me
	}
	catch ( ... )  // TODO: Remove me out once using HashTable.
//	catch (const Exception::ItemNotFoundException& ex) // TODO: Uncomment me out once using HashTable.
	{
//		std::cout << "ERROR: " << ex.what() << std::endl; // TODO: Uncomment me out once using HashTable.
		return;
	}

	std::cout << "EMAIL: " << employee.m_email << std::endl;

	std::string password;
	std::cout << "Enter password: ";
	std::cin.ignore();
	getline(std::cin, password);

	std::hash<std::string> stringHash;
	size_t hash;

	hash = stringHash(password);
	std::cout << "PASSWORD: \"" << password << "\" \t Hash: [" << hash << "]" << std::endl;

	password = "Samuel";
	hash = stringHash(password);
	std::cout << "PASSWORD: \"" << password << "\" \t Hash: [" << hash << "]" << std::endl;


	// Check to see if password hashes match
	if (hash == employee.m_passwordHash)
	{
		std::cout << std::endl << "LOGIN SUCCESSFUL" << std::endl;
	}
	else
	{
		std::cout << std::endl << "LOGIN FAILED" << std::endl;
		return;
	}

	std::cout << std::endl << "EMPLOYEE INFO:" << std::endl;
	employee.Display();

	std::cout << std::endl;
}
