#ifndef _PROGRAM
#define _PROGRAM

#include "../DataStructure/HashTable/HashTable.h"
#include "DataGen.h"
#include "Employee.h"

#include <string>
#include <fstream>
#include <map>

class Program
{
public:
	Program();
	void Run();
	void SetDataPath(std::string path);

private:
	void Setup();
	void Cleanup();

	std::string m_dataPath;
	std::ofstream m_log;
	DataGen m_dataGen;

	//DataStructure::HashTable<Employee> m_employees;
	std::map<int, Employee> m_employees; // TODO: Replace me
};

#endif
