#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "Helper.hpp"

using namespace std;

int main( int argCount, char* args[] )
{
  if ( argCount < 2 )
  {
    cout << "Expecting: " << args[0] << " filename" << endl;
    return 1;
  }
  
  string inpath = string( args[1] );
  string outpath = inpath;
  
  system( string( "cp " + inpath + " " + inpath + ".backup" ).c_str() );
  
  ifstream infile( inpath );
  
  vector<string> lines;
  
  string line;
  while ( getline( infile, line ) )
  {
    cout << "LINE: " << line << endl;
    line = Helper::Replace( line, "~\\\\", "" );
    line = Helper::Replace( line, "\\newpage", "" );
      line = Helper::Replace( line, "\\item", "- " );
      line = Helper::Replace( line, "\\begin{itemize}", "" );
      line = Helper::Replace( line, "\\end{itemize}", "" );
      line = Helper::Replace( line, "\\begin{lstlisting}[style=code]", "#+BEGIN_SRC cpp :class cpp" );
      line = Helper::Replace( line, "\\end{lstlisting}", "#+END_SRC" );
      line = Helper::Replace( line, "``", "\"" );
      line = Helper::Replace( line, "''", "\"" );
    // Do replacements
    // This isn't perfect, like if multiple tags are on the same line.
    if ( Helper::Contains( line, "\\textit" ) )
    {
      line = Helper::Replace( line, "{", "/" );
      line = Helper::Replace( line, "}", "/" );
      line = Helper::Replace( line, "\\textit", "" );
    }
    
    if ( Helper::Contains( line, "\\textbf" ) )
    {
      line = Helper::Replace( line, "{", "*" );
      line = Helper::Replace( line, "}", "*" );
      line = Helper::Replace( line, "\\textbf", "" );
    }
    
    if ( Helper::Contains( line, "\\texttt" ) )
    {
      line = Helper::Replace( line, "{", "=" );
      line = Helper::Replace( line, "}", "=" );
      line = Helper::Replace( line, "\\texttt", "" );
    }
    
    if ( Helper::Contains( line, "\\paragraph" ) )
    {
      line = Helper::Replace( line, "{", "- " );
      line = Helper::Replace( line, "}", " :: " );
      line = Helper::Replace( line, "\\paragraph", "" );
    }
    
    if ( Helper::Contains( line, "\\section" ) )
    {
      line = Helper::Replace( line, "{", "** " );
      line = Helper::Replace( line, "}", "" );
      line = Helper::Replace( line, "\\section", "" );
    }
    
    if ( Helper::Contains( line, "\\subsection" ) )
    {
      line = Helper::Replace( line, "{", "*** " );
      line = Helper::Replace( line, "}", "" );
      line = Helper::Replace( line, "\\subsection", "" );
    }
    
    cout << "LINEAFTER: " << line << endl << endl;
    lines.push_back( line );
  }
  
  infile.close();

  ofstream outfile( outpath );
  for ( auto& line : lines )
  {
    outfile << line << endl;
  }
  
  return 0;
}
