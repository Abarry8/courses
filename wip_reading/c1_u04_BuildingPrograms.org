# -*- mode: org -*-

#+TITLE: Building programs
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>

[[file:images/c1_u04_InputOutput_terminal-hi.png]]

-----
[[file:images/c1_u04_BuildingPrograms_drawsteps.jpg]]

Building a program is like drawing a picture. You don't just draw the final image as the first step - you usually sketch a base
to figure out what you're making, clean up the details, add inked lines over the image, and add coloring in steps.

It's the same with programs - You're not going to be building the /final program/ as step 1.

-----

* Planning out your program

** Listing features

Before starting on your program it's important to take note of *what the requirements are* - what are all the things
the program needs to do? It can be helpful to make a *list* to keep track of all the features, and to try to think
of what kind of *data* or *variables* might need to be used in the program. But, it's also OK to not be able to
identify /everything/ at this step - you're still learning!

#+BEGIN_QUOTE
PROGRAM REQUIREMENTS: Make a program that displays a recipe. It should ask the user how many batchces
the user wants to make and adjust the ingredient amounts based on that input.

DRAFT FEATURE LIST:
- Display recipe (ingredients, instructions)
- Ask user for how many batches (batches variable?)
- Adjust ingredients based on batches (variables for each ingredient?)
#+END_QUOTE

** Investigating example output

You might also refer to any *example output*, or try to come up with your own *example output* if none is given.
This way, you can visualize what the finished program will look like.

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
RECIPENAME

How many batches would you like to make? 2

INGREDIENTS
 - 2 tbsp ingredientA
 - 3 cups ingredientB
 - 1.5 tsps ingredientC

DIRECTIONS
 Step 1. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
 incididunt ut labore et dolore magna aliqua.

 Step 2. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
 aliquip ex ea commodoconsequat.

 Step 3. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
 eu fugiat nulla pariatur.
#+END_SRC

(Note that the text is *Lorem Ipsum*, a common placeholder text used to show what a product will look like with
enough text filled in. See also: https://en.wikipedia.org/wiki/Lorem_ipsum)

** Pseudocode or flowcharting

It might be useful to write out *pseudocode* or a *flowchart* for the steps. You can begin with a very general
list of steps...

[[file:images/c1_u04_BuildingPrograms_flowchart1.png]]

... And then break them down into even more detailed steps ...

[[file:images/c1_u04_BuildingPrograms_flowchart2.png]]

But at least having the simplified flowchart version will help give you a "roadmap" to follow as you work on your program.

-----

* Programming your program

[[file:images/c1_u04_BuildingPrograms_badcode.png]]

Next it's on to actually writing some code. Here are some suggestions for minimizing headaches.

** Adding comments as an outline

Put your general step-by-step list as *comments* in your program code - that way, you can put the related code beneath each "section".

** Keep a "quick reference sheet" handy!

While you're still learning, it can be useful to have a "quick reference" sheet to reference how the programming language looks
and works. You might compile a little "cookbook" of how-tos for the language you're using.

| Description                                            | C++                                       |
|--------------------------------------------------------+--------------------------------------------------|
| How to declare a float variable named =amount_vanilla= | =float amount_vanilla;=                          |
| How to assign a value to the =amount_vanilla= variable | =amount_vanilla = 1.5;=                          |
| How to print "Hello!"                                  | =cout << "Hello!" << endl;=                      |
| How to print "Vanilla:" and then =amount_vanilla=.     | =cout << "Vanilla: " << amount_vanilla << endl;= |

** DON'T IMPLEMENT THE ENTIRE PROGRAM IN ONE GO!

*DO NOT implement the ENTIRE PROGRAM in one go!* - Implement one feature at a time, such as "Ask the user to enter batches".
After implementing ONE FEATURE, then *RUN AND TEST* the program to make sure it doesn't have syntax errors, runs as intended, and does not crash.

Remember to think of your program as if you were writing a paper or drawing a picture - there's a *draft* phase. You take care of
certain steps, go back and re-evaluate, and improve in an iterative manner.

-----

* Diagnosing bugs and errors

[[file:images/c1_u04_BuildingPrograms_nobug.png]]

If you do run into bugs, here are some tips for dealing with them:

**  The "comment-out" technique
  - Comment out a big chunk of your code, or maybe ALL of it! Try to run the program and see if you still get the same errors.
  - If you don't get errors, slowly *uncomment out* a few lines of code at a time. Run the program again. Fix any errors that pop up with this one small region.
  - Repeat until you've uncommented out the entire program.

** The "show your variables" technique 
     Display your variable values to make sure they have the information you want - Use =print()= (Python/Lua) or =cout= (C++) to display
     your variables as a "temporary debug" feature as you go. You can verify that your variables contain what you think
     they're supposed to, and it will help you find bugs if something isn't being calculated correctly.

** The "rubber duck debugging" technique 
  This method is where you talk to an inanimate object (such as a rubber duck). Explain to your
  "rubber duck" what you're trying to do in the program and what the steps are. Sometimes, just
  explaining your code will help you figure out what's going wrong!

** The "step away from the computer" technique 
  Sometimes if you feel like you're banging your head against a wall you just *need to step away*.
  Your focus isn't going to come back if you keep trying to force it. Sometimes taking a 5 minute
  break or going off to do something else *really helps*. It gives your brain a chance to cool down
  and you might even think of a solution while away from the computer!

-----

* Testing and polishing your program

[[file:images/c1_u04_BuildingPrograms_tests.png]]

** Test cases for your program

Once your program is up and running you still need to do some *testing* to make sure it works as intended.
It's useful to make a list of behaviors or expected results, and then running your program to test against it.
This is especially useful if your program does any calculations. You can do the calculations with a calculator outside
of your program and note down what results /should/ be, and use this to compare with the program's /actual output/.

| Test | Input (batches) | Results (ingredients)     |
|------+-----------------+---------------------------|
|    1 | =batches=1=     | vanilla amount is 1.5 tsp |
|    2 | =batches=2=     | vanilla amount is 3 tsp   |
|    3 | =batches=0.5=   | vanilla amout is 0.75 tsp |

** Polishing up the UI

Make sure your user interface is also friendly and easy to use. Some general good rules are:

- *Prompt your user before getting input* - In other words, use an Output statement before asking the user to Input anything
  so they know that the program is waiting for them to enter something. (In Python, the =input()= function displays a question
  and gets the input in the same step, but for Lua and C++ you'll have to do a =print()= or =cout= /before/ your =io.read()= or =cin=).
- *Space out sections of the program* - Make sure you have enough *vertical space* (new lines) in the program to make different
  regions visually distinct. For example: /Section 1 - "Recipe name", section 2 - "ingredient list", section 3 - "directions"/.

*Bad UI: Too smushed together*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
RECIPENAME
How many batches would you like to make? 2
INGREDIENTS
2 tbsp ingredientA
3 cups ingredientB
1.5 tsps ingredientC
DIRECTIONS
Step 1. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua.
Step 2. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
aliquip ex ea commodoconsequat.
Step 3. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
eu fugiat nulla pariatur.
#+END_SRC

*Good UI: Easy to read*

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
RECIPENAME

How many batches would you like to make? 2

INGREDIENTS
 - 2 tbsp ingredientA
 - 3 cups ingredientB
 - 1.5 tsps ingredientC

DIRECTIONS
 Step 1. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
 incididunt ut labore et dolore magna aliqua.

 Step 2. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
 aliquip ex ea commodoconsequat.

 Step 3. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
 eu fugiat nulla pariatur.
#+END_SRC


-----
