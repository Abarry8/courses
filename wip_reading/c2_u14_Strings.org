# -*- mode: org -*-

#+TITLE: The C++ String Library
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>
-----

[[file:images/c2_u14_Strings_image.png]]


A *string* is a special data type that really is
just an *array of char* variables. A string has a lot
going on behind-the-scenes, and it also has a set of
*functions* you can use to do some common operations
on a string - finding text, getting a letter at some position,
and more.

Note that everything with strings is *case-sensitive*.
A computer considers the letter ='a'= and ='A'=
different, since they are represented by different number codes.
Keep that in mind for each of the string's functions.


#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** Strings as arrays

When we declare a string like this:

#+BEGIN_SRC cpp :class cpp
string str = "pizza";
#+END_SRC

what we have behind-the-scenes is an array like this:

#+ATTR_HTML: :border 2 :rules all :frame border :class col-7
| Value: | ='p'= | ='i'= | ='z'= | ='z'= | ='a'= |
| Index: |     0 |     1 |     2 |     3 |     4 |


Declaring a string actually gives us an *array of *char* variables*.
We can access the string as a whole by using the string variable's name
(=str=), or access one =char= at a time by treating
=str= like an array.

*** Subscript operator - get one char with [ ]

We can access each letter of the string directly with the
*subscript operator*, just like an array:

#+BEGIN_SRC cpp :class cpp
cout << str[0] << endl; // Outputs p
cout << str[1] << endl; // Outputs i
cout << str[2] << endl; // Outputs z
cout << str[3] << endl; // Outputs z
cout << str[4] << endl; // Outputs a
#+END_SRC

Because we can act on a string like an array, this means we
can also use a variable to access an arbitrary *index*
of a character in the array...

#+BEGIN_SRC cpp :class cpp
int i;
cout << "Get which letter? ";
cin >> i;
cout << str[i];
#+END_SRC

Or even iterate over the string with a *for loop*...

#+BEGIN_SRC cpp :class cpp
for ( int i = 0; i < str.size(); i++ )
{
    cout << i << " = " << str[i] << endl;
}
#+END_SRC

Additionally, strings have a set of *functions* that
we can use to manipulate, search, and otherwise work with them.


#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** String functionality

*** Size of the string

#+BEGIN_QUOTE

#+ATTR_HTML: :class nonum-title
*=size_t size() const;=*

The string's =size()= function will return
the size of the string - how many characters are
stored in the string.
The =size_t= data type is just a type of
integer - an /unsigned/ integer, because
sizes cannot be negative amounts.

Documentation: https://www.cplusplus.com/reference/string/string/size/
#+END_QUOTE


- Example: Outputting a string's length ::

   Let's write a little program that asks the user to enter
   some text, and then outputs the length of the string:

#+BEGIN_SRC cpp :class cpp
string text;
cout << "Enter some text: ";
getline( cin, text );
cout << "That string is " << text.size()
    << " characters long!" << endl;
#+END_SRC

When the user enters a line of text, it will count all
characters (including spaces) in the string, so the text
="cats dogs"= would be 9 characters long.

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Enter some text: cats dogs
That string is 9 characters long!
#+END_SRC

[[file:images/c2_u14_Strings_catsdogs.png]]




- Example: Counting z's ::
  If we wanted to use a *for loop* to iterate over
  all the letters of an array, we could! Perhaps we want to
  count the amount of z's that show up:

#+BEGIN_SRC cpp :class cpp
string text;
int zCount = 0;

cout << "Enter some text: ";
getline( cin, text );

// Iterate from i=0 to the size of the string (not-inclusive)
for ( unsigned int i = 0; i < text.size(); i++ )
{
    // If this letter is a lower-case z or upper-case Z
    if ( text[i] == 'z' || text[i] == 'Z' )
    {
        // Add one to z count
        zCount++;
    }
}

// Display the result
cout << "There were " << zCount
    << " z(s) in the string!" << endl;
#+END_SRC

#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Enter some text: The wizard fought a zombie lizard
There were 3 z(s) in the string!
#+END_SRC

[[file:images/c2_u14_Strings_wizard.png]]




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** Concatenating strings with +

We can use the =+= operator to add strings together as well. This is called *concatenation*.

Let's say we have two strings we want to combine - =favoriteColor= and =petName=,
we can use the concatenation operator =+= to build a new string, =superSecurePassword=.


#+BEGIN_SRC cpp :class cpp
string favoriteColor = "purple";
string petName = "Luna";
string superSecurePassword = favoriteColor + petName;
cout << superSecurePassword << endl; // = purpleLuna
#+END_SRC

[[file:images/c2_u14_Strings_luna.png]]

We can also add onto strings by using the =+== operator.
Let's say you're building a string over time in a program,
and want to *append* parts separately.

#+BEGIN_SRC cpp :class cpp
string pizzaToppings = "";

// Later on...
pizzaToppings += "Buffalo sauce, ";

// Later on...
pizzaToppings += "Cheese, ";

// Later on...
pizzaToppings += "Pineapple";

// Later on...
cout << pizzaToppings << endl;
#+END_SRC

At the end of the program, the value of =pizzaToppings= would be:

="Buffalo sauce, Cheese, Pineapple"=





#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** Finding text with find()

#+BEGIN_QUOTE

#+ATTR_HTML: :class nonum-title
*=size_t find (const string& str, size_t pos = 0 ) const;=*

The find function can be used to look for a substring in a bigger string.
If the substring is found, its position is returned.
Otherwise, the value of =string::npos= is found.

When a starting =pos= is included, it only begins the search at that position.
If left off, it defaults to 0 (the start of the string).

Documentation: https://www.cplusplus.com/reference/string/string/find/
#+END_QUOTE

So when we want to search a string for some text, we can call it like =bigString.find( findMeString )=,
and that function call will return an unsigned integer: the location of the =findMeString=
within =bigString=, or the value of =string::npos= when it is not found.


#+BEGIN_SRC cpp :class cpp
string str = "this was written during the 2021 winter storm make it stop please.";

string findMe = "winter";

size_t position = str.find( findMe );

cout << "The text \"" << findMe
    << "\" was found at position " << position << endl;
#+END_SRC


#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
The text "winter" was found at position 33
#+END_SRC



#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** Finding substrings with substr()

#+BEGIN_QUOTE

#+ATTR_HTML: :class nonum-title
*=string substr (size_t pos = 0, size_t len = npos) const;=*

Returns a string within the string, starting at the position =pos= provided, and with a length of =len=.

Documentation:  https://www.cplusplus.com/reference/string/string/substr/ 
#+END_QUOTE


With the =substr()= function, we can pull part of a string out, using a starting point and a length.

#+BEGIN_SRC cpp :class cpp
string text = "Name: Bob";

int start = 6;
int length = 3;

string name = text.substr( start, length );

cout << "Extracted \"" << name << "\"." << endl;

#+END_SRC


#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Extracted "Bob".
#+END_SRC

[[file:images/c2_u14_Strings_bob.png]]







#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** Comparing text with compare()

#+BEGIN_QUOTE

#+ATTR_HTML: :class nonum-title
*=int compare (const string& str) const;=*

This compares the string with =str= and returns an integer:
-    $0$   is returned if both strings are the same.
-    $< 1$ (a negative number) is returned if the caller string is "less-than" the =str= string.
-    $> 1$ (a positive number) is returned if the caller string is "greater-than" the =str= string.

One character is "less than" another if it comes *before*,
and it is "greater than" if it comes *after*, alphabetically.

(Remember that lower-case and upper-case letters are considered separate, so comparing ='a'= to ='A'= would actually return a positive number.)

Documentation:  https://www.cplusplus.com/reference/string/string/compare/
#+END_QUOTE

#+BEGIN_SRC cpp :class cpp
string first;
string second;

cout << "Enter first string: ";
cin >> first;

cout << "Enter second string: ";
cin >> second;

int order = first.compare( second );

cout << endl << "Result: " << order << endl;
#+END_SRC


#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Enter first string: apple
Enter second string: banana

Result: -1
#+END_SRC

[[file:images/c2_u14_Strings_compare.png]]






#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** Inserting text into a string with insert()




#+BEGIN_QUOTE

#+ATTR_HTML: :class nonum-title
*= string& insert (size_t pos, const string& str);=*

This function will take the calling string and modify it by inserting the string =str= at the position =pos=.

Documentation:  https://www.cplusplus.com/reference/string/string/insert/
#+END_QUOTE

#+BEGIN_SRC cpp :class cpp
string text = "helloworld";

cout << "Original text: " << text << endl;

int start;
string insertText;

cout << "Enter text to insert: ";
getline( cin, insertText );

cout << "Enter position to insert: ";
cin >> start;

text = text.insert( start, insertText );

cout << endl << "String is now: " << text << endl;
#+END_SRC


#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Original text: helloworld
Enter text to insert: -to the-
Enter position to insert: 5

String is now: hello-to the-world
#+END_SRC




#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** Erasing a chunk of text with erase()


#+BEGIN_QUOTE

#+ATTR_HTML: :class nonum-title
*=string& erase (size_t pos = 0, size_t len = npos);=*

This function will return a string with a portion erased, starting at position =pos= and pulling out a length of =len=.

Documentation:  https://www.cplusplus.com/reference/string/string/erase/
#+END_QUOTE

#+BEGIN_SRC cpp :class cpp
string text = "helloworld";

cout << "Original text: " << text << endl;

int start;
int length;

cout << "Enter position to begin erasing: ";
cin >> start;

cout << "Enter length of text to erase: ";
cin >> length;

text = text.erase( start, length );
cout << endl << "String is now: " << text << endl;
#+END_SRC


#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Original text: helloworld
Enter position to begin erasing: 2
Enter length of text to erase: 5

String is now: herld
#+END_SRC








#+ATTR_HTML: :class extra-space-invisible
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
*** Replacing a region of text with replace()

#+BEGIN_QUOTE

#+ATTR_HTML: :class nonum-title
*=string& replace (size_t pos,  size_t len,  const string& str);=*

This function is similar to erase, except it will insert the string =str= in place of the erased text.

Documentation:  https://www.cplusplus.com/reference/string/string/replace/
#+END_QUOTE



#+BEGIN_SRC cpp :class cpp
string text = "helloworld";

cout << "Original text: " << text << endl;

int start;
int length;
string replaceWith;

cout << "Enter string to replace with: ";
getline( cin, replaceWith );

cout << "Enter position to begin replacing: ";
cin >> start;

cout << "Enter length of text to replacing: ";
cin >> length;

text = text.replace( start, length, replaceWith );
cout << endl << "String is now: " << text << endl;
#+END_SRC




#+ATTR_HTML: :class console
#+BEGIN_SRC terminal :class terminal
Original text: helloworld
Enter string to replace with: BYE
Enter position to begin replacing: 2
Enter length of text to replacing: 5

String is now: heBYErld
#+END_SRC



-----
