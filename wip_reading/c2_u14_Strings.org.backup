# -*- mode: org -*-

#+TITLE: The C++ String Library
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>
-----


\begin{figure}[h]
    \begin{center}
        \includegraphics[width=10cm]{images/string.png}
    \end{center}
\end{figure}


A \textbf{string} is a special data type that really is
just an \textbf{array of char} variables. A string has a lot
going on behind-the-scenes, and it also has a set of
\textbf{functions} you can use to do some common operations
on a string - finding text, getting a letter at some position,
and more. ~\\

Note that everything with strings is \textbf{case-sensitive}.
A computer considers the letter \texttt{'a'} and \texttt{'A'}
different, since they are represented by different number codes.
Keep that in mind for each of the string's functions.

\newpage
\section{Strings as arrays}

    When we declare a string like this:

\begin{lstlisting}[style=code]
string str = "pizza";
\end{lstlisting}

    what we have behind-the-scenes is an array like this:

    \begin{center}
     \begin{tabular}{| l | c | c | c | c | c |} \hline
         \textbf{Index}     & 0 & 1 & 2 & 3 & 4
         \\ \hline
         \textbf{Element}   & 'p' & 'i' & 'z' & 'z' & 'a'
         \\ \hline
     \end{tabular}   
    \end{center}
    
    Declaring a string actually gives us an \textbf{array of \texttt{char} variables}.
    We can access the string as a whole by using the string variable's name
    (\texttt{str}), or access one \texttt{char} at a time by treating
    \texttt{str} like an array.

    \subsection{Subscript operator - get one char with [ ]}

        We can access each letter of the string directly with the
        \textbf{subscript operator}, just like an array:

\begin{lstlisting}[style=code]
cout << str[0] << endl; // Outputs p
cout << str[1] << endl; // Outputs i
cout << str[2] << endl; // Outputs z
cout << str[3] << endl; // Outputs z
cout << str[4] << endl; // Outputs a
\end{lstlisting}

        \vspace{0.5cm}

        Because we can act on a string like an array, this means we
        can also use a variable to access an arbitrary \textbf{index}
        of a character in the array...

\begin{lstlisting}[style=code]
int i;
cout << "Get which letter? ";
cin >> i;
cout << str[i];
\end{lstlisting}

        \vspace{0.5cm}

        Or even iterate over the string with a \textbf{for loop}...

\begin{lstlisting}[style=code]
for ( int i = 0; i < str.size(); i++ )
{
    cout << i << " = " << str[i] << endl;
}
\end{lstlisting}

        \vspace{0.5cm}

        Additionally, strings have a set of \textbf{functions} that
        we can use to manipulate, search, and otherwise work with them.



    \newpage
    \section{String functionality}
    

        % ----------------------------------------------------------------------------------- %
        \subsection{Size of the string}

            \begin{intro}{ \texttt{ size\_t size() const; } }    
                The string's \texttt{size()} function will return
                the size of the string - how many characters are
                stored in the string.
                The \texttt{size\_t} data type is just a type of
                integer - an \textit{unsigned} integer, because
                sizes cannot be negative amounts.
                
                ~\\ \footnotesize{ Documentation: https://www.cplusplus.com/reference/string/string/size/ }
            \end{intro}

            \paragraph{Example: Outputting a string's length}
            ~\\
            Let's write a little program that asks the user to enter
            some text, and then outputs the length of the string:

\begin{lstlisting}[style=code]
string text;
cout << "Enter some text: ";
getline( cin, text );
cout << "That string is " << text.size() 
    << " characters long!" << endl;
\end{lstlisting}

            When the user enters a line of text, it will count all
            characters (including spaces) in the string, so the text
            \texttt{"cats dogs"} would be 9 characters long.
                
\begin{lstlisting}[style=output]
Enter some text: cats dogs
That string is 9 characters long!
\end{lstlisting}

            \begin{center}
                \includegraphics[width=\textwidth]{Strings/images/catsdogs.png}
            \end{center}

            \newpage
            \paragraph{Example: Counting z's} ~\\
            If we wanted to use a \textbf{for loop} to iterate over
            all the letters of an array, we could! Perhaps we want to
            count the amount of z's that show up:

\begin{lstlisting}[style=code]
string text;
int zCount = 0;

cout << "Enter some text: ";
getline( cin, text );

// Iterate from i=0 to the size of the string (not-inclusive)
for ( unsigned int i = 0; i < text.size(); i++ )
{
    // If this letter is a lower-case z or upper-case Z
    if ( text[i] == 'z' || text[i] == 'Z' )
    {
        // Add one to z count
        zCount++;
    }
}

// Display the result
cout << "There were " << zCount
    << " z(s) in the string!" << endl;
\end{lstlisting}

\begin{lstlisting}[style=output]
Enter some text: The wizard fought a zombie lizard
There were 3 z(s) in the string!
\end{lstlisting}

            \begin{center}
                \includegraphics[width=\textwidth]{Strings/images/wizard.png}
            \end{center}

        % ----------------------------------------------------------------------------------- %
        \newpage
        \subsection{Concatenating strings with +}
        
            We can use the \texttt{+} operator to add strings
            together as well. This is called \textbf{concatenation}. ~\\
            
            Let's say we have two strings we want to combine -
            \texttt{favoriteColor} and \texttt{petName},
            we can use the concatenation operator \texttt{+}\footnote{It's the addition operator when using it with ints/floats, but concatenation when using it with strings.}
            to build a new string, \texttt{superSecurePassword}\footnote{This is NOT a secure password please don't do this.}:

\begin{lstlisting}[style=code]
string favoriteColor = "purple";
string petName = "Luna";
string superSecurePassword = favoriteColor + petName;
cout << superSecurePassword << endl; // = purpleLuna
\end{lstlisting}

            \begin{center}
                \includegraphics[width=1.5cm]{Strings/images/luna.png}
            \end{center}


            We can also add onto strings by using the \texttt{+=} operator.
            Let's say you're building a string over time in a program,
            and want to \textbf{append} parts separately.

\begin{lstlisting}[style=code]
string pizzaToppings = "";

// Later on...
pizzaToppings += "Buffalo sauce, ";

// Later on...
pizzaToppings += "Cheese, ";

// Later on...
pizzaToppings += "Pineapple";

// Later on...
cout << pizzaToppings << endl;
\end{lstlisting}

            At the end of the program, the value of \texttt{pizzaToppings} would be:
            
            \begin{center}
                \texttt{"Buffalo sauce, Cheese, Pineapple"}
            \end{center}
        
        
        % ----------------------------------------------------------------------------------- %
        \newpage
        \subsection{Finding text with find()}

            \begin{intro}{ \texttt{ size\_t find (const string\& str, size\_t pos = 0) const; } }    
                The find function can be used to look for a substring in a bigger string.
                If the substring is found, its position is returned.
                Otherwise, the value of \texttt{string::npos} is found.
                
                When a starting \texttt{pos} is included, it only begins the search at that position.
                If left off, it defaults to 0 (the start of the string).
                
                ~\\ \footnotesize{ Documentation: https://www.cplusplus.com/reference/string/string/find/ }
            \end{intro}
            
            So when we want to search a string for some text, we can call it like \texttt{bigString.find( findMeString )},
            and that function call will return an unsigned integer: the location of the \texttt{findMeString}
            within \texttt{bigString}, or the value of \texttt{string::npos} when it is not found.
            
            
\begin{lstlisting}[style=code]
string str = "this was written during the 2021 winter storm make it stop please.";

string findMe = "winter";

size_t position = str.find( findMe );

cout << "The text \"" << findMe 
    << "\" was found at position " << position << endl;
\end{lstlisting}

\begin{lstlisting}[style=output]
The text "winter" was found at position 33
\end{lstlisting}
            
        
        % ----------------------------------------------------------------------------------- %
        \newpage
        \subsection{Finding substrings with substr()}

            \begin{intro}{ \texttt{ string substr (size\_t pos = 0, size\_t len = npos) const; } }    
                Returns a string within the string, starting at the position \texttt{pos} provided,
                and with a length of \texttt{len}.
                
                ~\\ \footnotesize{ Documentation: https://www.cplusplus.com/reference/string/string/substr/ }
            \end{intro}
            
            With the \texttt{substr()} function, we can pull part of a string out,
            using a starting point and a length.
            
\begin{lstlisting}[style=code]
string text = "Name: Bob";

int start = 6;
int length = 3;

string name = text.substr( start, length );

cout << "Extracted \"" << name << "\"." << endl;

\end{lstlisting}

\begin{lstlisting}[style=output]
Extracted "Bob".
\end{lstlisting}

            \begin{center}
                \includegraphics[width=\textwidth]{Strings/images/bob.png}
            \end{center}


        % ----------------------------------------------------------------------------------- %
        \newpage
        \subsection{Comparing text with compare()}

            \begin{intro}{ \texttt{ int compare (const string\& str) const; } }    
                This compares the string with \texttt{str} and returns an integer:
                
                \begin{itemize}
                    \item   $0$   is returned if both strings are the same.
                    \item   $< 1$ (a negative number) is returned if the caller string is ``less-than'' the \texttt{str} string.
                    \item   $> 1$ (a positive number) is returned if the caller string is ``greater-than'' the \texttt{str} string.
                \end{itemize}
                
                One character is ``less than'' another if it comes \textbf{before},
                and it is ``greater than'' if it comes \textbf{after}, alphabetically.
                
                ~\\ (Remember that lower-case and upper-case letters are considered separate,
                so comparing \texttt{'a'} to \texttt{'A'} would actually return a positive number.)
                
                ~\\ \footnotesize{ Documentation: https://www.cplusplus.com/reference/string/string/compare/ }
            \end{intro}
            
\begin{lstlisting}[style=code]
string first;
string second;

cout << "Enter first string: ";
cin >> first;

cout << "Enter second string: ";
cin >> second;

int order = first.compare( second );

cout << endl << "Result: " << order << endl;
\end{lstlisting}

\begin{lstlisting}[style=output]
Enter first string: apple
Enter second string: banana

Result: -1
\end{lstlisting}
            
            
            \begin{center}
                \includegraphics[width=\textwidth]{Strings/images/compare.png}
            \end{center}

        % ----------------------------------------------------------------------------------- %
        \newpage
        \subsection{Inserting text into a string with insert()}

            \begin{intro}{ \texttt{ string\& insert (size\_t pos, const string\& str); } }    
                
                This function will take the calling string and modify it by inserting
                the string \texttt{str} at the position \texttt{pos}.
                
                ~\\ \footnotesize{ Documentation: https://www.cplusplus.com/reference/string/string/insert/ }
            \end{intro}
            
            
            
\begin{lstlisting}[style=code]
string text = "helloworld";

cout << "Original text: " << text << endl;

int start;
string insertText;

cout << "Enter text to insert: ";
getline( cin, insertText );

cout << "Enter position to insert: ";
cin >> start;

text = text.insert( start, insertText );

cout << endl << "String is now: " << text << endl;
\end{lstlisting}

\begin{lstlisting}[style=output]
Original text: helloworld
Enter text to insert: -to the-
Enter position to insert: 5

String is now: hello-to the-world
\end{lstlisting}
            
            
        % ----------------------------------------------------------------------------------- %
        \newpage
        \subsection{Erasing a chunk of text with erase()}

            \begin{intro}{ \texttt{ string\& erase (size\_t pos = 0, size\_t len = npos); } }    
                This function will return a string with a portion erased,
                starting at position \texttt{pos} and pulling out a length of \texttt{len}.
                
                ~\\ \footnotesize{ Documentation: https://www.cplusplus.com/reference/string/string/erase/ }
            \end{intro}
            
\begin{lstlisting}[style=code]
string text = "helloworld";

cout << "Original text: " << text << endl;

int start;
int length;

cout << "Enter position to begin erasing: ";
cin >> start;

cout << "Enter length of text to erase: ";
cin >> length;

text = text.erase( start, length );    
cout << endl << "String is now: " << text << endl;
\end{lstlisting}

\begin{lstlisting}[style=output]
Original text: helloworld
Enter position to begin erasing: 2
Enter length of text to erase: 5

String is now: herld
\end{lstlisting}
            
        
        % ----------------------------------------------------------------------------------- %    
        \newpage
        \subsection{Replacing a region of text with replace()}

            \begin{intro}{ \texttt{ string\& replace (size\_t pos,  size\_t len,  const string\& str); } }    
                This function is similar to erase, except it will insert the string \texttt{str}
                in place of the erased text.
                
                ~\\ \footnotesize{ Documentation: https://www.cplusplus.com/reference/string/string/replace/ }
            \end{intro}
            
\begin{lstlisting}[style=code]
string text = "helloworld";

cout << "Original text: " << text << endl;

int start;
int length;
string replaceWith;

cout << "Enter string to replace with: ";
getline( cin, replaceWith );

cout << "Enter position to begin replacing: ";
cin >> start;

cout << "Enter length of text to replacing: ";
cin >> length;

text = text.replace( start, length, replaceWith );    
cout << endl << "String is now: " << text << endl;
\end{lstlisting}

\begin{lstlisting}[style=output]
Original text: helloworld
Enter string to replace with: BYE
Enter position to begin replacing: 2
Enter length of text to replacing: 5

String is now: heBYErld
\end{lstlisting}
            
            
            
            
