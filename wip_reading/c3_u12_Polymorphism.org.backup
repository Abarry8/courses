# -*- mode: org -*-

#+TITLE: Polymorphism
#+AUTHOR: Rachel Wil Sha Singh

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/rworgmode.css" />
#+HTML_HEAD: <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
#+HTML_HEAD: <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/styles/default.min.css">
#+HTML_HEAD: <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.5.0/highlight.min.js"></script> <script>hljs.initHighlightingOnLoad();</script>
-----

* Design and polymorphism

So much of the design tricks and features we utilize in C++ and other
object-oriented programming languages all stem from the concept of
"do not repeat yourself". If you're writing the same
set of code in multiple places, there is a chance that we could
design the program so that we only need to write that code once.

[[file:images/c3_u12_Polymorphism_family.png]]

Polymorphism is a way that we can utilize pointers and something called
*vtables* to have a family of classes (related by inheritance)
and be able to write one set of code to handle interfacing with
/all of those family members/.
We have a family tree of classes, and we can write our program to
treat all the objects as the *parent class*, but the program will
decide which set of functions to call at run time.

#+BEGIN_SRC cpp :class cpp
Parent* myPtr = nullptr;
if      ( type == 1 ) { myPtr = new ChildA; }
else if ( type == 2 ) { myPtr = new ChildB; }

myPtr->Display();
delete myPtr;
#+END_SRC


#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
- Example: Quizzer and multiple question types ::

Let's say we are writing a quiz program and
there are different types of questions: True/false questions,
multiple choice, and fill-in-the-blank. They all have a common
question string, but how they store their answers is different...

#+ATTR_HTML: :class uml
| Question                 | TrueFalseQuestion       | MultipleChoiceQuestion    | FillInQuestion          |
|--------------------------+-------------------------+---------------------------+-------------------------|
| # =m_question= : string  | # =m_question= : string | # =m_question= : string   | # =m_question= : string |
|                          | # =m_answer= : bool     | # =m_options= : string[4] | # =m_answer= : string   |
|                          |                         | # =m_correct= : int       |                         |
| + bool AskQuestion()     | + bool AskQuestion()    | + bool AskQuestion()      | + bool AskQuestion()    |
| + void DisplayQuestion() |                         | + void ListAllAnswers()   |                         |

How would you store a series of inter-mixed quiz questions in a program?
Without polymorphism, you might think to just have separate vectors or arrays
for all the questions:

#+BEGIN_SRC cpp :class cpp
vector<TrueFalseQuestion>       tfQuestions;
vector<MultipleChoiceQuestion>  mcQuestions;
vector<FillInQuestion>          fiQuestions;
#+END_SRC

Utilizing polymorphism in C++, we could simply store an array of
pointers of the parent type:

#+BEGIN_SRC cpp :class cpp
vector<Question*> questions;
#+END_SRC

And then initialize the question as the type we want during creation:

#+BEGIN_SRC cpp :class cpp
questions.push_back(new TrueFalseQuestion);
questions.push_back(new MultipleChoiceQuestion);
questions.push_back(new FillInQuestion);
#+END_SRC

Since we are using the `new` keyword here, we would
also need to make sure to `delete` these items at the end
of the program:

#+BEGIN_SRC cpp :class cpp
for (auto& question : questions)
{
    delete question;
}
#+END_SRC

#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML

- Other design considerations ::

When we're working with polymorphism in this way, we need to be able
to treat each child as its parent, from a "calling functions"
perspective. Each child can have its own unique member functions and variables,
but when we're making calls to functions via a pointer to the parent type,
the parent only knows about functions that it, itself, has.

Let's say that the `Question` class has a `DisplayQuestion()`
function. Since all its children use `m_question` in the same way
and inherit this function, it will be fine to call it via the pointer.

#+BEGIN_SRC cpp :class cpp
ptrQuestion->DisplayQuestion(); // ok
#+END_SRC

But with a function that belongs to a child - not the parent's interface -
we wouldn't be able to call that function via the pointer without casting.

#+BEGIN_SRC cpp :class cpp
ptrQuestion->ListAllAnswers();  // not ok

(static_cast<MultipleChoiceQuestion*>(ptrQuestion))->ListAllAnswers(); ; ok
#+END_SRC

You could, however, still call that `ListAllAnswers` function
from within `MultipleChoiceQuestion`'s `DisplayQuestion` function,
and that would still work fine...

#+BEGIN_SRC cpp :class cpp
bool MultipleChoiceQuestion::AskQuestion()
{
    DisplayQuestion();
    ListAllAnswers();
    // etc.
}
#+END_SRC

Still fuzzy? That's OK, this is just an overview; we're going to step
into how all this works more in-depth next.


-----
* Reviewing classes and pointers

** Class inheritance and function overriding

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

Some things to remember about inheritance with classes:

- Any public or protected members (functions and variables) are inherited by the child class.
  (e.g., =m_question=, =DisplayQuestion()=, and =AskQuestion()=).
- A child class can override the a parent's function by declaring and defining a function with the same signature.
  (e.g., =AskQuestion()=).
- If the child class doesn't override a parent's function, then when that function is called via the child object
  it will call the parent's version of that function. (e.g., =DisplayQuestion()=).

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

[[file:images/c3_u12_Polymorphism_questioninherit.png]]

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML


#+ATTR_HTML: :class extra-space
#+BEGIN_HTML
--------------------------------------------------------------------------------
#+END_HTML
** Review: Pointers to class objects

You can declare a pointer to point to the address of an existing object,
or use the pointer to allocate memory for one or more new instances
of that class...

- Pointer to existing address: =myPtr = &existingQuestion;=
- Pointer to allocate memory: =myPtr = new Question;=

Then, to access a member of that object via the pointer, we use the =->= operator,
which is equivalent to dereferencing the pointer and then accessing a member:

- Arrow operator: =myPtr->DisplayQuestion();=
- Dereference and access: =(*myPtr).DisplayQuestion();=


-----

* Which version of the method is called?

Let's say we have several objects already declared:

#+ATTR_HTML: :class left-side
#+NAME: right-side
#+BEGIN_HTML

#+BEGIN_SRC cpp :class cpp
Question q1, q2;
MultipleChoiceQuestion mc1;
#+END_SRC

We could create a =Question*= ptr that points to
=q1= or =q2= or even =mc1=...

#+BEGIN_SRC cpp :class cpp
Question* ptr;
ptr = &q1;  ; ok
ptr = &q2;  ; ok
ptr = &mc1; ; ok?
#+END_SRC

#+END_HTML
#+ATTR_HTML: :class right-side
#+NAME: right-side
#+BEGIN_HTML

[[file:images/c3_u12_Polymorphism_questioninherit.png]]

#+END_HTML
#+ATTR_HTML: :class clear-both
#+NAME: clear-both
#+BEGIN_HTML
#+END_HTML


And, any functions that the =Question= class and
the =MultipleChoiceQuestion= class could be called
from this pointer...

#+BEGIN_SRC cpp :class cpp
ptr->DisplayQuestion();
#+END_SRC

This is fine for any member methods *not overridden* by the child class.
But, which version of the function is called if we used an *overridden* method?

#+BEGIN_SRC cpp :class cpp
ptr->AskQuestion();
#+END_SRC

[[file:images/c3_u12_Polymorphism_whichone.png]]


- No virtual methods - Which =AskQuestion()= is called? ::

  Let's say our class declarations look like this:

*Question:*

#+BEGIN_SRC cpp :class cpp
class Question
{
    public:
    bool AskQuestion();
    // etc.
};
#+END_SRC

*MultipleChoiceQuestion:*

#+BEGIN_SRC cpp :class cpp
class MultipleChoiceQuestion : public Question
{
    public:
    bool AskQuestion();
    // etc.
};
#+END_SRC

Here are the outputs we could have from using pointers in different ways:

*A. =Question='s =AskQuestion()= is called:*

#+BEGIN_SRC cpp :class cpp
  Question* ptr = new Question;
  bool result = ptr->AskQuestion();
#+END_SRC

*B. =MultipleChoiceQuestion='s =AskQuestion()= is called:*

#+BEGIN_SRC cpp :class cpp
  MultipleChoiceQuestion* ptr = new MultipleChoiceQuestion;
  bool result = ptr->AskQuestion();
#+END_SRC

*C. =Question='s =AskQuestion()= is called:*

#+BEGIN_SRC cpp :class cpp
  Question* ptr = new MultipleChoiceQuestion;
  bool result = ptr->AskQuestion();
#+END_SRC

"Well, how is that useful at all? The function called matches the
pointer data type!" - true, but we're missing one piece that allows us to
call *any child's version of the method* from a pointer of the parent type...

-----

* Virtual methods, late binding, and the Virtual Table (V-Table)


Instead, let's mark our method with the \textbf{virtual} keyword:

~\\ \textbf{Question:}
\begin{lstlisting}[style=code]
class Question
{
    public:
    virtual bool AskQuestion();
    // etc.
};
\end{lstlisting}

~\\ \textbf{MultipleChoiceQuestion:}
\begin{lstlisting}[style=code]
class MultipleChoiceQuestion : public Question
{
    public:
    virtual bool AskQuestion();
    // etc.
};
\end{lstlisting}

Here are the outputs we could have from using pointers in different ways:

\begin{mdframed}[backgroundcolor=colorblind_light_blue]
\texttt{Question* ptr = new Question;} ~\\
\texttt{bool result = ptr->AskQuestion();} ~\\~\\
\textbf{Question}'s AskQuestion() is called.
\end{mdframed}

\begin{mdframed}[backgroundcolor=colorblind_light_yellow]
\texttt{MultipleChoiceQuestion* ptr = new MultipleChoiceQuestion;} ~\\
\texttt{bool result = ptr->AskQuestion();} ~\\~\\
\textbf{MultipleChoiceQuestion}'s AskQuestion() is called.
\end{mdframed}

\begin{mdframed}[backgroundcolor=colorblind_light_yellow]
\texttt{Question* ptr = new MultipleChoiceQuestion;} ~\\
\texttt{bool result = ptr->AskQuestion();} ~\\~\\
\textbf{MultipleChoiceQuestion}'s AskQuestion() is called.
\end{mdframed}

With this, we can now store a list of \textbf{Question*} objects,
and each question can be a different child class, but we can
write one set of code to interact with each one of them.

\newpage
\subsection{Virtual methods, late binding, and the Virtual Table}

By using the \textbf{virtual} keyword, something happens with our functions -
it allows the pointer-to-the-parent class to figure out \textit{which version}
of the method to actually call, instead of just defaulting to the parent class' version.
But how does this work?

\paragraph{The \texttt{virtual} keyword} tells the compiler that the function
called will be figured out later. By marking a function as \textbf{virtual},
it then is added to something called a \textbf{virtual table} - or \textbf{vtable}.

The \textbf{vtable} stores special \textit{pointers to functions}. If a class
contains \textit{at least one virtual function}, then it will have its own vtable.

\begin{center}
\includegraphics[width=12cm]{ObjectOrientedProgramming/Polymorphism/vtables1.png}
\end{center}

With the \textbf{Question} class, it isn't inheriting any methods from anywhere
else so the vtable reflects the same methods it has. But, we also have
the child class that inherits \textbf{DisplayQuestion()} and overrides \textbf{AskQuestion()}.

\begin{center}
\includegraphics[width=12cm]{ObjectOrientedProgramming/Polymorphism/vtables2.png}
\end{center}

Because of these \textbf{vtables}, we can then have our pointers reference
this vtable when figuring out which version of a method to call.
Doing this is called \textbf{late binding} or \textbf{dynamic binding}.

\newpage
\subsection{When should we use \texttt{virtual}?}

\paragraph{Destructors should always be virtual.} If you're working with inheritance.
By making your destructor \textbf{virtual} for each class in the family,
you are ensuring that the \textbf{correct destructor} will be called when
the object is destroyed or goes out of scope. If you don't make it virtual
and utilize polymorphism, the correct destructor may not be called
(i.e., \texttt{Question}'s instead of \texttt{MultipleChoiceQuestion}'s).

\paragraph{Constructors cannot be marked \texttt{virtual}.}
When the object is instantiated (e.g., \texttt{ptr = new MultipleChoiceQuestion;})
that class' constructor will be called already.

\paragraph{Not every function needs to be virtual.}
It's all about design. Though generally, if you always want the parent's
version of a method to be called, you wouldn't override that method in
the child class anyway.

\newpage
\subsection{Designing interfaces with pure virtual functions and abstract classes}

Polymorphism works best if you're designing a family of classes around
some sort of \textbf{interface} that they will all share. In the C\# language,
there is an interface type that is available to you, but that's not here
in C++, so we implement it via classes.

\paragraph{What is an Interface?} ~\\

When we're designing a class to be an interface, the idea is that the
user (or other programmers) will just see a set of functions it will
interface with - none of the behind-the-scenes, how-it-works stuff.

Most of the devices we use have some sort of \textbf{interface},
hiding the more complicated specifics of how it actually works within a case.
For example, a calculator has a simple interface of buttons, but if you opened
it up you would be able to see its hardware and how everything is hooked up.

We use the same idea with writing software, where we expose some interface
(in the form of the class' \textbf{public methods}) as how the ``user''
interacts with our class.

\begin{center}
\includegraphics[width=8cm]{ObjectOrientedProgramming/Polymorphism/questionmanager.png}
\end{center}

A common design practice is to write the first \textbf{base (parent) class}
to be a specification of this sort of \textbf{interface} that all its children
will adhere to, and to ensure that each child class \textbf{must follow the interface}
by using something that the compiler will enforce itself: pure virtual functions.

\newpage

When working with our Quiz program idea, our \textbf{base class} is \texttt{Question},
which would define the interface for all other types of Questions.
Generally, our base interface class \textbf{would never be instantiated} -
it is not complete in and of itself (i.e., a Question with no types of Answers) -
but is merely used to outline a common interface for its family members.

~\\
Here is a blank diagram with just the member variables defined, but not yet
any functionality, so that we can begin to step through thinking about an interface:

\begin{center}
\includegraphics[width=\textwidth]{ObjectOrientedProgramming/Polymorphism/questionfamily.png}
\end{center}

Thinking in terms of implementing a program that could \textbf{edit questions}
(such as the teacher's view of the quiz), as well as that could
\textbf{ask questions} (such as the student's view), we can try to think of
what kind of functionality we would need from a question...

\begin{itemize}
    \item   Setup the question, answer(s)
    \item   Display the question to the user
    \item   Get the user's answer
    \item   Check if the user's answer was correct
\end{itemize}

But, the specifics of how each of these question types stores the correct
answer (and what data type it is) and validates it differ between each of them...

\begin{center}
    \begin{tabular}{p{3cm} | p{1.5cm} p{3cm} p{4cm}  }
                                & \textbf{User}     & \textbf{Stored answer}    & \textbf{Validate} \\ \hline
        \textbf{True/false}     & bool              & bool answer               & \footnotesize User input == answer? \\ \hline
        \textbf{MultiChoice}    & int               & string options[4]
        
                                                      int answer                & \footnotesize User input == answer? \\ \hline
        \textbf{FillIn}         & string            & string answer             & \footnotesize User input == answer?
    \end{tabular}
\end{center}

We could design our Questions so that they have functionality that interacts
with the user directly (e.g., a bool function that asks the user to enter
their response and returns true if they got it right and false if not)
rather than writing functions around returning the actual answer (which would
be more difficult because they have different data types).

\begin{itemize}
    \item   Set up question
    \item   Run question
\end{itemize}

\paragraph{Declarations:}~\\
We can set up a simple interface for our Questions with these functions.
They've been marked as \texttt{virtual}, which allows us to use polymorphism,
and they've also been marked with \texttt{= 0} at the end, marking them as
\textbf{pure virtual} - this tells the compiler that child classes \textbf{must}
implement their own version of these methods. A function that contains
pure virtual methods is called an \textbf{abstract class}.

\begin{lstlisting}[style=code]
class Question
{
  public:
  virtual void Setup() = 0;
  virtual bool Run() = 0;

  protected:
  string m_question;
};
\end{lstlisting}
\vspace{0.5cm}

Now our child classes can inherit from \texttt{Question}. They will be
required to override \texttt{Setup()} and \texttt{Run()}, and we can also
have additional functions as needed for that implementation:

\begin{lstlisting}[style=code]
class MultipleChoiceQuestion : public Question
{
  public:
  virtual void Setup();
  virtual bool Run();
  void ListAllAnswers();

  protected:
  string m_options[4];
  int m_answer;
};
\end{lstlisting}

\newpage
\paragraph{Definitions:}~\\
Each class will have its own implementation of these interface functions,
but since they're part of an interface, when we build a program around
these classes later we can call all of them the same way.

~\\ \textbf{Question:}
\begin{lstlisting}[style=code]
void Question::Setup() {
    cout << "Enter question: ";
    getline( cin, m_question );
}
\end{lstlisting}

~\\ \textbf{TrueFalseQuestion:}
\begin{lstlisting}[style=code]
void TrueFalseQuestion::Setup() {
    Question::Setup();    
    cout << "Enter answer (0 = false, 1 = true): ";
    cin >> m_answer;
}
\end{lstlisting}

~\\ \textbf{MultipleChoiceQuestion:}
\begin{lstlisting}[style=code]
void MultipleChoiceQuestion::Setup() {
    Question::Setup();
    
    for ( int i = 0; i < 4; i++ )
    {
        cout << "Enter option " << i << ": ";
        getline( cin, m_options[i] );
    }
    
    cout << "Which index is correct? ";
    cin >> m_answer;
}
\end{lstlisting}

~\\ \textbf{FillInQuestion:}
\begin{lstlisting}[style=code]
void FillInQuestion::Setup() {
    Question::Setup();
    cout << "Enter answer text: ";
    getline( cin, m_answer );
}
\end{lstlisting}

\newpage
\paragraph{Function calls:}~\\
Now, no matter what \textit{kind} of question subclass we're using,
we can utilize the same interface - and the same code.

\begin{lstlisting}[style=code]
// Create the pointer
Question* ptr = nullptr;

// Allocate memory
if ( choice == "true-false" )
{
    ptr = new TrueFalseQuestion();
}
else if ( choice == "multiple-choice" )
{
    ptr = new MultipleChoiceQuestion();
}
else if ( choice == "fill-in" )
{
    ptr = new FillInQuestion();
}

// Set up the question
ptr->Setup();

// Run the question
ptr->Run();

// Free the memory
delete ptr;
\end{lstlisting}

And, utilizing this interface, we could then store a \texttt{vector<Question*>}
and set up each question as any question subclass without any duplicate code.

